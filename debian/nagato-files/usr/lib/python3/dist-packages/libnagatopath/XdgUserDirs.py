
from gi.repository import GLib

ICON_NAMES = {
    0: "user-desktop",
    1: "folder-documents",
    2: "folder-download",
    3: "folder-music",
    4: "folder-pictures",
    5: "folder-publicshare",
    6: "folder-templates",
    7: "folder-videos",
}


def is_xdg_user_dir(fullpath, include_home_directory=False):
    if include_home_directory and fullpath == GLib.get_home_dir():
        return True
    yuki_is_xdg_user_dir, _ = get_icon_name(fullpath)
    return yuki_is_xdg_user_dir

def get_icon_name(fullpath):
    for yuki_index, yuki_name in ICON_NAMES.items():
        if fullpath == GLib.get_user_special_dir(yuki_index):
            return True, yuki_name
    return False, "folder"
