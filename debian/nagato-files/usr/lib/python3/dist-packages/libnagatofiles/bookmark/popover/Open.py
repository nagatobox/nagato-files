
from libnagato4.popover.item.Item import NagatoItem


class NagatoOpen(NagatoItem):

    TITLE = "Open"

    def _on_clicked(self, button):
        self._raise("YUKI.N > popdown")
        yuki_path = self._enquiry("YUKI.N > path")
        if yuki_path is None:
            return
        self._raise("YUKI.N > open directory", yuki_path)
