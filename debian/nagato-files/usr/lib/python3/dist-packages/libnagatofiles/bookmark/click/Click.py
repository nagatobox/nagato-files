
from libnagato4.Object import NagatoObject
from libnagatofiles.bookmark.click.Dispatch import NagatoDispatch


class NagatoClick(NagatoObject):

    def _get_path_at_position(self, tree_view, event):
        yuki_response = tree_view.get_path_at_pos(event.x, event.y)
        if yuki_response is None:
            return None
        yuki_tree_path, _, _, _ = yuki_response
        yuki_model = tree_view.get_model()
        return yuki_model[yuki_tree_path][3]

    def _on_button_press(self, tree_view, event):
        yuki_path = self._get_path_at_position(tree_view, event)
        if yuki_path:
            self._dispatch.dispatch(yuki_path, event)

    def __init__(self, parent):
        self._parent = parent
        self._dispatch = NagatoDispatch(self)
        yuki_tree_view = self._enquiry("YUKI.N > tree like widget")
        yuki_tree_view.connect("button-press-event", self._on_button_press)
