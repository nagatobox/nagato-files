
from libnagato4.popover.item.Item import NagatoItem


class NagatoOpenTrash(NagatoItem):

    TITLE = "Open Trash Bin"

    def _on_clicked(self, button):
        self._raise("YUKI.N > popdown")
        self._raise("YUKI.N > add new trash bin")
