
from libnagato4.popover.item.Item import NagatoItem


class NagatoOpenInNewTab(NagatoItem):

    TITLE = "Open in New Tab"

    def _on_clicked(self, button):
        self._raise("YUKI.N > popdown")
        yuki_path = self._enquiry("YUKI.N > path")
        if yuki_path is None:
            return
        self._raise("YUKI.N > add new tab", yuki_path)
