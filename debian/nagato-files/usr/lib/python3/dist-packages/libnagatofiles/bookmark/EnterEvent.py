
from gi.repository import Gdk


class HaruhiEnterEvent:

    def _on_enter_notify(self, tree_view, event):
        print("enter notify")

    def _on_realize(self, tree_view):
        yuki_gdk_window = tree_view.get_window()
        yuki_gdk_window.set_events(Gdk.EventMask.POINTER_MOTION_MASK)
        tree_view.connect("motion-notify-event", self._on_enter_notify)

    def __init__(self, tree_view):
        tree_view.connect("realize", self._on_realize)
