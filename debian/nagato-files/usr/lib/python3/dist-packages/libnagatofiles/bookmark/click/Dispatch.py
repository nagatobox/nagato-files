
from libnagato4.Object import NagatoObject
from libnagatofiles.bookmark.click.Left import NagatoLeft
from libnagatofiles.bookmark.click.Right import NagatoRight


class NagatoDispatch(NagatoObject):

    def dispatch(self, path, event):
        if event.button == 1:
            self._left.queue(path)
        if event.button == 3:
            self._right.queue(path, event)

    def __init__(self, parent):
        self._parent = parent
        self._left = NagatoLeft(self)
        self._right = NagatoRight(self)
