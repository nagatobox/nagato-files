
from gi.repository import Gtk
from libnagato4.Ux import Unit
from libnagato4.Object import NagatoObject


class NagatoIcon(Gtk.TreeViewColumn, NagatoObject):

    def _data_func(self, column, renderer, tree_model, tree_path, data=None):
        yuki_icon_name = tree_model[tree_path][0]
        if yuki_icon_name is not None:
            renderer.set_property("icon-name", yuki_icon_name)
        else:
            renderer.set_property("pixbuf", None)
        renderer.set_property("height", Unit(5))
        renderer.set_property("width", Unit(4))

    def __init__(self, parent):
        self._parent = parent
        yuki_renderer = Gtk.CellRendererPixbuf(xalign=1)
        Gtk.TreeViewColumn.__init__(self, cell_renderer=yuki_renderer)
        self.set_cell_data_func(yuki_renderer, self._data_func)
        self._raise("YUKI.N > insert column", self)
