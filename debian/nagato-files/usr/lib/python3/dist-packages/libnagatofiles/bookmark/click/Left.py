
from libnagato4.Object import NagatoObject


class NagatoLeft(NagatoObject):

    def queue(self, path):
        if path == "trash":
            self._raise("YUKI.N > add new trash bin")
        else:
            self._raise("YUKI.N > open directory", path)

    def __init__(self, parent):
        self._parent = parent
