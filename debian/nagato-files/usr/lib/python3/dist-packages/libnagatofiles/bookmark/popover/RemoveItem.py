
from libnagato4.popover.item.Item import NagatoItem
from libnagatofiles.util.RemoveBookmark import (
    NagatoRemoveBookmark as Remover
    )


class NagatoRemoveItem(NagatoItem):

    TITLE = "Remove Bookmark"

    def _on_clicked(self, button):
        self._raise("YUKI.N > popdown")
        yuki_path = self._enquiry("YUKI.N > path")
        if yuki_path is None:
            return
        self._remover.remove_path(yuki_path)

    def _on_initialize(self):
        self._remover = Remover(self)
