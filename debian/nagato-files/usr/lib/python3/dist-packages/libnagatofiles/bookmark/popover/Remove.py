
from libnagatopath import XdgUserDirs
from libnagatofiles.popover.Revealer import AbstractRevealer
from libnagatofiles.bookmark.popover.RemoveItem import NagatoRemoveItem


class NagatoRemove(AbstractRevealer):

    def _on_map(self, revealer):
        yuki_path = self._enquiry("YUKI.N > path")
        yuki_revealed = not XdgUserDirs.is_xdg_user_dir(yuki_path, True)
        self.set_reveal_child(yuki_revealed)

    def _on_initialize(self):
        NagatoRemoveItem(self)
