
from libnagato4.Object import NagatoObject


class NagatoSetDiff(NagatoObject):

    def set_new_for_list(self, list_):
        yuki_current_set = set(list_)
        if len(self._prev_set):
            self._raise("YUKI.N > set new", yuki_current_set-self._prev_set)
            self._raise("YUKI.N > set lost", self._prev_set-yuki_current_set)
        self._prev_set = yuki_current_set.copy()

    def __init__(self, parent):
        self._parent = parent
        self._prev_set = set()
