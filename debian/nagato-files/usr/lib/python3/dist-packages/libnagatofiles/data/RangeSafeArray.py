

class HaruhiRangeSafeArray(object):

    def append(self, index, value):
        if index > self.max:
            self._list.append(value)
        else:
            self._list[index] = value
            del self._list[index+1:]

    def __getitem__(self, index):
        if 0 > index or index > self.max:
            return None
        return self._list[index]

    def __setitem__(self, index, value):
        self._list[index] = value

    def __init__(self):
        self._list = []

    @property
    def max(self):
        return len(self._list)-1
