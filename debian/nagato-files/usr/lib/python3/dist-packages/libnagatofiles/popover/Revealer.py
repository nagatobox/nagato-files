
from gi.repository import Gtk
from libnagato4.Object import NagatoObject


class AbstractRevealer(Gtk.Revealer, NagatoObject):

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)

    def _on_map(self, revealer):
        raise NotImplementedError

    def _on_initialize(self):
        raise NotImplementedError

    def __init__(self, parent):
        self._parent = parent
        Gtk.Revealer.__init__(self)
        self.set_transition_type(Gtk.RevealerTransitionType.NONE)
        self._on_initialize()
        self.connect("map", self._on_map)
        self._raise("YUKI.N > add to box", self)
