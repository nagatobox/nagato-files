
from libnagato4.Ux import Unit
from libnagato4.widget.HBox import AbstractHBox
from libnagatofiles.trash.bar.SearchEntry import NagatoSearchEntry
from libnagatofiles.filer.bar.button.SwitchToList import NagatoSwitchToList
from libnagatofiles.filer.bar.button.SwitchToGrid import NagatoSwitchToGrid


class NagatoNavigation(AbstractHBox):

    def _on_initialize(self):
        NagatoSearchEntry(self)
        NagatoSwitchToGrid(self)
        NagatoSwitchToList(self)
        self.set_size_request(-1, Unit(5))
        self._raise("YUKI.N > css", (self, "toolbar"))
        self._raise("YUKI.N > add to box", self)
