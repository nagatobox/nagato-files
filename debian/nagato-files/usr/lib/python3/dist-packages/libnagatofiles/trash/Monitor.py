
from gi.repository import Gio
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagatofiles.trash import Directories
from libnagatofiles.trash.rowdata.Append import NagatoAppend
from libnagatofiles.trash.rowdata.Delete import NagatoDelete
from libnagatofiles.data.SetDiff import NagatoSetDiff


class NagatoMonitor(NagatoObject):

    def _yuki_n_set_new(self, names):
        for yuki_name in names:
            yuki_path = GLib.build_filenamev([Directories.FILE_DIR, yuki_name])
            self._append.append_for_fullpath(yuki_path)
            self._raise("YUKI.N > refresh tile", yuki_path)

    def _yuki_n_set_lost(self, names):
        for yuki_name in names:
            self._delete(yuki_name)
            self._raise("YUKI.N > trash changed")

    def _timeout(self):
        yuki_list = []
        for yuki_file_info in self._gio_file.enumerate_children("*", 0, None):
            yuki_list.append(yuki_file_info.get_name())
        self._set_diff.set_new_for_list(yuki_list)
        return GLib.SOURCE_CONTINUE

    def __init__(self, parent):
        self._parent = parent
        self._set_diff = NagatoSetDiff(self)
        self._append = NagatoAppend(self)
        self._delete = NagatoDelete(self)
        self._gio_file = Gio.File.new_for_path(Directories.FILE_DIR)
        GLib.timeout_add_seconds(1, self._timeout)
