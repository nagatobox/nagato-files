
from libnagato4.Object import NagatoObject
from libnagatofiles.trash.model.FilterLayer import NagatoFilterLayer


class NagatoFilterModelLayer(NagatoObject):

    def _inform_filtered_directory_model(self):
        return self._model

    def _yuki_n_refilter(self):
        self._model.refilter()
        self._raise("YUKI.N > filter changed")

    def _yuki_n_tree_path_activated(self, tree_path=None):
        pass
        # activate currently unused.

    def __init__(self, parent):
        self._parent = parent
        yuki_base_model = self._enquiry("YUKI.N > treeview model")
        self._model = yuki_base_model.filter_new()
        NagatoFilterLayer(self)
