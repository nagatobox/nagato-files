
from gi.repository import GLib

INFO_DIR = GLib.build_filenamev([GLib.get_user_data_dir(), "Trash/info"])
FILE_DIR = GLib.build_filenamev([GLib.get_user_data_dir(), "Trash/files"])
