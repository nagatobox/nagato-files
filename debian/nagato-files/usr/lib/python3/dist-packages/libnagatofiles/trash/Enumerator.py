
from gi.repository import Gio
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagatofiles.trash import Directories
from libnagatofiles.trash.rowdata.Append import NagatoAppend

FLAG = Gio.FileQueryInfoFlags.NONE


class NagatoEnumerator(NagatoObject):

    def _inform_last_file_name(self):
        return self._last_file_name

    def _enumerate(self, gio_file):
        yuki_name = ""
        for yuki_file_info in gio_file.enumerate_children("*", FLAG, None):
            yuki_name = yuki_file_info.get_name()
            yuki_names = [Directories.FILE_DIR, yuki_name]
            yuki_path = GLib.build_filenamev(yuki_names)
            self._append.append(yuki_path, yuki_file_info)
        self._last_file_name = yuki_name

    def __init__(self, parent):
        self._parent = parent
        self._append = NagatoAppend(self)
        self._last_file_name = ""
        self._enumerate(Gio.File.new_for_path(Directories.FILE_DIR))
