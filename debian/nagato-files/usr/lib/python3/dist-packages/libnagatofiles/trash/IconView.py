
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.trash.column import Types
from libnagatofiles.trash.UserInput import NagatoUserInput


class NagatoIconView(Gtk.IconView, NagatoObject):

    MODEL_REQUEST_MESSAGE = "YUKI.N > filtered directory model"

    def _inform_tree_like_widget(self):
        return self

    def _initialize_icon_view(self):
        Gtk.IconView.__init__(self)
        yuki_model = self._enquiry(self.MODEL_REQUEST_MESSAGE)
        self.set_model(yuki_model)
        self.set_pixbuf_column(Types.ICON_IMAGE)
        self.set_selection_mode(Gtk.SelectionMode.SINGLE)
        self.set_columns(-1)
        self.set_tooltip_column(Types.TOOLTIP)

    def __init__(self, parent):
        self._parent = parent
        self._initialize_icon_view()
        NagatoUserInput(self)
        self._raise("YUKI.N > add to scrolled window", self)
