
from gi.repository import Gio
from libnagatofiles.trash.column import Types
from libnagatofiles.trash.items.Items import NagatoItems


class NagatoErase(NagatoItems):

    def _queue_async(self, tree_row):
        yuki_source = Gio.File.new_for_path(tree_row[Types.FULLPATH])
        yuki_trash_info_path = tree_row[Types.TRASH_INFO_FULLPATH]
        yuki_trash_info = Gio.File.new_for_path(yuki_trash_info_path)
        self._delete.delete(yuki_source)
        yuki_trash_info.delete(None)
