

from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.trash.column import Types
from libnagatofiles.filer.column.Interfaces import HaruhiInterfaces
from libnagatofiles.filer.column.RendererText import HaruhiRenderer


class NagatoAge(Gtk.TreeViewColumn, NagatoObject, HaruhiInterfaces):

    def __init__(self, parent):
        self._parent = parent
        yuki_renderer = HaruhiRenderer.new(align=1)
        Gtk.TreeViewColumn.__init__(
            self,
            title="Deletion Time",
            cell_renderer=yuki_renderer,
            text=Types.DELETION
            )
        self.set_expand(False)
        self._bind_interfaces(yuki_renderer, Types.DELETION_UNIX_TIME)
        self._raise("YUKI.N > insert column", self)
