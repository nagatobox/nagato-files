
from libnagato4.Object import NagatoObject
from libnagato4.Transmitter import HaruhiTransmitter
from libnagatofiles.trash.model.ModelLayer import NagatoModelLayer


class NagatoPortalLayer(NagatoObject):

    def _yuki_n_selection_changed(self, selection):
        self._transmitter.transmit(("selection-changed", selection))

    def _yuki_n_filter_changed(self):
        self._transmitter.transmit(("filter-changed", None))

    def _yuki_n_trash_changed(self):
        self._transmitter.transmit(("trash-changed", None))

    def _yuki_n_register_navigation_widget(self, widget):
        self._transmitter.register_listener(widget)

    def _inform_page_id(self):
        return "trash-bin"

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = HaruhiTransmitter()
        NagatoModelLayer(self)
