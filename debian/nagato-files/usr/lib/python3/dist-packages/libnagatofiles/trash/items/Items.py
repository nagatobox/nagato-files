
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagatofiles.trash.column import Types
from libnagatofiles.trash.items.Delete import NagatoDelete


class NagatoItems(NagatoObject):

    def _queue_async(self, tree_row):
        raise NotImplementedError

    def _yield_selected_rows(self):
        for yuki_fullpath in self._enquiry("YUKI.N > selection"):
            yuki_data = Types.FULLPATH, yuki_fullpath
            yield self._enquiry("YUKI.N > trash bin row", yuki_data)

    def queue(self):
        for yuki_tree_row in self._yield_selected_rows():
            GLib.idle_add(self._queue_async, yuki_tree_row)

    def __init__(self, parent):
        self._parent = parent
        self._delete = NagatoDelete(self)
