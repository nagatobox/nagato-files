
from libnagato4.Object import NagatoObject
from libnagatofiles.trash.ButtonRelease import NagatoButtonRelease


class NagatoUserInput(NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        NagatoButtonRelease(self)
