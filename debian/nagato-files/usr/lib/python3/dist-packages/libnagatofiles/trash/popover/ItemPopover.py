
from libnagatofiles.filer.popover.FilerPopover import NagatoFilePopover
from libnagatofiles.trash.popover.ItemBox import NagatoItemBox
from libnagatofiles.filer.popover.SortBox import NagatoSortBox
from libnagatofiles.filer.popover.SelectBox import NagatoSelectBox
from libnagatofiles.filer.popover.AppsBox import NagatoAppsBox


class NagatoItemPopover(NagatoFilePopover):

    def _yuki_n_loopback_add_popover_groups(self, parent):
        NagatoItemBox(parent)
        NagatoAppsBox(parent)
        NagatoSortBox(parent)
        NagatoSelectBox(parent)
