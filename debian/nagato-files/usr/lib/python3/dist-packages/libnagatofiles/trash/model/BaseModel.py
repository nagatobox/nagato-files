
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.trash.column import Types

DEFAULT_SORT_COLUMN = Types.DELETION_UNIX_TIME
SORT_TYPE = Gtk.SortType.DESCENDING


class NagatoBaseModel(Gtk.ListStore, NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        Gtk.ListStore.__init__(self, *Types.TYPES)
        self.set_sort_column_id(DEFAULT_SORT_COLUMN, SORT_TYPE)
