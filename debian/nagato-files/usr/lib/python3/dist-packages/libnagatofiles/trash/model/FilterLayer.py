
from libnagato4.Object import NagatoObject
from libnagatofiles.trash.model.TileLayer import NagatoTileLayer
from libnagatofiles.trash.column import Types
from libnagatofiles.filer.filter.KeyWords import NagatoKeywords


class NagatoFilterLayer(NagatoObject):

    def _yuki_n_filter_changed(self, filter_=None):
        self._keywords.set_filter(filter_)

    def _on_filter(self, model, iter_, user_data=None):
        yuki_file_name = model[iter_][Types.FILE_NAME].lower()
        return self._keywords.is_shown(yuki_file_name)

    def __init__(self, parent):
        self._parent = parent
        yuki_model = self._enquiry("YUKI.N > filtered directory model")
        yuki_model.set_visible_func(self._on_filter)
        self._keywords = NagatoKeywords(self)
        NagatoTileLayer(self)
