
from gi.repository import GLib


def get_new():
    yuki_start = GLib.random_int_range(0, 100)
    yuki_list = []
    for yuki_position in range(yuki_start, 10000, 200):
        yuki_list.append(yuki_position)
    return yuki_list.copy()
