
import cairo
from gi.repository import Gdk
from libnagato4.Ux import Unit
from libnagatothumbnail.CairoContext import HaruhiCairoContext

CONTENT_TYPE = cairo.CONTENT_COLOR_ALPHA


class HaruhiSurface:

    @classmethod
    def new_for_size(cls, size):
        return HaruhiSurface((size, size))

    def get_pixbuf(self):
        return Gdk.pixbuf_get_from_surface(self._surface, 0, 0, *self._size)

    def paint_pixbuf(self, pixbuf):
        self._context.paint_pixbuf(pixbuf)
        return self._context.cairo_context

    def __init__(self, size):
        self._size = size
        self._surface = cairo.ImageSurface(cairo.Format.ARGB32, *size)
        self._context = HaruhiCairoContext(self._surface, size, None)
