
from gi.repository import Gtk
from libnagato4.Ux import Unit
from libnagato4.Object import NagatoObject


class AbstractHBox(Gtk.Box, NagatoObject):

    TITLE = "define title here"
    CSS = "define css here"

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)

    def _on_initialize(self):
        raise NotImplementedError

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self.set_homogeneous(True)
        yuki_label = Gtk.Label(self.TITLE)
        yuki_label.set_size_request(-1, Unit(8))
        yuki_label.set_xalign(1)
        yuki_label.set_hexpand(True)
        self.add(yuki_label)
        self._on_initialize()
        self._raise("YUKI.N > css", (self, self.CSS))
        self._raise("YUKI.N > add to box", self)
