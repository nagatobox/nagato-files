
from gi.repository import Gio
from gi.repository import GLib


def get(prefix=""):
    yuki_max = 10
    while True:
        yuki_max *= 10
        yuki_number = GLib.random_int_range(0, yuki_max)
        yuki_names = [GLib.get_tmp_dir(), prefix+str(yuki_number)]
        yuki_directory = GLib.build_filenamev(yuki_names)
        if GLib.file_test(yuki_directory, GLib.FileTest.EXISTS):
            continue
        yuki_gio_file = Gio.File.new_for_path(yuki_directory)
        yuki_gio_file.make_directory(None)
        return yuki_directory
