
from gi.repository import Gtk
from libnagato4.widget import Margin
from libnagato4.Object import NagatoObject
from libnagatofiles.chooser.mp4tag.CoverArtBox import NagatoCoverArtBox
from libnagatofiles.chooser.mp4tag.TitleBox import NagatoTitleBox
from libnagatofiles.chooser.mp4tag.ArtistBox import NagatoArtistBox
from libnagatofiles.chooser.mp4tag.AlbumBox import NagatoAlbumBox


class NagatoVBox(Gtk.Box, NagatoObject):

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        Margin.set_with_unit(self, 2, 2, 2, 2)
        NagatoCoverArtBox(self)
        NagatoTitleBox(self)
        NagatoArtistBox(self)
        NagatoAlbumBox(self)
        self.set_opacity(0.9)
        self._raise("YUKI.N > add to content area", self)
        self._raise("YUKI.N > css", (self, "background"))
        self.show_all()
