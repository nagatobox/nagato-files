
from libnagatofiles.chooser.mp4tag.TagBox import AbstractTagBox


class NagatoArtistBox(AbstractTagBox):

    TITLE = "Artist : "
    CSS = "config-box-even"
    KEY = "artist"
