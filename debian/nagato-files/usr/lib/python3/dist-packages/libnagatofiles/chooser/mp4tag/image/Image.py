
from gi.repository import Gtk
from libnagato4.Ux import Unit
from libnagato4.Object import NagatoObject
from libnagatofiles.chooser.mp4tag.image.Initializer import NagatoInitializer
from libnagatofiles.chooser.mp4tag.image.Receiver import NagatoReceiver


class NagatoImage(Gtk.Image, NagatoObject):

    def _yuki_n_image_pixbuf(self, pixbuf):
        self.set_from_pixbuf(pixbuf)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Image.__init__(self)
        self.set_size_request(Unit(16), Unit(16))
        NagatoReceiver(self)
        NagatoInitializer(self)
        self._raise("YUKI.N > add to button", self)
