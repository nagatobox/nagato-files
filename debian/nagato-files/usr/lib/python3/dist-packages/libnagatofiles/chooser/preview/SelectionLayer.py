
from libnagato4.Object import NagatoObject
from libnagatofiles.chooser.preview import Columns
from libnagatofiles.chooser.preview.ScrolledWindow import NagatoScrolledWindow


class NagatoSelectionLayer(NagatoObject):

    def _yuki_n_tree_path_selected(self, tree_path):
        for yuki_row in self._enquiry("YUKI.N > treeview model"):
            yuki_selected = (yuki_row.path == tree_path)
            if yuki_row[Columns.SELECTED] != yuki_selected:
                yuki_row[Columns.SELECTED] = yuki_selected

    def _on_row_changed(self, model, path, iter_):
        yuki_row = model[path]
        if yuki_row[Columns.SELECTED]:
            yuki_selection = yuki_row[Columns.UNSELECTED_PIXBUF]
            self._raise("YUKI.N > selection changed", yuki_selection)
            yuki_image = yuki_row[Columns.SELECTED_PIXBUF]
        else:
            yuki_image = yuki_row[Columns.UNSELECTED_PIXBUF]
        model.handler_block(self._id)
        yuki_row[Columns.PIXBUF] = yuki_image
        model.handler_unblock(self._id)

    def __init__(self, parent):
        self._parent = parent
        NagatoScrolledWindow(self)
        yuki_model = self._enquiry("YUKI.N > treeview model")
        self._id = yuki_model.connect("row-changed", self._on_row_changed)
