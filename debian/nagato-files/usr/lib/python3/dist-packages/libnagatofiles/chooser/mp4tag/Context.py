
from libnagato4.chooser.context.Context import AbstractContext
from libnagatofiles.chooser.mp4tag.ContextProps import NagatoContextProps
from libnagatofiles.chooser.mp4tag.Metadata import NagatoMetadata
from libnagatofiles.chooser.mp4tag import Apply


class NagatoContext(AbstractContext):

    @classmethod
    def new_for_path(cls, parent, source_path):
        yuki_context = cls(parent)
        yuki_context.set_source_path(source_path)
        return yuki_context

    def _yuki_n_tag(self, user_data):
        yuki_key, yuki_data = user_data
        self._props[yuki_key] = yuki_data
        self._props["selectable"] = True

    def apply(self):
        Apply.apply(self._props)

    def select(self, user_data):
        self._metadata.select(user_data)

    def set_source_path(self, path):
        self._context_props.set_path(path)
        self._metadata = NagatoMetadata.new_for_path(self, path)
        self._props["selectable"] = False

    def _on_initialize(self):
        self._context_props = NagatoContextProps(self)
