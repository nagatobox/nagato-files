
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.widget import Margin


class NagatoTagEntry(Gtk.Entry, NagatoObject):

    def _on_changed(self, editable, key):
        yuki_data = key, editable.get_text()
        self._raise("YUKI.N > selection changed", yuki_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Entry.__init__(self)
        yuki_key = self._enquiry("YUKI.N > key")
        yuki_text = self._enquiry("YUKI.N > chooser config", yuki_key)
        self.set_text(yuki_text)
        self.connect("changed", self._on_changed, yuki_key)
        Margin.set_with_unit(self, 2, 2, 2, 2)
        self._raise("YUKI.N > add to box", self)
