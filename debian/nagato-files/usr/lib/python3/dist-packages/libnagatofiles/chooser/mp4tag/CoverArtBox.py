
from libnagatofiles.chooser.mp4tag.HBox import AbstractHBox
from libnagatofiles.chooser.mp4tag.image.Button import NagatoButton


class NagatoCoverArtBox(AbstractHBox):

    TITLE = "CoverArt : "
    CSS = "config-box-even"

    def _on_initialize(self):
        NagatoButton(self)
