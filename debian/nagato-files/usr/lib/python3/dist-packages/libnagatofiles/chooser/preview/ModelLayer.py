
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.chooser.preview import Columns
from libnagatofiles.chooser.preview.TileLayer import NagatoTileLayer


class NagatoModelLayer(Gtk.ListStore, NagatoObject):

    def _inform_treeview_model(self):
        return self

    def _yuki_n_append_data(self, user_data):
        yuki_selected, yuki_path, yuki_tiles = user_data
        yuki_data = (
            yuki_selected,
            yuki_path,
            yuki_tiles[0],
            yuki_tiles[0],
            yuki_tiles[1]
            )
        self.append(yuki_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ListStore.__init__(self, *Columns.TYPES)
        NagatoTileLayer(self)
