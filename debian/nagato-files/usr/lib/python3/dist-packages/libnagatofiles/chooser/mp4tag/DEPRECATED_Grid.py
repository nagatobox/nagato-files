
from gi.repository import Gtk
from libnagato4.widget import Margin
from libnagato4.Object import NagatoObject


class NagatoGrid(Gtk.Grid, NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        Gtk.Grid.__init__(self)
        Margin.set_with_unit(self, 2, 2, 2, 2)
        self.attach(Gtk.Label("Cover Art"), 0, 0, 1, 1)
        self._raise("YUKI.N > add to content area", self)
        self._raise("YUKI.N > css", (self, "background"))
        self.show_all()
