
from libnagato4.Object import NagatoObject
from libnagatofiles.chooser.preview.Loader import NagatoLoader
from libnagatofiles.chooser.preview.SelectionLayer import NagatoSelectionLayer
from libnagatofiles.chooser.preview.Tile import HaruhiTile


class NagatoTileLayer(NagatoObject):

    def _yuki_n_thumbnail_created(self, out_path):
        yuki_tiles = self._tile.build_(out_path)
        yuki_data = False, out_path, yuki_tiles
        self._raise("YUKI.N > append data", yuki_data)

    def __init__(self, parent):
        self._parent = parent
        self._tile = HaruhiTile()
        NagatoLoader(self)
        NagatoSelectionLayer(self)
