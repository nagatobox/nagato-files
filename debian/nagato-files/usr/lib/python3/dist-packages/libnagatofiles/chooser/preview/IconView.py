
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.chooser.preview import Columns
from libnagatofiles.chooser.preview.ButtonRelease import NagatoButtonRelease


class NagatoIconView(Gtk.IconView, NagatoObject):

    MODEL_REQUEST_MESSAGE = "YUKI.N > treeview model"

    def _inform_tree_like_widget(self):
        return self

    def _on_item_activated(self, icon_view, tree_path):
        self._raise("YUKI.N > tree path selected", tree_path)
        self._raise("YUKI.N > response", Gtk.ResponseType.APPLY)

    def __init__(self, parent):
        self._parent = parent
        Gtk.IconView.__init__(self)
        yuki_model = self._enquiry(self.MODEL_REQUEST_MESSAGE)
        self.set_model(yuki_model)
        self.set_pixbuf_column(Columns.PIXBUF)
        self.set_selection_mode(Gtk.SelectionMode.NONE)
        self.set_columns(-1)
        NagatoButtonRelease(self)
        self._raise("YUKI.N > add to scrolled window", self)
        self.connect("item-activated", self._on_item_activated)
