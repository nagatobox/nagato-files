
from libnagato4.chooser.Dialog import AbstractDialog
from libnagatofiles.chooser.preview.ModelLayer import NagatoModelLayer


class NagatoDialog(AbstractDialog):

    @classmethod
    def run_with_context(cls, parent, context):
        yuki_dialog = NagatoDialog(parent)
        yuki_response = yuki_dialog.get_response(context)
        yuki_dialog.destroy()
        return yuki_response

    def _yuki_n_loopback_add_chooser_widget(self, parent):
        NagatoModelLayer(parent)

    def _yuki_n_response(self, response):
        print("response", response)
        self.response(response)
