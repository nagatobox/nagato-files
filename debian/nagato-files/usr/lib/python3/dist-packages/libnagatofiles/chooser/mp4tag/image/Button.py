
from gi.repository import Gtk
from libnagato4.widget import Margin
from libnagato4.Object import NagatoObject
from libnagato4.chooser.selectfile.Dialog import NagatoDialog
from libnagatofiles.chooser.mp4tag.image.OpenImage import NagatoContext
from libnagatofiles.chooser.mp4tag.image.Image import NagatoImage


class NagatoButton(Gtk.Button, NagatoObject):

    def _yuki_n_add_to_button(self, image):
        self.set_image(image)

    def _on_clicked(self, button):
        yuki_context = NagatoContext(self)
        yuki_response = NagatoDialog.run_with_context(self, yuki_context)
        if yuki_response == Gtk.ResponseType.APPLY:
            yuki_data = "coverart-path", yuki_context["selection"][0]
            self._raise("YUKI.N > selection changed", yuki_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self)
        self.props.tooltip_text = "Select Coverart Image"
        NagatoImage(self)
        Margin.set_with_unit(self, 2, 2, 2, 2)
        self.connect("clicked", self._on_clicked)
        self._raise("YUKI.N > add to box", self)
