
from gi.repository import Gio
from libnagatothumbnail.Surface import HaruhiSurface
from libnagatothumbnail.selected.Selected import HaruhiSelected
from libnagatothumbnail.image.Preview import HaruhiPreview


class HaruhiTile:

    def build_(self, fullpath):
        yuki_gio_file = Gio.File.new_for_path(fullpath)
        yuki_file_info = yuki_gio_file.query_info("*", 0)
        haruhi_surface = HaruhiSurface.new_tile()
        yuki_pixbuf = self._preview.load(fullpath, yuki_file_info)
        yuki_cairo_context = haruhi_surface.paint_pixbuf(yuki_pixbuf)
        yuki_unselected_tile = haruhi_surface.get_pixbuf()
        self._selected.paint(yuki_cairo_context)
        yuki_selected_tile = haruhi_surface.get_pixbuf()
        return yuki_unselected_tile, yuki_selected_tile

    def __init__(self):
        self._preview = HaruhiPreview()
        self._selected = HaruhiSelected()
