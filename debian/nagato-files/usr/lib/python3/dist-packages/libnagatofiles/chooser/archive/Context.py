
from libnagato4.chooser.context.AnySaveFile import AbstractAnySaveFile


class NagatoContext(AbstractAnySaveFile):

    TITLE = "Save File"

    @classmethod
    def new(cls, parent, directory, type_):
        yuki_context = cls(parent)
        yuki_context["directory"] = directory
        yuki_context["current-name"] = "archive"+type_
        return yuki_context

    def get_path(self):
        yuki_gio_file = self._props["selection"][0]
        return yuki_gio_file.get_path()

    def _get_mimetype(self):
        return {"all": "*/*"}
