
from libnagatofiles.chooser.preview import WorkingDirectory
from libnagatofiles.chooser.preview import PositionList


class HaruhiOutPaths:

    def pop(self):
        try:
            yuki_position = self._positions.pop(0)
        except IndexError:
            return None, None
        yuki_out = "{0}/{1}.png".format(self._directory, yuki_position)
        return yuki_position, yuki_out

    def __init__(self):
        self._directory = WorkingDirectory.get("nagato-files-")
        self._positions = PositionList.get_new()
