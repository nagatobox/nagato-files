
from libnagato4.chooser.context.Context import AbstractContext


class NagatoContext(AbstractContext):

    TITLE = "Select Preview"

    def get_pixbuf(self):
        return self._props["pixbuf"]

    def select(self, pixbuf):
        self._props["pixbuf"] = pixbuf
        self._props["selectable"] = True

    def _on_initialize(self):
        self._props["selectable"] = False
        self._props["title"] = self.TITLE
        self._props["pixbuf"] = None
