
from libnagato4.Ux import Unit
from libnagato4.Object import NagatoObject
from libnagatofiles.chooser.mp4tag.image import Pixbuf


class NagatoReceiver(NagatoObject):

    def _build_image(self):
        yuki_pixbuf = Pixbuf.new_for_size(self._path, Unit(24))
        self._raise("YUKI.N > image pixbuf", yuki_pixbuf)

    def receive_transmission(self, user_data):
        yuki_path = self._enquiry("YUKI.N > chooser config", "coverart-path")
        if yuki_path == self._path:
            return
        self._path = yuki_path
        self._build_image()

    def __init__(self, parent):
        self._parent = parent
        self._path = self._enquiry("YUKI.N > chooser config", "coverart-path")
        self._raise("YUKI.N > register selection object", self)
