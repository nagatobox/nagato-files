
from libnagatofiles.filer.bar.button.Button import NagatoButton


class NagatoNext(NagatoButton):

    NAME = "go-next-symbolic"
    TOOLTIP = "Next"

    def _on_clicked(self, button):
        self._raise("YUKI.N > move path", 1)
