
from libnagato4.Object import NagatoObject
from libnagato4.Transmitter import HaruhiTransmitter
from libnagatofiles.imageviewer.pixbuf.Pixbufs import NagatoPixbufs
from libnagatofiles.imageviewer.WidgetsLayer import NagatoWidgetsLayer


class NagatoPixbufLayer(NagatoObject):

    def _yuki_n_pixbuf_signals(self, user_data):
        self._transmitter.transmit(user_data)

    def _yuki_n_register_pixbuf_object(self, object_):
        self._transmitter.register_listener(object_)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = HaruhiTransmitter()
        self._pixbufs = NagatoPixbufs(self)
        NagatoWidgetsLayer(self)
