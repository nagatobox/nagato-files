
from gi.repository import GLib
from gi.repository import GdkPixbuf
from libnagato4.Object import NagatoObject


class NagatoAnimation(NagatoObject):

    def _refresh_pixbuf(self, iter_, path):
        yuki_pixbuf = iter_.get_pixbuf()
        yuki_delay = iter_.get_delay_time()
        self._raise("YUKI.N > refresh pixbuf", yuki_pixbuf.copy())
        GLib.timeout_add(yuki_delay, self._timeout, iter_, path)

    def _timeout(self, iter_, path):
        if iter_.advance() and path == self._enquiry("YUKI.N > path"):
            self._refresh_pixbuf(iter_, path)
        return GLib.SOURCE_REMOVE

    def start(self, path):
        yuki_animation = GdkPixbuf.PixbufAnimation.new_from_file(path)
        yuki_iter = yuki_animation.get_iter()
        self._refresh_pixbuf(yuki_iter, path)

    def __init__(self, parent):
        self._parent = parent
