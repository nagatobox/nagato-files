
from libnagato4.Object import NagatoObject
from libnagatofiles.imageviewer.signal import Zoom as Signal

FIXED = {Signal.ZOOM_FIT: -1, Signal.ZOOM_ORIGINAL: 1}
RELATIVE = {Signal.ZOOM_IN: 1.1, Signal.ZOOM_OUT: 0.9}
# SCROLL = {Signal.ZOOM_UP: 0.01, Signal.ZOOM_DOWN: -0.01}


class NagatoZoomDispatch(NagatoObject):

    def _get_base_rate(self, current):
        if current == -1:
            return self._enquiry("YUKI.N > buffer rate")
        return current

    def get_current(self, signal, current):
        if signal in FIXED:
            return FIXED[signal]
        elif signal in RELATIVE:
            yuki_rate = RELATIVE[signal]
            return self._get_base_rate(current)*yuki_rate
        """
        else:
            yuki_rate = SCROLL[signal]
            return self._get_base_rate(current)+yuki_rate
        """

    def __init__(self, parent):
        self._parent = parent
