
from libnagatofiles.imageviewer.button.Previous import NagatoPrevious
from libnagatofiles.imageviewer.button.Next import NagatoNext


class AsakuraNavigations:

    def __init__(self, parent):
        NagatoPrevious(parent)
        NagatoNext(parent)
