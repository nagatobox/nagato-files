
from libnagato4.Object import NagatoObject


class NagatoReceiver(NagatoObject):

    @classmethod
    def new_for_signal(cls, parent, signal):
        yuki_receiver = NagatoReceiver(parent)
        yuki_receiver.set_signal(signal)

    def receive_transmission(self, user_data):
        yuki_signal, yuki_data = user_data
        if yuki_signal == self._signal:
            self._raise("YUKI.N > signal received", yuki_data)

    def set_signal(self, signal):
        self._signal = signal

    def __init__(self, parent):
        self._parent = parent
        self._raise("YUKI.N > register pixbuf object", self)
