
from libnagatofiles.imageviewer.painter.CheckerBoard import NagatoCheckerBoard
from libnagatofiles.imageviewer.painter.Pixbuf import NagatoPixbuf


class AsakuraPainters:

    def paint(self, cairo_context):
        self._checker_board.paint(cairo_context)
        self._pixbuf.paint(cairo_context)

    def __init__(self, parent):
        self._checker_board = NagatoCheckerBoard(parent)
        self._pixbuf = NagatoPixbuf(parent)
