
from gi.repository import GdkPixbuf
from libnagato4.Object import NagatoObject
from libnagatofiles.imageviewer.zoom.MaximumSize import NagatoMaximumSize
from libnagatofiles.imageviewer.zoom.Rate import NagatoRate

INTERP = GdkPixbuf.InterpType.BILINEAR


class NagatoZoomEditor(NagatoObject):

    def _yuki_n_buffer_rate(self, rate):
        self._buffer_rate = rate

    def _inform_buffer_rate(self):
        return self._buffer_rate

    def edit(self, pixbuf):
        if self._rate.use_fit:
            yuki_width, yuki_height = self._max.get_zoomed_size(pixbuf)
        else:
            yuki_width, yuki_height = self._rate.get_zoomed_size(pixbuf)
        return pixbuf.scale_simple(yuki_width, yuki_height, INTERP)

    def __init__(self, parent):
        self._parent = parent
        self._buffer_rate = -1
        self._max = NagatoMaximumSize(self)
        self._rate = NagatoRate(self)
