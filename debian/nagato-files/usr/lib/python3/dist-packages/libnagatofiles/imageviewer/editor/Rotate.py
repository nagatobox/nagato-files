
from libnagato4.Object import NagatoObject
from libnagatofiles.imageviewer.signal import Pixbuf as Signal


class NagatoRotate(NagatoObject):

    def edit(self, pixbuf):
        return pixbuf.rotate_simple(self._rotate_angle)

    def receive_transmission(self, user_data):
        yuki_signal, yuki_data = user_data
        if yuki_signal == Signal.ROTATE_ANGLE_CHANGED:
            self._rotate_angle = (self._rotate_angle+yuki_data)%360
            self._raise("YUKI.N > request edit")

    def __init__(self, parent):
        self._parent = parent
        self._rotate_angle = 0
        self._raise("YUKI.N > register pixbuf object", self)
