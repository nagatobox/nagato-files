
from libnagato4.Object import NagatoObject
from libnagatofiles.imageviewer.signal import Pixbuf as Signal
from libnagatofiles.imageviewer.pixbuf.SignalReceiver import (
    NagatoSignalReceiver
    )


class NagatoSource(NagatoObject):

    def _yuki_n_refresh_pixbuf(self, pixbuf):
        self._pixbuf = pixbuf
        yuki_data = Signal.SOURCE_PIXBUF_CHANGED, self._pixbuf
        self._raise("YUKI.N > pixbuf signals", yuki_data)

    def __init__(self, parent):
        self._parent = parent
        self._pixbuf = None
        NagatoSignalReceiver(self)

    @property
    def pixbuf(self):
        return self._pixbuf
