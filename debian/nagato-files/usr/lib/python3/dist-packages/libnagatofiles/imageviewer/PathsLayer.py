
from libnagato4.Object import NagatoObject
from libnagato4.Transmitter import HaruhiTransmitter
from libnagatofiles.imageviewer.PixbufLayer import NagatoPixbufLayer
from libnagatofiles.imageviewer.paths.Paths import HaruhiPaths


class NagatoPathsLayer(NagatoObject):

    def _yuki_n_new_path(self, path):
        self.set_path(path)

    def _yuki_n_move_path(self, user_data):
        yuki_path = self._paths.move_path(user_data)
        if yuki_path is not None:
            self._transmitter.transmit(yuki_path)

    def _yuki_n_register_path_object(self, object_):
        self._transmitter.register_listener(object_)

    def set_path(self, path):
        self._paths.set_initial_path(path)
        self._transmitter.transmit(path)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = HaruhiTransmitter()
        self._paths = HaruhiPaths()
        NagatoPixbufLayer(self)
