
from gi.repository import Gdk
from libnagato4.Object import NagatoObject


class NagatoPixbuf(NagatoObject):

    def paint(self, cairo_context):
        yuki_pixbuf = self._enquiry("YUKI.N > pixbuf")
        yuki_drawing_area = self._enquiry("YUKI.N > drawing area")
        yuki_size, _ = yuki_drawing_area.get_allocated_size()
        yuki_x = max(0, (yuki_size.width-yuki_pixbuf.get_width())/2)
        yuki_y = max(0, (yuki_size.height-yuki_pixbuf.get_height())/2)
        Gdk.cairo_set_source_pixbuf(cairo_context, yuki_pixbuf, yuki_x, yuki_y)
        cairo_context.paint()

    def __init__(self, parent):
        self._parent = parent
