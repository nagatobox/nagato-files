
from libnagato4.Object import NagatoObject
from libnagatofiles.imageviewer.signal import Pixbuf as Signal
from libnagatofiles.imageviewer.zoom.ZoomDispatch import NagatoZoomDispatch


class NagatoRate(NagatoObject):

    def get_zoomed_size(self, pixbuf):
        yuki_width, yuki_height = pixbuf.get_width(), pixbuf.get_height()
        return yuki_width*self._rate, yuki_height*self._rate

    def receive_transmission(self, user_data):
        yuki_signal, yuki_data = user_data
        if yuki_signal != Signal.ZOOM_RATE_CHANGED:
            return
        yuki_args = yuki_data, self._rate
        self._rate = self._zoom_dispatch.get_current(*yuki_args)
        self._raise("YUKI.N > request edit")

    def __init__(self, parent):
        self._parent = parent
        self._rate = -1
        self._zoom_dispatch = NagatoZoomDispatch(self)
        self._raise("YUKI.N > register pixbuf object", self)

    @property
    def use_fit(self):
        return self._rate == -1
