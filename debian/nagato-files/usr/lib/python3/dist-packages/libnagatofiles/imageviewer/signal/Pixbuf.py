
SOURCE_PIXBUF_CHANGED = 0       # paired with Pixbuf
EDITED_PIXBUF_CHANGED = 1       # paired with Pixbuf
SCROLLED_AREA_SIZE_CHANGED = 2  # paired with Gdk.Rectangle
ZOOM_RATE_CHANGED = 3           # paired with integer, see signal/Zoom.py
ROTATE_ANGLE_CHANGED = 4        # paired with integer as degree
