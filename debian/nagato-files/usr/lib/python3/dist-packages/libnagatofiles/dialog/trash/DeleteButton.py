
from gi.repository import Gtk
from libnagato4.dialog.Button import AbstractButton


class NagatoDeleteButton(AbstractButton):

    RESPONSE = Gtk.ResponseType.APPLY
    TITLE = "Delete File(s)"
    CSS = "button-warning"
