
from libnagato4.dialog.Dialog import AbstractDialog
from libnagato4.dialog.warning.Image import NagatoImage
from libnagatofiles.dialog.drop.Label import NagatoLabel
from libnagatofiles.dialog.drop.ButtonBox import NagatoButtonBox


class NagatoDialog(AbstractDialog):

    CSS = "dialog-warning"

    def _yuki_n_loopback_add_content(self, parent):
        NagatoImage(parent)
        NagatoLabel(parent)
        NagatoButtonBox(parent)
