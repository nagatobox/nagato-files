
from libnagato4.dialog.Dialog import AbstractDialog
from libnagato4.dialog.warning.Image import NagatoImage
from libnagatofiles.dialog.trash.ScrolledWindow import NagatoScrolledWindow
from libnagatofiles.dialog.trash.ButtonBox import NagatoButtonBox


class NagatoTrash(AbstractDialog):

    CSS = "dialog-warning"

    def _yuki_n_loopback_add_content(self, parent):
        NagatoImage(parent)
        NagatoScrolledWindow(parent)
        NagatoButtonBox(parent)
