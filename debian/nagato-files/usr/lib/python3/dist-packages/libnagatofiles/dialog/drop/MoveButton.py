
from libnagato4.dialog.Button import AbstractButton
from libnagatofiles.filer.drag import ResponseType


class NagatoMoveButton(AbstractButton):

    RESPONSE = ResponseType.MOVE
    TITLE = "Move"
    CSS = "button-warning"
