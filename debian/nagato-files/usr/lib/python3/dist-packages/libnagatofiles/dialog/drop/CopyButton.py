
from libnagato4.dialog.Button import AbstractButton
from libnagatofiles.filer.drag import ResponseType


class NagatoCopyButton(AbstractButton):

    RESPONSE = ResponseType.COPY
    TITLE = "Copy"
    CSS = "button-warning"
