
import os
from gi.repository import Gio
from libnagato4.dialog.ButtonBox import AbstractButtonBox
from libnagato4.dialog.quit.CancelButton import NagatoCancelButton
from libnagatofiles.dialog.drop.CopyButton import NagatoCopyButton
from libnagatofiles.dialog.drop.MoveButton import NagatoMoveButton


class NagatoButtonBox(AbstractButtonBox):

    def _is_movable(self):
        for yuki_uri in self._enquiry("YUKI.N > uris"):
            yuki_file = Gio.File.new_for_uri(yuki_uri)
            yuki_path = yuki_file.get_path()
            if not os.access(yuki_path, os.W_OK):
                return False
        return True

    def _on_initialize(self):
        NagatoCancelButton(self)
        NagatoCopyButton(self)
        if self._is_movable():
            NagatoMoveButton(self)
