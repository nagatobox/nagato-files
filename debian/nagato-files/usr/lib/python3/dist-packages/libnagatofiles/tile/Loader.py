
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagatofiles.Mikuru import Columns
from libnagatofiles.tile.AsyncPathLoader import NagatoAsyncPathLoader
from libnagatothumbnail.TileSetter import HaruhiTileSetter

TEST_FLAGS = GLib.FileTest.EXISTS | GLib.FileTest.IS_SYMLINK


class NagatoLoader(NagatoObject):

    MODEL_REQUEST_MESSAGE = "YUKI.N > treeview model"

    def _inform_enumerate_rows(self):
        for yuki_row in self._enquiry(self.MODEL_REQUEST_MESSAGE):
            yield yuki_row

    def _get_tree_row(self, path):
        for yuki_row in self._inform_enumerate_rows():
            if yuki_row[Columns.FULLPATH] == path:
                return yuki_row

    def _refresh_tile_for_tree_row(self, tree_row):
        try:
            yuki_file_info = tree_row[Columns.FILE_INFO]
        except GLib.Error:
            print("lost or directory has been moved.")
            return
        yuki_fullpath = tree_row[Columns.FULLPATH]
        yuki_tiles = self._tile_setter.get_tiles(yuki_fullpath, yuki_file_info)
        yuki_unselected_tile, yuki_selected_tile = yuki_tiles
        tree_row[Columns.ICON_IMAGE] = yuki_unselected_tile
        tree_row[Columns.UNSELECTED_TILE] = yuki_unselected_tile
        tree_row[Columns.SELECTED_TILE] = yuki_selected_tile

    def _yuki_n_tree_row_loaded(self, tree_row):
        self._refresh_tile_for_tree_row(tree_row)

    def refresh_tile(self, fullpath):
        if not GLib.file_test(fullpath, TEST_FLAGS):
            return
        yuki_row = self._get_tree_row(fullpath)
        self._refresh_tile_for_tree_row(yuki_row)

    def initialize_directory(self):
        yuki_directory = self._enquiry("YUKI.N > current directory")
        self._async_path_loader.load_async(yuki_directory)

    def __init__(self, parent):
        self._parent = parent
        self._async_path_loader = NagatoAsyncPathLoader(self)
        self._tile_setter = HaruhiTileSetter()
