
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagatofiles.Mikuru import SourcePriority
from libnagatofiles.tile.SortedTreeRow import NagatoSortedTreeRow

PRIORITY = SourcePriority.TILE_LOADING


class NagatoAsyncPathLoader(NagatoObject):

    def _timeout(self, user_data):
        yuki_rows, yuki_directory = user_data
        if yuki_directory != self._enquiry("YUKI.N > current directory"):
            return False
        if not yuki_rows:
            return False
        yuki_row = yuki_rows.pop(0)
        self._raise("YUKI.N > tree row loaded", yuki_row)
        return True

    def load_async(self, directory):
        yuki_data = self._sorted_tree_row.get_sorted(), directory
        GLib.timeout_add(1, self._timeout, yuki_data, priority=PRIORITY)

    def __init__(self, parent):
        self._parent = parent
        self._sorted_tree_row = NagatoSortedTreeRow(self)
