
from gi.repository import Gtk
from gi.repository import GLib
from gi.repository import GdkPixbuf
from libnagato4.Object import NagatoObject
from libnagato4.file import HomeDirectory
from libnagatofiles.filesystem.reader.GtkBookmark import NagatoGtkBookmark
from libnagatofiles.filesystem.reader.UserSpecialDirs import (
    NagatoUserSpecialDirs
    )
from libnagatofiles.filesystem.reader.TrashBin import NagatoTrashBin

COLUMN_TYPES = str, str, str, str, str


class NagatoListStore(Gtk.ListStore, NagatoObject):

    def _yuki_n_append_data(self, user_data):
        self.append(user_data)

    def _yuki_n_append_path(self, user_data):
        yuki_path, yuki_icon_name = user_data
        yuki_base = GLib.filename_display_basename(yuki_path)
        yuki_tip = HomeDirectory.shorten(yuki_path)
        yuki_data = yuki_icon_name, "directory", yuki_base, yuki_path, yuki_tip
        self.append(yuki_data)

    def _inform_tree_row(self, user_data):
        yuki_index, yuki_data = user_data
        for yuki_tree_row in self:
            if yuki_tree_row[yuki_index] == yuki_data:
                return yuki_tree_row
        return None

    def refresh(self):
        self.clear()
        self._user_special_dirs.read()
        self.append((None, "space", "", "", None))
        self._gtk_bookmark.read()
        self.append((None, "space", "", "", None))
        self._trash_bin.read()

    def __init__(self, parent):
        self._parent = parent
        Gtk.ListStore.__init__(self, *COLUMN_TYPES)
        self._user_special_dirs = NagatoUserSpecialDirs(self)
        self._gtk_bookmark = NagatoGtkBookmark(self)
        self._trash_bin = NagatoTrashBin(self)
        self.refresh()
