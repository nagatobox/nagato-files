
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.filesystem.Monitor import NagatoMonitor

SIZE = Gtk.IconSize.SMALL_TOOLBAR


class NagatoTrashBin(NagatoObject):

    def _get_text(self, count):
        yuki_text = "Empty" if count == 0 else str(count)
        return "Trash ( {} )".format(yuki_text)

    def _yuki_n_trash_bin_count_changed(self, count):
        yuki_tree_row = self._enquiry("YUKI.N > tree row", (3, "trash"))
        yuki_tree_row[2] = self._get_text(count)

    def read(self):
        yuki_data = "user-trash-symbolic", "trash-bin", "Trash", "trash", "Trash"
        self._raise("YUKI.N > append data", yuki_data)

    def __init__(self, parent):
        self._parent = parent
        self._monitor = NagatoMonitor(self)
