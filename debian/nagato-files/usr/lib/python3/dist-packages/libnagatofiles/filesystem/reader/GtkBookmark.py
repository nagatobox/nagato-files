
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagatofiles.util.LineReader import HaruhiLineReader


class NagatoGtkBookmark(NagatoObject):

    def _get_bookmark_file_path(self):
        yuki_names = [GLib.get_user_config_dir(), "gtk-3.0", "bookmarks"]
        return GLib.build_filenamev(yuki_names)

    def _append(self, path):
        if not GLib.file_test(path, GLib.FileTest.EXISTS):
            return
        if self._enquiry("YUKI.N > tree row", (3, path)) is None:
            self._raise("YUKI.N > append path", (path, "folder-symbolic"))

    def read(self):
        yuki_reader = HaruhiLineReader(self._get_bookmark_file_path())
        for yuki_line in yuki_reader.read():
            yuki_data = yuki_line.split(" ", 1)
            yuki_path, _ = GLib.filename_from_uri(yuki_data[0])
            self._append(yuki_path)

    def __init__(self, parent):
        self._parent = parent
