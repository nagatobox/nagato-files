
import patoolib
from gi.repository import Gtk
from gi.repository import GLib
from libnagato4.chooser.context.SelectDirectory import NagatoContext
from libnagato4.chooser.selectfile.Dialog import NagatoDialog


def extract(parent, file_path):
    yuki_directory = GLib.path_get_dirname(file_path)
    yuki_context = NagatoContext.new_for_directory(parent, yuki_directory)
    yuki_response = NagatoDialog.run_with_context(parent, yuki_context)
    if yuki_response != Gtk.ResponseType.APPLY:
        return
    yuki_selected_directory = yuki_context["selection"]
    patoolib.extract_archive(file_path, outdir=yuki_selected_directory)
