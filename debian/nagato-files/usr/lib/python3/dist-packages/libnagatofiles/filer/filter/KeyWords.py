
from libnagato4.Object import NagatoObject


class NagatoKeywords(NagatoObject):

    def is_shown(self, file_name):
        yuki_file_name = file_name.lower()
        for yuki_filter in self._keywords:
            if yuki_filter in yuki_file_name:
                return True
        return self._keywords == []

    def has_keyword(self):
        return len(self._keywords) > 0

    def set_filter(self, filter_=None):
        if filter_ is not None:
            self._keywords = filter_.lower().split()
            self._raise("YUKI.N > refilter")

    def receive_transmission(self, path):
        self._keywords.clear()

    def __init__(self, parent):
        self._parent = parent
        self._keywords = []
        self._raise("YUKI.N > register current directory object", self)
