
from libnagato4.Object import NagatoObject


class NagatoShowHidden(NagatoObject):

    def is_shown(self, file_name):
        if not self._show_hidden and file_name.startswith("."):
            return False
        return True

    def toggle_show_hidden(self):
        self._show_hidden = not self._show_hidden
        self._raise("YUKI.N > refilter")

    def __init__(self, parent):
        self._parent = parent
        self._show_hidden = False

    @property
    def show_hidden(self):
        return self._show_hidden
