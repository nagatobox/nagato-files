
from libnagato4.popover.item.Item import NagatoItem
from libnagato4.file import HomeDirectory


class NagatoRecentDirectory(NagatoItem):

    @classmethod
    def new_for_path(cls, parent, path):
        yuki_item = cls(parent)
        yuki_item.set_path(path)

    def _on_clicked(self, button):
        self._raise("YUKI.N > popdown")
        self._raise("YUKI.N > path activated", self._path)

    def set_path(self, path):
        self._path = path
        self.set_label(HomeDirectory.shorten(path))
        self.set_alignment(0, 0.5)
