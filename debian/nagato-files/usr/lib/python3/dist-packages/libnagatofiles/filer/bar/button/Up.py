
from gi.repository import Gio
from libnagatofiles.filer.bar.button.Navigation import NagatoNavigation


class NagatoUp(NagatoNavigation):

    NAME = "go-up-symbolic"
    MESSAGE = "YUKI.N > path activated"
    USER_DATA = "up"
    TOOLTIP = "up"

    def _on_clicked(self, button):
        yuki_directory = self._enquiry("YUKI.N > current directory")
        yuki_gio_file = Gio.File.new_for_path(yuki_directory).get_parent()
        self._raise(self.MESSAGE, yuki_gio_file.get_path())

    def _refresh_sensitive(self):
        yuki_directory = self._enquiry("YUKI.N > current directory")
        self.set_sensitive(yuki_directory != "/")
