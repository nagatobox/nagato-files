
from libnagato4.popover.item.Item import NagatoItem


class NagatoUnselectAll(NagatoItem):

    TITLE = "Unselect All"
    MESSAGE = "YUKI.N > clear selection"
    USER_DATA = None
