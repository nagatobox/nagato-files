
from libnagatofiles.filer.popover.MultiSelection import NagatoMultiSelection


class NagatoSelectBefore(NagatoMultiSelection):

    METHOD = "iter_previous"
    SELECTION = False
    TITLE = "Select Before"
