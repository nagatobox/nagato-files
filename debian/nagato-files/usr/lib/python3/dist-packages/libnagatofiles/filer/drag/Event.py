
from gi.repository import Gdk
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.filer.drag.SafeQueue import NagatoSafeQueue

FLAGS = Gtk.DestDefaults.MOTION | Gtk.DestDefaults.DROP
ACTION = Gdk.DragAction.MOVE | Gdk.DragAction.COPY


class NagatoEvent(NagatoObject):

    def _on_drag_data_received(self, widget, context, x, y, data, info, time):
        if self._enquiry("YUKI.N > is valid position"):
            self._safe_queue.queue(data)

    def _initialize_event(self):
        yuki_widget = self._enquiry("YUKI.N > tree like widget")
        yuki_widget.connect('drag-data-received', self._on_drag_data_received)
        yuki_target_entries = self._enquiry("YUKI.N > target entries")
        yuki_widget.drag_dest_set(FLAGS, yuki_target_entries, ACTION)

    def __init__(self, parent):
        self._parent = parent
        self._initialize_event()
        self._safe_queue = NagatoSafeQueue(self)
