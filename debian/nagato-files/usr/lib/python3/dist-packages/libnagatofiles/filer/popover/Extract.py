
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.Mikuru import Columns
from libnagatofiles.filer.popover.ExtractItem import NagatoExtractItem
from libnagatofiles.archive import SupportedTypes


class NagatoExtract(Gtk.Revealer, NagatoObject):

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)

    def _on_map(self, revealer):
        yuki_tree_row = self._enquiry("YUKI.N > prime last selected row")
        yuki_type = yuki_tree_row[Columns.MIME_TYPE]
        yuki_revealed = (yuki_type in SupportedTypes.MIME)
        revealer.set_reveal_child(yuki_revealed)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Revealer.__init__(self)
        self.set_transition_type(Gtk.RevealerTransitionType.NONE)
        self.set_reveal_child(False)
        NagatoExtractItem(self)
        self.connect("map", self._on_map)
        self._raise("YUKI.N > add to box", self)
