
from libnagatofiles.filer.popover.MultiSelection import NagatoMultiSelection


class NagatoUnselectAfter(NagatoMultiSelection):

    METHOD = "iter_next"
    SELECTION = True
    TITLE = "Unselect After"
