
from libnagato4.popover.MultiStack import NagatoMultiStack
from libnagatofiles.filer.popover import GdkRectangle


class NagatoFilePopover(NagatoMultiStack):

    def popup_for_context(self, context):
        pass

    def popup_for_widget(self, widget, event):
        yuki_adjustments = self._enquiry("YUKI.N > adjustments")
        yuki_v, yuki_h = yuki_adjustments
        yuki_gdk_rectangle = GdkRectangle.get_from_event(event)
        yuki_gdk_rectangle.x = yuki_gdk_rectangle.x - yuki_h
        yuki_gdk_rectangle.y = yuki_gdk_rectangle.y - yuki_v
        self.set_relative_to(widget)
        self.set_pointing_to(yuki_gdk_rectangle)
        self.show_all()
