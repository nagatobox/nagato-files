
from libnagato4.popover.item.ToStack import NagatoToStack


class NagatoToMakeDirectory(NagatoToStack):

    TITLE = "Make Directory"
    WHERE_TO_GO = "makedirectory"
