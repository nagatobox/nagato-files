
from libnagato4.popover.item.Item import NagatoItem


class NagatoShowPreview(NagatoItem):

    TITLE = "Show Preview"

    def _on_clicked(self, button):
        self._raise("YUKI.N > popdown")
        yuki_path = self._enquiry("YUKI.N > prime path")
        self._raise("YUKI.N > show preview", yuki_path)
