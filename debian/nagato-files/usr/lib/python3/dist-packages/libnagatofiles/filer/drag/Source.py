
from gi.repository import Gdk
from gi.repository import GLib
from libnagato4.Object import NagatoObject

DRAG_MASK = Gdk.ModifierType.BUTTON1_MASK
ACTION = Gdk.DragAction.MOVE | Gdk.DragAction.COPY


class NagatoSource(NagatoObject):

    def _on_drag_begin(self, widget, context):
        yuki_path = self._enquiry("YUKI.N > path at pointer")
        yuki_model = self._enquiry("YUKI.N > filtered directory model")
        if yuki_path is None:
            return
        yuki_data = yuki_model[yuki_path], yuki_path, True
        self._raise("YUKI.N > last selected item changed", yuki_data)

    def _on_drag_data_get(self, widget, context, data, info, time):
        yuki_uris = []
        for yuki_path in self._enquiry("YUKI.N > selection"):
            yuki_uris.append(GLib.filename_to_uri(yuki_path))
        data.set_uris(yuki_uris)
        return True

    def _initialize_signals(self):
        yuki_widget = self._enquiry("YUKI.N > tree like widget")
        yuki_entries = self._enquiry("YUKI.N > target entries")
        yuki_widget.enable_model_drag_source(DRAG_MASK, yuki_entries, ACTION)
        yuki_widget.connect_after("drag-begin", self._on_drag_begin)
        yuki_widget.connect("drag-data-get", self._on_drag_data_get)

    def __init__(self, parent):
        self._parent = parent
        self._initialize_signals()
