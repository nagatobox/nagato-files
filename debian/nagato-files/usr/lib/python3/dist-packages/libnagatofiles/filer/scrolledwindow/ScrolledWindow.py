
from gi.repository import Gtk
from libnagato4.Object import NagatoObject


class NagatoScrolledWindow(Gtk.ScrolledWindow, NagatoObject):

    POLICY = Gtk.PolicyType.AUTOMATIC
    NAME = "YUKI.N > define name for stack here."

    def _inform_adjustments(self):
        yuki_v = self.get_vadjustment().get_value()
        yuki_h = self.get_hadjustment().get_value()
        return yuki_v, yuki_h

    def _yuki_n_add_to_scrolled_window(self, widget):
        self.add(widget)

    def _on_initialize(self):
        pass

    def __init__(self, parent):
        self._parent = parent
        Gtk.ScrolledWindow.__init__(self)
        self.set_policy(self.POLICY, self.POLICY)
        self.set_vexpand(True)
        self._on_initialize()
        self._raise("YUKI.N > add to stack named", (self, self.NAME))
