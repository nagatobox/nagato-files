
from gi.repository import Gio
from libnagato4.Object import NagatoObject
from libnagato4.Transmitter import HaruhiTransmitter
from libnagatofiles.filer.popover.makedirectory.Box import NagatoBox
from libnagatofiles.filer.popover.makedirectory.Path import NagatoPath


class NagatoModel(NagatoObject):

    def _inform_appliable(self):
        return self._path.get_appliable()

    def _yuki_n_apply(self):
        yuki_path = self._path.get_path()
        yuki_file = Gio.File.new_for_path(yuki_path)
        yuki_file.make_directory()

    def _yuki_n_edited(self, basename):
        self._path.reset_path(basename)
        self._transmitter.transmit("edited")

    def _yuki_n_mapped(self):
        self._path.reset_directory()

    def _yuki_n_register_apply_object(self, object_):
        self._transmitter.register_listener(object_)

    def __init__(self, parent):
        self._parent = parent
        self._path = NagatoPath(self)
        self._transmitter = HaruhiTransmitter()
        NagatoBox(self)
