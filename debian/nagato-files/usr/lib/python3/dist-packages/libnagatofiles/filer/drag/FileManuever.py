
from gi.repository import Gio
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagatofiles.Mikuru import SafePath2

COPY_FLAG = Gio.FileCopyFlags.NONE


class NagatoFileManeuver(NagatoObject):

    def _get_gio_files(self, directory, source_uri):
        yuki_source_gio_file = Gio.File.new_for_uri(source_uri)
        yuki_names = [directory, yuki_source_gio_file.get_basename()]
        yuki_target_path = GLib.build_filenamev(yuki_names)
        yuki_safe_path = SafePath2.get_path(yuki_target_path)
        yuki_destination_gio_file = Gio.File.new_for_path(yuki_safe_path)
        return yuki_source_gio_file, yuki_destination_gio_file

    def copy_to(self, directory, uris):
        for yuki_uri in uris:
            yuki_source, yuki_target = self._get_gio_files(directory, yuki_uri)
            yuki_source.copy(yuki_target, COPY_FLAG)
        self._raise("YUKI.N > clear selection")

    def move_to(self, directory, uris):
        for yuki_uri in uris:
            yuki_source, yuki_target = self._get_gio_files(directory, yuki_uri)
            yuki_source.move(yuki_target, COPY_FLAG)
        self._raise("YUKI.N > clear selection")

    def __init__(self, parent):
        self._parent = parent
