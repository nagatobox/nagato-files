
from libnagato4.popover.Box import NagatoBox
from libnagato4.popover.Separator import NagatoSeparator
from libnagato4.popover.main.BackToMain import NagatoBackToMain
from libnagatofiles.filer.popover.rename.Model import NagatoModel


class NagatoRenameBox(NagatoBox):

    NAME = "rename"

    def _on_initialize(self):
        NagatoBackToMain(self)
        NagatoSeparator(self)
        NagatoModel(self)
