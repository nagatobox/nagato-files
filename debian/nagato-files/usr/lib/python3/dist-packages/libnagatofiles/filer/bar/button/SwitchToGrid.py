
from libnagatofiles.filer.bar.button.Navigation import NagatoNavigation


class NagatoSwitchToGrid(NagatoNavigation):

    NAME = "view-grid-symbolic"
    MESSAGE = "YUKI.N > switch to"
    USER_DATA = "iconview"
    TOOLTIP = "Switch To Icon View"

    def _refresh_sensitive(self):
        pass
