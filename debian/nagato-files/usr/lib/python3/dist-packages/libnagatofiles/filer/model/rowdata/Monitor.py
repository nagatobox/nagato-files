
from gi.repository import Gio
from libnagato4.Object import NagatoObject
from libnagatofiles.filer.model.rowdata.Append import NagatoAppend
from libnagatofiles.filer.model.rowdata.Delete import NagatoDelete
from libnagatofiles.filer.model.MonitorEventDispatch import NagatoEventDispatch
from libnagatofiles.filer.signal import Model as Signal

FLAG = Gio.FileMonitorFlags.WATCH_MOVES


class NagatoMonitor(NagatoObject):

    def _yuki_n_create(self, gio_file):
        yuki_fullpath = gio_file.get_path()
        yuki_file_info = gio_file.query_info("*", 0, None)
        self._append(yuki_fullpath, yuki_file_info)
        yuki_data = Signal.REQUEST_REFRESH_TILE, yuki_fullpath
        self._raise("YUKI.N > model signal", yuki_data)

    def _yuki_n_delete(self, gio_file):
        yuki_fullpath = gio_file.get_path()
        self._delete(yuki_fullpath)

    def restart(self):
        yuki_gio_file = self._enquiry("YUKI.N > gio file")
        self._monitor = yuki_gio_file.monitor_directory(FLAG)
        self._monitor.connect("changed", self._dispatch.dispatch)

    def __init__(self, parent):
        self._parent = parent
        self._monitor = None
        self._dispatch = NagatoEventDispatch(self)
        self._append = NagatoAppend(self)
        self._delete = NagatoDelete(self)
