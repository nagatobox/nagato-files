
from libnagatofiles.filer.popover.FilerPopover import NagatoFilePopover
from libnagatofiles.filer.popover.FileBox import NagatoFileBox
from libnagatofiles.filer.popover.RenameBox import NagatoRenameBox
from libnagatofiles.filer.popover.SortBox import NagatoSortBox
from libnagatofiles.filer.popover.SelectBox import NagatoSelectBox
from libnagatofiles.filer.popover.AppsBox import NagatoAppsBox


class NagatoFilePopover(NagatoFilePopover):

    def _yuki_n_loopback_add_popover_groups(self, parent):
        NagatoFileBox(parent)
        NagatoRenameBox(parent)
        NagatoAppsBox(parent)
        NagatoSortBox(parent)
        NagatoSelectBox(parent)
