
from gi.repository import Gtk
from gi.repository import Gio
from libnagato4.Object import NagatoObject
from libnagatofiles.Mikuru import Columns
from libnagatofiles.filer.column.RendererPixbuf import HaruhiRenderer
from libnagatofiles.filer.column.Zebra import HaruhiZebra


class NagatoFileIcon(Gtk.TreeViewColumn, NagatoObject, HaruhiZebra):

    def _addtional_data_func(self, renderer, tree_row):
        yuki_type = tree_row[Columns.MIME_TYPE]
        yuki_icon = Gio.content_type_get_icon(yuki_type)
        renderer.set_property("gicon", yuki_icon)

    def __init__(self, parent):
        self._parent = parent
        yuki_renderer = HaruhiRenderer()
        Gtk.TreeViewColumn.__init__(
            self,
            title="",
            cell_renderer=yuki_renderer
            )
        self._raise("YUKI.N > insert column", self)
        self._bind_zebra_column(yuki_renderer)
