
from libnagato4.popover.item.ToStack import NagatoToStack


class NagatoToSort(NagatoToStack):

    TITLE = "Sort by"
    WHERE_TO_GO = "sort"
