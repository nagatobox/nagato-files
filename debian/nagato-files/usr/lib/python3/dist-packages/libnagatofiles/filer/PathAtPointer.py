
from libnagato4.Object import NagatoObject
from libnagatofiles.filer.DevicePosition import NagatoDevicePosition


class NagatoPathAtPointer(NagatoObject):

    def _extract_response(self, response):
        return response[0] if isinstance(response, tuple) else response

    def get(self):
        yuki_widget = self._enquiry("YUKI.N > tree like widget")
        yuki_x, yuki_y = self._device_position.get_position()
        yuki_response = yuki_widget.get_path_at_pos(yuki_x, yuki_y)
        if yuki_response is None:
            return None
        return self._extract_response(yuki_response)

    def is_valid_position(self):
        yuki_x, yuki_y = self._device_position.get_position()
        return yuki_x >= 0 and yuki_y >= 0

    def __init__(self, parent):
        self._parent = parent
        self._device_position = NagatoDevicePosition(self)
