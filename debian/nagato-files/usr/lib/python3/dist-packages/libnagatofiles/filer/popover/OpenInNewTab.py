
from libnagato4.popover.item.Item import NagatoItem


class NagatoOpenInNewTab(NagatoItem):

    TITLE = "Open In New Tab"
    MESSAGE = "YUKI.N > add new tab"

    def _on_clicked(self, button):
        self._raise("YUKI.N > popdown")
        yuki_directory = self._enquiry("YUKI.N > prime path")
        self._raise(self.MESSAGE, yuki_directory)
