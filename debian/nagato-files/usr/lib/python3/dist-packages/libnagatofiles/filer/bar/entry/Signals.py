
from gi.repository import Gio
from gi.repository import GLib
from libnagato4.Object import NagatoObject


class NagatoSignals(NagatoObject):

    def _check_filter(self, path):
        yuki_current_directory = self._enquiry("YUKI.N > current directory")
        yuki_gio_file = Gio.File.new_for_path(path).get_parent()
        yuki_parent_path = yuki_gio_file.get_path()
        yuki_filter = path[len(yuki_parent_path)+1:]
        if yuki_filter != ".":
            self._raise("YUKI.N > filter changed", yuki_filter)
        if yuki_current_directory != yuki_parent_path:
            self._raise("YUKI.N > filter changed", "")

    def _try_move_directory(self, text):
        if not self._enquiry("YUKI.N > locked"):
            self._raise("YUKI.N > path activated", text)
            self._raise("YUKI.N > filter changed", "")

    def _on_changed(self, entry):
        yuki_entry_text = entry.get_text()
        if GLib.file_test(yuki_entry_text, GLib.FileTest.IS_DIR):
            self._try_move_directory(yuki_entry_text)
        else:
            self._check_filter(yuki_entry_text)

    def __init__(self, parent):
        self._parent = parent
        yuki_entry = self._enquiry("YUKI.N > path entry")
        yuki_id = yuki_entry.connect("changed", self._on_changed)
