
from libnagato4.Object import NagatoObject
from libnagatofiles.tile.AsyncPathLoader import NagatoAsyncPathLoader
from libnagatofiles.filer.basemodel.TileSetter import NagatoTileSetter
from libnagatofiles.filer.signal import Model as Signal


class NagatoTileLoader(NagatoObject):

    def _inform_enumerate_rows(self):
        for yuki_row in self._enquiry("YUKI.N > treeview model"):
            yield yuki_row

    def _yuki_n_tree_row_loaded(self, tree_row):
        self._setter.set_for_tree_row(tree_row)

    def receive_transmission(self, user_data):
        yuki_signal, yuki_data = user_data
        if yuki_signal == Signal.DIRECTORY_REALIZED:
            yuki_directory = self._enquiry("YUKI.N > current directory")
            self._async_path_loader.load_async(yuki_directory)
        if yuki_signal == Signal.REQUEST_REFRESH_TILE:
            self._setter.set_for_fullpath(yuki_data)

    def __init__(self, parent):
        self._parent = parent
        self._async_path_loader = NagatoAsyncPathLoader(self)
        self._setter = NagatoTileSetter(self)
        self._raise("YUKI.N > register model object", self)
