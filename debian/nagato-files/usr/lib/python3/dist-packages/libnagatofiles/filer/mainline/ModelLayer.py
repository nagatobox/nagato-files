
from libnagato4.Object import NagatoObject
from libnagato4.Transmitter import HaruhiTransmitter
from libnagatofiles.filer.basemodel.BaseModel import NagatoBaseModel
from libnagatofiles.filer.basemodel.Controllers import AsakuraControllers
from libnagatofiles.filer.mainline.FilterLayer import NagatoFilterLayer


class NagatoModelLayer(NagatoObject):

    def _inform_treeview_model(self):
        return self._base_model

    def _inform_filtered_directory_model(self):
        return self._filtered_model

    def _yuki_n_refilter(self):
        self._filtered_model.refilter()

    def _yuki_n_model_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def _yuki_n_register_model_object(self, object_):
        self._transmitter.register_listener(object_)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = HaruhiTransmitter()
        self._base_model = NagatoBaseModel(self)
        self._filtered_model = self._base_model.filter_new()
        AsakuraControllers(self)
        NagatoFilterLayer(self)
