
from libnagato4.popover.Box import NagatoBox
from libnagato4.popover.Separator import NagatoSeparator
from libnagatofiles.filer.popover.ToSort import NagatoToSort
from libnagatofiles.filer.popover.ToMakeDirectory import NagatoToMakeDirectory
from libnagatofiles.filer.popover.ShowHidden import NagatoShowHidden
from libnagatofiles.filer.popover.DupricateDirectory import (
    NagatoDupricateDirectory
    )


class NagatoVoidBox(NagatoBox):

    NAME = "main"

    def _on_initialize(self):
        NagatoShowHidden(self)
        NagatoToMakeDirectory(self)
        NagatoDupricateDirectory(self)
        NagatoSeparator(self)
        NagatoToSort(self)
