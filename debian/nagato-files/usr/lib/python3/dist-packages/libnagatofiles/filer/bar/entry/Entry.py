
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.filer.bar.entry.Signals import NagatoSignals


class NagatoEntry(Gtk.Entry, NagatoObject):

    def _inform_locked(self):
        return self._lock

    def receive_transmission(self, directory):
        self._lock = True
        self.set_text(directory)
        self.set_position(len(directory))
        self._lock = False

    def _inform_path_entry(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        Gtk.Entry.__init__(self)
        self.set_hexpand(True)
        self.set_alignment(0.01)
        self._lock = False
        NagatoSignals(self)
        self._raise("YUKI.N > add to box", self)
        self._raise("YUKI.N > register current directory object", self)
