
from gi.repository import Gtk
from libnagato4.Object import NagatoObject


class NagatoButton(Gtk.Button, NagatoObject):

    NAME = "folder-open-symbolic"
    SIZE = Gtk.IconSize.LARGE_TOOLBAR
    MESSAGE = "YUKI.N > ignore"
    USER_DATA = None
    TOOLTIP = None

    def _on_clicked(self, button):
        self._raise(self.MESSAGE, self.USER_DATA)

    def _post_initialization(self):
        pass

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self)
        self.set_image(Gtk.Image.new_from_icon_name(self.NAME, self.SIZE))
        if self.TOOLTIP is not None:
            self.props.tooltip_text = self.TOOLTIP
        self.connect("clicked", self._on_clicked)
        self._raise("YUKI.N > add to box", self)
        self._post_initialization()
        self._raise("YUKI.N > css", (self, "toolbar-button"))
