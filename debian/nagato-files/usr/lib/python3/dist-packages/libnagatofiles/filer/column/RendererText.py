
from gi.repository import Gtk
from gi.repository import Pango
from libnagato4.Ux import Unit


class HaruhiRenderer(Gtk.CellRendererText):

    @classmethod
    def new(cls, ellipsize=Pango.EllipsizeMode.NONE, align=0):
        yuki_renderer = cls()
        yuki_renderer.set_property("xpad", Unit(2))
        yuki_renderer.set_property("ellipsize", ellipsize)
        yuki_renderer.set_property("xalign", align)
        return yuki_renderer

    def __init__(self):
        Gtk.CellRendererText.__init__(self)
