
from libnagato4.popover.Box import NagatoBox
from libnagato4.popover.Separator import NagatoSeparator
from libnagatofiles.filer.popover.OpenInNewTab import NagatoOpenInNewTab
from libnagatofiles.filer.popover.rename.ToRename import NagatoToRename
from libnagatofiles.filer.popover.CopyUri import NagatoCopyUri
from libnagatofiles.filer.popover.CopyPath import NagatoCopyPath
from libnagatofiles.filer.popover.ToSort import NagatoToSort
from libnagatofiles.filer.popover.ToSelect import NagatoToSelect
from libnagatofiles.filer.popover.maneuver.Maneuvers import AsakuraManeuvers


class NagatoDirectoryBox(NagatoBox):

    NAME = "main"

    def _on_initialize(self):
        NagatoOpenInNewTab(self)
        NagatoToRename(self)
        NagatoCopyUri(self)
        NagatoCopyPath(self)
        NagatoSeparator(self)
        NagatoToSort(self)
        NagatoToSelect(self)
        NagatoSeparator(self)
        AsakuraManeuvers(self)
