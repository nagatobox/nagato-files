
from libnagato4.Object import NagatoObject
from libnagatofiles.dialog.drop.Dialog import NagatoDialog
from libnagatofiles.filer.drag.FileManuever import NagatoFileManeuver
from libnagatofiles.filer.drag import ResponseType


class NagatoAction(NagatoObject):

    def queue(self):
        yuki_response = NagatoDialog.get_response(self)
        yuki_directory = self._enquiry("YUKI.N > directory")
        yuki_uris = self._enquiry("YUKI.N > uris")
        if yuki_response == ResponseType.COPY:
            self._file_manuever.copy_to(yuki_directory, yuki_uris)
        if yuki_response == ResponseType.MOVE:
            self._file_manuever.move_to(yuki_directory, yuki_uris)

    def __init__(self, parent):
        self._parent = parent
        self._file_manuever = NagatoFileManeuver(self)
