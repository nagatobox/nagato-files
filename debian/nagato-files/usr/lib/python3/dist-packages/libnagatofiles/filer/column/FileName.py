
from gi.repository import Gtk
from gi.repository import Pango
from libnagato4.Object import NagatoObject
from libnagatofiles.Mikuru import Columns
from libnagatofiles.filer.column.Interfaces import HaruhiInterfaces
from libnagatofiles.filer.column.RendererText import HaruhiRenderer


class NagatoFileName(Gtk.TreeViewColumn, NagatoObject, HaruhiInterfaces):

    def __init__(self, parent):
        self._parent = parent
        yuki_renderer = HaruhiRenderer.new(Pango.EllipsizeMode.MIDDLE)
        Gtk.TreeViewColumn.__init__(
            self,
            title="Name",
            cell_renderer=yuki_renderer,
            text=Columns.FILE_NAME
            )
        self.set_expand(True)
        self._bind_interfaces(yuki_renderer, Columns.PATH_FOR_SORT)
        self._raise("YUKI.N > insert column", self)
