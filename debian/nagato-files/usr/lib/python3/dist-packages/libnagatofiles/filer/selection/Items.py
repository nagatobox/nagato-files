
from libnagato4.Object import NagatoObject
from libnagatofiles.Mikuru import Columns

MODEL_REQUEST_MESSAGE = "YUKI.N > filtered directory model"


class NagatoItems(NagatoObject):

    def _get_selected_rows(self):
        for yuki_row in self._enquiry(MODEL_REQUEST_MESSAGE):
            if not yuki_row[Columns.SELECTED]:
                yield yuki_row

    def get_all(self):
        yuki_fullpaths = []
        for yuki_row in self._get_selected_rows():
            yuki_fullpaths.append(yuki_row[Columns.FULLPATH])
        return yuki_fullpaths

    def clear_all(self):
        for yuki_row in self._get_selected_rows():
            yuki_row[Columns.SELECTED] = True

    def select_all(self):
        for yuki_row in self._enquiry(MODEL_REQUEST_MESSAGE):
            yuki_row[Columns.SELECTED] = False

    def can_move(self):
        for yuki_row in self._get_selected_rows():
            yuki_file_info = yuki_row[Columns.FILE_INFO]
            yuki_attribute = "access::can-delete"
            if not yuki_file_info.get_attribute_boolean(yuki_attribute):
                return False
        return True

    def invert_selection(self):
        for yuki_row in self._enquiry(MODEL_REQUEST_MESSAGE):
            yuki_row[Columns.SELECTED] = not yuki_row[Columns.SELECTED]

    def __init__(self, parent):
        self._parent = parent
