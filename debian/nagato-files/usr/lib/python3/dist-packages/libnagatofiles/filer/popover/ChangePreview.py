
from gi.repository import Gtk
from libnagato4.popover.item.Item import NagatoItem
from libnagatofiles.Mikuru import File
from libnagatofiles.Mikuru import ThumbnailCache
from libnagatofiles.chooser.preview.Dialog import NagatoDialog
from libnagatofiles.chooser.preview.Context import NagatoContext
from libnagatofiles.filer.signal import Model as Signal


class NagatoChangePreview(NagatoItem):

    TITLE = "Change Preview"

    def _on_clicked(self, button):
        self._raise("YUKI.N > popdown")
        yuki_context = NagatoContext(self)
        yuki_response = NagatoDialog.run_with_context(self, yuki_context)
        if yuki_response != Gtk.ResponseType.APPLY:
            return
        yuki_path = self._enquiry("YUKI.N > prime path")
        ThumbnailCache.save_wait(yuki_context.get_pixbuf(), yuki_path)
        yuki_data = Signal.REQUEST_REFRESH_TILE, yuki_path
        self._raise("YUKI.N > model signal", yuki_data)
