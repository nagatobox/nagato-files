
from libnagato4.Ux import Unit
from libnagato4.widget.HBox import AbstractHBox
from libnagatofiles.filer.bar.button.ViewMore import NagatoViewMore
from libnagatofiles.filer.bar.status.Label import NagatoLabel
from libnagatofiles.filer.bar.button.Archive import NagatoArchive
from libnagatofiles.filer.bar.button.CopyTo import NagatoCopyTo
from libnagatofiles.filer.bar.button.SendTo import NagatoSendTo
from libnagatofiles.filer.bar.button.Trash import NagatoTrash


class NagatoAction(AbstractHBox):

    def _on_initialize(self):
        NagatoViewMore(self)
        NagatoLabel(self)
        NagatoArchive(self)
        NagatoCopyTo(self)
        NagatoSendTo(self)
        NagatoTrash(self)
        self.set_size_request(-1, Unit(5))
        self._raise("YUKI.N > css", (self, "toolbar"))
        self._raise("YUKI.N > add to box", self)
