
from libnagatofiles.filer.bar.button.Button import NagatoButton


class NagatoNavigation(NagatoButton):

    def _refresh_sensitive(self):
        raise NotImplementedError

    def receive_transmission(self, directory):
        self._refresh_sensitive()

    def _post_initialization(self):
        self._raise("YUKI.N > register current directory object", self)
