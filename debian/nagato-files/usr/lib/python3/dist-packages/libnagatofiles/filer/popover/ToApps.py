
from libnagato4.popover.item.ToStack import NagatoToStack


class NagatoToApps(NagatoToStack):

    TITLE = "Open File With..."
    WHERE_TO_GO = "apps"
