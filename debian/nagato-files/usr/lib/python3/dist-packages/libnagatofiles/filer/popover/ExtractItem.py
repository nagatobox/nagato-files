
from libnagatofiles.Mikuru import Columns
from libnagato4.popover.item.Item import NagatoItem
from libnagatofiles.archive import Extractor


class NagatoExtractItem(NagatoItem):

    TITLE = "Extract"

    def _on_clicked(self, button):
        self._raise("YUKI.N > popdown")
        yuki_tree_row = self._enquiry("YUKI.N > prime last selected row")
        yuki_path = yuki_tree_row[Columns.FULLPATH]
        Extractor.extract(self, yuki_path)
