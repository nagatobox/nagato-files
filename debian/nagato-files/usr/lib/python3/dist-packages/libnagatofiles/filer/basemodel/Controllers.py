
from libnagatofiles.filer.basemodel.Sort import NagatoSort
from libnagatofiles.filer.basemodel.TileLoader import NagatoTileLoader


class AsakuraControllers:

    def __init__(self, parent):
        NagatoSort(parent)
        NagatoTileLoader(parent)
