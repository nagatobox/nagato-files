
from libnagato4.Object import NagatoObject
from libnagatofiles.Mikuru import Columns
from libnagatofiles.filer.filter.ShowHidden import NagatoShowHidden
from libnagatofiles.filer.filter.KeyWords import NagatoKeywords
from libnagatofiles.filer.mainline.SelectionLayer import NagatoSelectionLayer


class NagatoFilterLayer(NagatoObject):

    def _inform_show_hidden(self):
        return self._show_hidden.show_hidden

    def _on_filter(self, model, iter_, user_data=None):
        yuki_file_name = model[iter_][Columns.FILE_NAME].lower()
        if not self._keywords.has_keyword():
            return self._show_hidden.is_shown(yuki_file_name)
        return self._keywords.is_shown(yuki_file_name)

    def _yuki_n_toggle_show_hidden(self):
        self._show_hidden.toggle_show_hidden()

    def _yuki_n_filter_changed(self, filter_=None):
        self._keywords.set_filter(filter_)

    def __init__(self, parent):
        self._parent = parent
        self._keywords = NagatoKeywords(self)
        self._show_hidden = NagatoShowHidden(self)
        yuki_model = self._enquiry("YUKI.N > filtered directory model")
        yuki_model.set_visible_func(self._on_filter)
        NagatoSelectionLayer(self)
