
from libnagatofiles.filer.signal import Model as Signal

SIGNAL = Signal.FORCE_SORT_COLUMN


class HaruhiSortable:

    def _on_clicked(self, tree_column):
        self._order += 1
        yuki_order = self._order % 2
        yuki_data = self._sort_column_id, yuki_order
        self._raise("YUKI.N > model signal", (SIGNAL, yuki_data))
        return True

    def _bind_sortable_column(self, sort_column_id):
        self._sort_column_id = sort_column_id
        self._order = 0
        self.set_sort_column_id(sort_column_id)
        self.connect("clicked", self._on_clicked)
