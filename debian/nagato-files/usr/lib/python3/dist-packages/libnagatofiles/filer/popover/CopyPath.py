
from gi.repository import Gtk
from gi.repository import Gdk
from libnagato4.popover.item.Item import NagatoItem


class NagatoCopyPath(NagatoItem):

    TITLE = "Copy Path"

    def _on_clicked(self, button):
        self._raise("YUKI.N > popdown")
        yuki_path = self._enquiry("YUKI.N > prime path")
        if yuki_path is None:
            return
        yuki_display = Gdk.Display.get_default()
        yuki_clipboard = Gtk.Clipboard.get_default(yuki_display)
        yuki_clipboard.set_text(yuki_path, -1)
