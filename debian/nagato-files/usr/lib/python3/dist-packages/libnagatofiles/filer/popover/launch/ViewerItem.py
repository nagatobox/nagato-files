
from gi.repository import Gtk
from libnagato4.popover.item.Item import NagatoItem


class NagatoViewerItem(NagatoItem):

    def _on_clicked(self, button):
        self._raise("YUKI.N > popdown")
        yuki_path = self._enquiry("YUKI.N > prime path")
        if yuki_path is not None:
            yuki_id = self._enquiry("YUKI.N > id")
            yuki_data = yuki_id, yuki_path
            self._raise("YUKI.N > add new viewer", yuki_data)

    def _on_initialize(self):
        yuki_label = self._enquiry("YUKI.N > label")
        self.set_label(yuki_label)
