
from gi.repository import GLib
from libnagatofiles.filer.popover.archive.Archive import NagatoArchive


class NagatoRar(NagatoArchive):

    TITLE = "rar"
    TYPE = ".rar"

    def _create_archive(self, user_data):
        # this method should be HEAVY PROCESS.
        # patoolib.create_archive(*user_data, verbosity=1, interactive=False)
        yuki_target, yuki_sources = user_data
        yuki_count = 0 # as tmp name
        for yuki_source in yuki_sources:
            print(yuki_source)
        print("create rar archive", user_data)
        return GLib.SOURCE_REMOVE
