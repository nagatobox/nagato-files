
from libnagatopath import XdgUserDirs
from libnagatofiles.Mikuru import Columns
from libnagatofiles.popover.Revealer import AbstractRevealer
from libnagatofiles.filer.popover.rename.ToRenameItem import NagatoToRenameItem


class NagatoToRename(AbstractRevealer):

    def _get_revealed(self):
        yuki_prime_row = self._enquiry("YUKI.N > prime last selected row")
        yuki_path = yuki_prime_row[Columns.FULLPATH]
        if XdgUserDirs.is_xdg_user_dir(yuki_path, True):
            return False
        yuki_file_info = yuki_prime_row[Columns.FILE_INFO]
        return yuki_file_info.get_attribute_boolean("access::can-rename")

    def _on_map(self, revealer):
        yuki_revealed = self._get_revealed()
        self.set_reveal_child(yuki_revealed)

    def _on_initialize(self):
        NagatoToRenameItem(self)
