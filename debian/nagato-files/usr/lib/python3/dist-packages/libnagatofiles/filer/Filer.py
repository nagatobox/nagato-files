
from libnagato4.Object import NagatoObject
from libnagato4.Transmitter import HaruhiTransmitter
from libnagatofiles.filer.mainline.DirectoryLayer import NagatoDirectoryLayer


class NagatoFiler(NagatoObject):

    @classmethod
    def new_for_directory(cls, parent, directory=None):
        yuki_filer = NagatoFiler(parent)
        yuki_filer.set_directory(directory)

    def _yuki_n_selection_changed(self, selection):
        self._transmitter.transmit(("selection-changed", selection))

    def _yuki_n_register_navigation_widget(self, widget):
        self._transmitter.register_listener(widget)

    def _inform_page_id(self):
        return self._page_id

    def set_directory(self, directory=None):
        NagatoDirectoryLayer(self, directory)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = HaruhiTransmitter()
        self._page_id = self._enquiry("YUKI.N > unique id")
