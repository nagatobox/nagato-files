
from libnagato4.Object import NagatoObject
from libnagatofiles.filer.signal import Model as Signal

DISPATCH = {
    Signal.SORT_COLUMN_CHANGED: "_on_sort_column_changed",
    Signal.SORT_ORDER_CHANGED: "_on_sort_order_changed",
    Signal.FORCE_SORT_COLUMN: "_on_force_sort_column"
    }


class NagatoSort(NagatoObject):

    def _on_sort_column_changed(self, column_id):
        yuki_model = self._enquiry("YUKI.N > treeview model")
        _, yuki_order = yuki_model.get_sort_column_id()
        yuki_model.set_sort_column_id(column_id, yuki_order)

    def _on_sort_order_changed(self, order):
        yuki_model = self._enquiry("YUKI.N > treeview model")
        yuki_column_id, _ = yuki_model.get_sort_column_id()
        yuki_model.set_sort_column_id(yuki_column_id, order)

    def _on_force_sort_column(self, user_data):
        yuki_model = self._enquiry("YUKI.N > treeview model")
        yuki_model.set_sort_column_id(*user_data)

    def receive_transmission(self, user_data):
        yuki_signal, yuki_data = user_data
        if yuki_signal in DISPATCH:
            yuki_method = getattr(self, DISPATCH[yuki_signal])
            yuki_method(yuki_data)

    def __init__(self, parent):
        self._parent = parent
        self._raise("YUKI.N > register model object", self)
