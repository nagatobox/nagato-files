
from libnagato4.popover.Box import NagatoBox
from libnagato4.popover.Separator import NagatoSeparator
from libnagato4.popover.main.BackToMain import NagatoBackToMain
from libnagatofiles.filer.popover.makedirectory.Model import NagatoModel


class NagatoMakeDirectoryBox(NagatoBox):

    NAME = "makedirectory"

    def _on_initialize(self):
        NagatoBackToMain(self)
        NagatoSeparator(self)
        NagatoModel(self)
