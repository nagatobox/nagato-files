
from libnagatofiles.filer.bar.button.Selection import NagatoSelection


class NagatoCopyTo(NagatoSelection):

    NAME = "edit-copy-symbolic"
    TOOLTIP = "copy to"

    def _on_clicked(self, button):
        self._raise("YUKI.N > copy selection")
