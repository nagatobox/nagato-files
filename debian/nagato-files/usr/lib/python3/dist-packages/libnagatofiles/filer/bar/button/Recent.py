
from libnagatofiles.filer.bar.button.Button import NagatoButton
from libnagatofiles.filer.popover.RecentPopover import NagatoRecentPopover


class NagatoRecent(NagatoButton):

    NAME = "document-open-recent-symbolic"

    def _on_clicked(self, *args):
        self._popover.set_relative_to(self)
        self._popover.refresh_items()
        self._popover.show_all()

    def _check_addtional_conditions(self):
        yuki_history = self._enquiry("YUKI.N > recent directories")
        self.set_sensitive(len(yuki_history) > 1)

    def receive_transmission(self, directory):
        self._check_addtional_conditions()

    def _post_initialization(self):
        self._raise("YUKI.N > register current directory object", self)
        self.set_sensitive(False)
        self._popover = NagatoRecentPopover(self)
