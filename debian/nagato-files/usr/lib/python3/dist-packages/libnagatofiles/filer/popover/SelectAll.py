
from libnagato4.popover.item.Item import NagatoItem


class NagatoSelectAll(NagatoItem):

    TITLE = "Select All"
    MESSAGE = "YUKI.N > select all"
    USER_DATA = None
