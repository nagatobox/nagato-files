
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.Mikuru import Columns
from libnagatofiles.filer.column.Interfaces import HaruhiInterfaces
from libnagatofiles.filer.column.RendererText import HaruhiRenderer


class NagatoPermission(Gtk.TreeViewColumn, NagatoObject, HaruhiInterfaces):

    def __init__(self, parent):
        self._parent = parent
        yuki_renderer = HaruhiRenderer.new()
        Gtk.TreeViewColumn.__init__(
            self,
            title="Permission",
            cell_renderer=yuki_renderer,
            text=Columns.READBLE_PERMISSION
            )
        self.set_expand(False)
        self._bind_interfaces(yuki_renderer,  Columns.READBLE_PERMISSION)
        self._raise("YUKI.N > insert column", self)
