
from libnagato4.Object import NagatoObject


class NagatoDelete(NagatoObject):

    def __call__(self, fullpath):
        yuki_tree_model = self._enquiry("YUKI.N > treeview model")
        for yuki_row in yuki_tree_model:
            if yuki_row[0] == fullpath:
                self._raise("YUKI.N > remove iter", yuki_row[0])
                yuki_tree_model.remove(yuki_row.iter)
                break

    def __init__(self, parent):
        self._parent = parent
