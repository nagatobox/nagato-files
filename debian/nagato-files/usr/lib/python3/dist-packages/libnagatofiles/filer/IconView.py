
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.Mikuru import Columns
from libnagatofiles.filer.UserInput import NagatoUserInput


class NagatoIconView(Gtk.IconView, NagatoObject):

    MODEL_REQUEST_MESSAGE = "YUKI.N > filtered directory model"

    def _inform_tree_like_widget(self):
        return self

    def _on_item_activated(self, icon_view, tree_path):
        yuki_model = icon_view.get_model()
        yuki_path = yuki_model[tree_path][Columns.FULLPATH]
        self._raise("YUKI.N > path activated", yuki_path)

    def _initialize_icon_view(self):
        Gtk.IconView.__init__(self)
        self.set_model(self._enquiry(self.MODEL_REQUEST_MESSAGE))
        self.set_pixbuf_column(Columns.ICON_IMAGE)
        self.set_selection_mode(Gtk.SelectionMode.NONE)
        self.set_columns(-1)
        self.set_tooltip_column(Columns.FILE_NAME)

    def _dummy(self, *args):
        self.stop_emission("drag_data_received")

    def __init__(self, parent):
        self._parent = parent
        self._initialize_icon_view()
        NagatoUserInput(self)
        self._raise("YUKI.N > add to scrolled window", self)
        self.connect("item-activated", self._on_item_activated)
        self.connect("drag-data-received", self._dummy)
