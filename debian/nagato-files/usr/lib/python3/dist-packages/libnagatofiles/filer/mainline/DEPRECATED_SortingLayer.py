
from libnagato4.Object import NagatoObject
from libnagatofiles.filer.mainline.UtilityLayer import NagatoUtilityLayer


class NagatoSortingLayer(NagatoObject):

    """
    def _yuki_n_sort_column_changed(self, column_id):
        yuki_model = self._enquiry("YUKI.N > treeview model")
        _, yuki_order = yuki_model.get_sort_column_id()
        yuki_model.set_sort_column_id(column_id, yuki_order)

    def _yuki_n_sort_order_changed(self, order):
        yuki_model = self._enquiry("YUKI.N > treeview model")
        yuki_column_id, _ = yuki_model.get_sort_column_id()
        yuki_model.set_sort_column_id(yuki_column_id, order)

    def _yuki_n_force_sort_column(self, user_data):
        yuki_model = self._enquiry("YUKI.N > treeview model")
        yuki_model.set_sort_column_id(*user_data)
    """

    def __init__(self, parent):
        self._parent = parent
        NagatoUtilityLayer(self)
