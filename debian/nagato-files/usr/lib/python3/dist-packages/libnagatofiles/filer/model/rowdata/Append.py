
from libnagato4.Object import NagatoObject
from libnagatofiles.Mikuru import File
from libnagatothumbnail.dummy.Loader import HaruhiLoader


class NagatoAppend(NagatoObject):

    def __call__(self, fullpath, file_info):
        yuki_file_type = file_info.get_content_type()
        yuki_is_dir = (yuki_file_type == "inode/directory")
        yuki_time_modified = file_info.get_attribute_uint64("time::modified")
        yuki_basename = file_info.get_name()
        yuki_data_tuple = (
            fullpath,
            yuki_basename,
            None,   # DEPRECATED : is-directory
            File.get_path_for_sort(yuki_basename, yuki_is_dir),
            True,
            yuki_file_type,
            File.get_access_new(file_info),
            File.get_age(yuki_time_modified),
            yuki_time_modified,
            self._loader.get_from_file_info(file_info),
            None,   # DEPRECATED : tag-type for tile building
            file_info,
            None,   # DEPRECATED : preview-type
            None,   # Unselected Tile Pixbuf. PENDING at this phase.
            None    # Selected Tile Pixbuf. PENDING at this phase.
            )
        self._raise("YUKI.N > append data", yuki_data_tuple)

    def __init__(self, parent):
        self._parent = parent
        self._loader = HaruhiLoader()
