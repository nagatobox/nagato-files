
from libnagato4.Object import NagatoObject
from libnagatofiles.Mikuru import Columns

TEMPLATE = "{} directories and {} files. {} items total."


class NagatoCount(NagatoObject):

    def _on_row_count_changed(self, *args):
        self.refresh()

    def refresh(self):
        yuki_directories = 0
        yuki_files = 0
        for yuki_row in self._enquiry("YUKI.N > filtered directory model"):
            if yuki_row[Columns.MIME_TYPE] == "inode/directory":
                yuki_directories += 1
            else:
                yuki_files += 1
        yuki_items = yuki_directories+yuki_files
        yuki_text = TEMPLATE.format(yuki_directories, yuki_files, yuki_items)
        self._raise("YUKI.N > set text", yuki_text)

    def __init__(self, parent):
        self._parent = parent
        yuki_model = self._enquiry("YUKI.N > filtered directory model")
        yuki_model.connect("row-inserted", self._on_row_count_changed)
        yuki_model.connect("row-deleted", self._on_row_count_changed)
        self.refresh()
