
from libnagato4.Object import NagatoObject
from libnagatofiles.Mikuru import Columns


class NagatoDifference(NagatoObject):

    def _compare(self, list_alpha, list_beta):
        yuki_diff_set = set(list_alpha) - set(list_beta)
        return len(yuki_diff_set)

    def _swap_tile(self, tree_row):
        if not tree_row[Columns.SELECTED]:
            yuki_image = tree_row[Columns.SELECTED_TILE]
        else:
            yuki_image = tree_row[Columns.UNSELECTED_TILE]
        if yuki_image is not None:
            tree_row[Columns.ICON_IMAGE] = yuki_image

    def _on_row_changed(self, model, tree_path, tree_iter):
        yuki_diff_count = 0
        self._old = self._new
        self._new = self._enquiry("YUKI.N > selection")
        yuki_diff_count += self._compare(self._old, self._new)
        yuki_diff_count += self._compare(self._new, self._old)
        if yuki_diff_count == 0:
            return
        self._swap_tile(model[tree_path])
        self._raise("YUKI.N > selection changed", self._new)

    def __init__(self, parent):
        self._parent = parent
        self._old = []
        self._new = []
        yuki_model = self._enquiry("YUKI.N > treeview model")
        yuki_model.connect("row-changed", self._on_row_changed)
