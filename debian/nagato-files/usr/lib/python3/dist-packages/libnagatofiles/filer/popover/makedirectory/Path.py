
from gi.repository import GLib
from libnagato4.Object import NagatoObject


class NagatoPath(NagatoObject):

    def get_path(self):
        yuki_names = [self._current_directory, self._basename]
        return GLib.build_filenamev(yuki_names)

    def get_appliable(self):
        if self._basename == "":
            return False
        return not GLib.file_test(self.get_path(), GLib.FileTest.EXISTS)

    def reset_path(self, basename):
        self._basename = basename

    def reset_directory(self):
        self._current_directory = self._enquiry("YUKI.N > current directory")

    def __init__(self, parent):
        self._parent = parent
        self._directory = ""
        self._basename = ""
