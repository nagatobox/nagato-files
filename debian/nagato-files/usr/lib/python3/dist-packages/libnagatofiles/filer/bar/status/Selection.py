
from libnagato4.Object import NagatoObject


class NagatoSelection(NagatoObject):

    def _refresh_selection(self, selection):
        if selection:
            yuki_text = "{} items are selected".format(len(selection))
            self._raise("YUKI.N > set text", yuki_text)
        else:
            self._raise("YUKI.N > selection cleared")

    def receive_transmission(self, user_data):
        yuki_type, yuki_data = user_data
        if yuki_type == "selection-changed":
            self._refresh_selection(yuki_data)

    def __init__(self, parent):
        self._parent = parent
        self._raise("YUKI.N > register navigation widget", self)
