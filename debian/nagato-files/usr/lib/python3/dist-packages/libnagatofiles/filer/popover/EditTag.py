
from libnagatofiles.Mikuru import Columns
from libnagatofiles.popover.Revealer import AbstractRevealer
from libnagatofiles.filer.popover.EditTagItem import NagatoEditTagItem


class NagatoEditTag(AbstractRevealer):

    def _on_map(self, revealer):
        yuki_tree_row = self._enquiry("YUKI.N > prime last selected row")
        yuki_type = yuki_tree_row[Columns.MIME_TYPE]
        yuki_revealed = yuki_type in ("audio/mpeg", "audio/mp4")
        revealer.set_reveal_child(yuki_revealed)

    def _on_initialize(self):
        NagatoEditTagItem(self)
