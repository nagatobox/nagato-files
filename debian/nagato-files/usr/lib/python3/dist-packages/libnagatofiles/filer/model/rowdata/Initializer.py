
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagatofiles.Mikuru import SourcePriority
from libnagatofiles.filer.model.rowdata.Append import NagatoAppend
from libnagatofiles.filer.signal import Model as Signal

PRIORITY = SourcePriority.FILE_PARSING


class NagatoInitializer(NagatoObject):

    def _on_finished(self, gio_file, task):
        yuki_enumerator = gio_file.enumerate_children_finish(task)
        yuki_directory = gio_file.get_path()
        for yuki_file_info in yuki_enumerator:
            yuki_names = [yuki_directory, yuki_file_info.get_name()]
            yuki_fullpath = GLib.build_filenamev(yuki_names)
            self._append(yuki_fullpath, yuki_file_info)
        yuki_enumerator.close()
        self._raise("YUKI.N > model signal", (Signal.DIRECTORY_REALIZED, None))
        # self._raise("YUKI.N > directory realized")

    def refresh(self):
        self._raise("YUKI.N > clear base model")
        yuki_gio_file = self._enquiry("YUKI.N > gio file")
        yuki_args = "*", 0, PRIORITY, None, self._on_finished
        yuki_gio_file.enumerate_children_async(*yuki_args)

    def __init__(self, parent):
        self._parent = parent
        self._append = NagatoAppend(self)
