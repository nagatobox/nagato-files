
from libnagatofiles.Mikuru import Columns
from libnagatofiles.filer.popover.launch.Viewer import AbstractViewer


class NagatoMediaViewer(AbstractViewer):

    LABEL = "Show in Media Viewer"
    ID = "media-viewer"

    def _on_map(self, revealer):
        yuki_tree_row = self._enquiry("YUKI.N > prime last selected row")
        yuki_type = yuki_tree_row[Columns.MIME_TYPE]
        yuki_header = yuki_type.split("/")[0]
        yuki_revealed = yuki_header in ("video, audio")
        revealer.set_reveal_child(yuki_revealed)
