
from gi.repository import Gio
from libnagato4.Object import NagatoObject
from libnagatofiles.filer.model.rowdata.Initializer import NagatoInitializer
from libnagatofiles.filer.model.rowdata.Monitor import NagatoMonitor


class NagatoObserver(NagatoObject):

    def _inform_gio_file(self):
        return self._gio_file

    def receive_transmission(self, directory):
        self._gio_file = Gio.File.new_for_path(directory)
        self._initializer.refresh()
        self._monitor.restart()

    def __init__(self, parent):
        self._parent = parent
        self._gio_file = None
        self._initializer = NagatoInitializer(self)
        self._monitor = NagatoMonitor(self)
        self._raise("YUKI.N > register current directory object", self)
