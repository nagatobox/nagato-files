
from libnagatofiles.filer.popover.MultiSelection import NagatoMultiSelection


class NagatoUnselectBefore(NagatoMultiSelection):

    METHOD = "iter_previous"
    SELECTION = True
    TITLE = "Unselect Before"
