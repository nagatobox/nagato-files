
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.filer.popover.launch.ViewerItem import NagatoViewerItem


class AbstractViewer(Gtk.Revealer, NagatoObject):

    LABEL = "define label here."
    ID = "define id here"

    def _inform_label(self):
        return self.LABEL

    def _inform_id(self):
        return self.ID

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)

    def _on_map(self, revealer):
        raise NotImplementedError

    def __init__(self, parent):
        self._parent = parent
        Gtk.Revealer.__init__(self)
        self.set_transition_type(Gtk.RevealerTransitionType.NONE)
        NagatoViewerItem(self)
        self.set_reveal_child(False)
        self.connect("map", self._on_map)
        self._raise("YUKI.N > add to box", self)
