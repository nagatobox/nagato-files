
from libnagato4.Object import NagatoObject
from gi.repository import GLib
from gi.repository import Gio
from libnagatofiles.Mikuru import SafePath2


class NagatoCopyDirectory(NagatoObject):

    def _get_destination_file(self, directory, source_path):
        yuki_basename = GLib.path_get_basename(source_path)
        yuki_target = GLib.build_filenamev([directory, yuki_basename])
        yuki_safe_path = SafePath2.get_path(yuki_target)
        return Gio.File.new_for_path(yuki_safe_path)

    def to_directory(self, directory, source_path):
        yuki_dest_file = self._get_destination_file(directory, source_path)
        yuki_dest_file.make_directory(None)
        yuki_dest_dir = yuki_dest_file.get_path()
        yuki_source_file = Gio.File.new_for_path(source_path)
        for yuki_file_info in yuki_source_file.enumerate_children("*", 0):
            yuki_names = [source_path, yuki_file_info.get_name()]
            yuki_path = GLib.build_filenamev(yuki_names)
            yuki_data = yuki_dest_dir, yuki_path
            self._raise("YUKI.N > directory found", yuki_data)

    def __init__(self, parent):
        self._parent = parent
