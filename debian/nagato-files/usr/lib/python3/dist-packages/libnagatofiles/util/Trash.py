
from gi.repository import Gtk
from gi.repository import Gio
from libnagatofiles.util.FileManeuver import NagatoFileManeuver
from libnagatofiles.dialog.trash.Trash import NagatoTrash as Dialog


class NagatoTrash(NagatoFileManeuver):

    def _send_to(self, selection, directory=None):
        for yuki_fullpath in selection:
            yuki_file = Gio.File.new_for_path(yuki_fullpath)
            yuki_exit_signal = yuki_file.trash(None)
            print(yuki_exit_signal)
        self._raise("YUKI.N > clear selection")

    def _confirm(self):
        if Gtk.ResponseType.APPLY == Dialog.get_response(self):
            yuki_selection = self._enquiry("YUKI.N > selection")
            self._send_to(yuki_selection, None)
