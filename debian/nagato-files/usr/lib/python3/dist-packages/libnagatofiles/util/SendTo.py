
from gi.repository import Gio
from gi.repository import GLib
from gi.repository import Gtk
from libnagato4.chooser.context.SelectDirectory import NagatoContext
from libnagato4.chooser.selectfile.Dialog import NagatoDialog
from libnagatofiles.Mikuru import SafePath2
from libnagatofiles.util.FileManeuver import NagatoFileManeuver

COPY_FLAG = Gio.FileCopyFlags.ALL_METADATA


class NagatoSendTo(NagatoFileManeuver):

    def _send_to(self, directory):
        for yuki_fullpath in self._enquiry("YUKI.N > selection"):
            yuki_source_gio_file = Gio.File.new_for_path(yuki_fullpath)
            yuki_names = [directory, yuki_source_gio_file.get_basename()]
            yuki_target_path = GLib.build_filenamev(yuki_names)
            yuki_safe_path = SafePath2.get_path(yuki_target_path)
            yuki_destination_gio_file = Gio.File.new_for_path(yuki_safe_path)
            yuki_source_gio_file.move(yuki_destination_gio_file, COPY_FLAG)
        self._raise("YUKI.N > clear selection")

    def _confirm(self):
        yuki_directory = self._enquiry("YUKI.N > current directory")
        yuki_context = NagatoContext.new_for_directory(self, yuki_directory)
        yuki_response = NagatoDialog.run_with_context(self, yuki_context)
        if yuki_response == Gtk.ResponseType.APPLY:
            self._send_to(yuki_context["selection"])
