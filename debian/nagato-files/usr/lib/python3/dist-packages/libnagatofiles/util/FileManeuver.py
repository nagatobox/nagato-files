
from libnagato4.Object import NagatoObject


class NagatoFileManeuver(NagatoObject):

    def _confirm(self):
        print("YUKI.N > do not look at me.")

    def execute(self):
        yuki_selection = self._enquiry("YUKI.N > selection")
        if yuki_selection:
            self._confirm()

    def _on_initialize(self):
        pass

    def __init__(self, parent):
        self._parent = parent
        self._on_initialize()
