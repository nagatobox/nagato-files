
from gi.repository import Gtk
from libnagato4.chooser.context.SelectDirectory import NagatoContext
from libnagato4.chooser.selectfile.Dialog import NagatoDialog
from libnagatofiles.util.FileManeuver import NagatoFileManeuver
from libnagatofiles.util.CopyDispatch import NagatoCopyDispatch


class NagatoCopyTo(NagatoFileManeuver):

    def _send_to(self, directory):
        for yuki_fullpath in self._enquiry("YUKI.N > selection"):
            self._dispatch.dispatch(directory, yuki_fullpath)
        self._raise("YUKI.N > clear selection")

    def _on_initialize(self):
        self._dispatch = NagatoCopyDispatch(self)

    def _confirm(self):
        yuki_directory = self._enquiry("YUKI.N > current directory")
        yuki_context = NagatoContext.new_for_directory(self, yuki_directory)
        yuki_response = NagatoDialog.run_with_context(self, yuki_context)
        if yuki_response == Gtk.ResponseType.APPLY:
            self._send_to(yuki_context["selection"])
