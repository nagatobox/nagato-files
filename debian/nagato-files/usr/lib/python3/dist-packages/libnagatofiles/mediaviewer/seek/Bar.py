
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.mediaviewer.seek.Painters import NagatoPainters
from libnagatofiles.mediaviewer.seek.MouseMotion import NagatoMouseMotion


class NagatoBar(Gtk.DrawingArea, NagatoObject):

    def _inform_allocated_size(self):
        yuki_size, _ = self.get_allocated_size()
        return yuki_size

    def _inform_pointed_position(self):
        return self._mouse_motion.pointed_position

    def _yuki_n_queue_draw(self):
        self.queue_draw()

    def _on_draw(self, drawing_area, cairo_context):
        self._painters.paint(cairo_context)

    def __init__(self, parent):
        self._parent = parent
        Gtk.DrawingArea.__init__(self)
        self.set_hexpand(True)
        self._mouse_motion = NagatoMouseMotion.new_for_event_source(self)
        self._painters = NagatoPainters(self)
        self.connect("draw", self._on_draw)
        self._raise("YUKI.N > add to box", self)
