
from libnagato4.Object import NagatoObject
from libnagatofiles.mediaviewer.PathsLayer import NagatoPathsLayer


class NagatoMediaViewer(NagatoObject):

    @classmethod
    def new_for_path(cls, parent, path):
        yuki_media_viewer = NagatoMediaViewer(parent)
        yuki_media_viewer.set_path(path)

    def _inform_page_id(self):
        return "media-viewer"

    def set_path(self, path):
        self._paths_layer.set_path(path)

    def __init__(self, parent):
        self._parent = parent
        self._paths_layer = NagatoPathsLayer(self)
