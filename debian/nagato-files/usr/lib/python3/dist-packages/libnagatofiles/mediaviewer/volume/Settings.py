
from libnagato4.Object import NagatoObject
from libnagatofiles.mediaviewer.volume import Icon


class NagatoSettings(NagatoObject):

    def _set_attributes(self, rate):
        yuki_button = self._enquiry("YUKI.N > button")
        yuki_button.set_image(Icon.get_for_rate(rate))
        yuki_button.props.tooltip_text = "{0:.0%}".format(rate)

    def get_rate(self):
        yuki_query = "player", "software_volume", 0.2
        return self._enquiry("YUKI.N > settings", yuki_query)

    def receive_transmission(self, user_data):
        yuki_type, yuki_key, yuki_value = user_data
        if yuki_type == "player" and yuki_key == "software_volume":
            self._set_attributes(float(yuki_value))

    def __init__(self, parent):
        self._parent = parent
        yuki_rate = self.get_rate()
        self._set_attributes(yuki_rate)
        self._raise("YUKI.N > register settings object", self)
