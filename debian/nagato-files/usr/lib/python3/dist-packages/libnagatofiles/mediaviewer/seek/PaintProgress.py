
from libnagato4.Ux import Unit
from libnagato4.Object import NagatoObject

HEIGHT = Unit(1)/2


class NagatoPaintProgress(NagatoObject):

    def paint(self, cairo_context):
        yuki_size = self._enquiry("YUKI.N > allocated size")
        yuki_current, yuki_duration = self._enquiry("YUKI.N > timing")
        yuki_rate = yuki_current/yuki_duration
        yuki_rgba = self._enquiry("YUKI.N > rgba")
        cairo_context.set_source_rgba(*yuki_rgba)
        cairo_context.rectangle(
            0,
            yuki_size.height - HEIGHT,
            yuki_size.width * yuki_rate,
            HEIGHT
            )
        cairo_context.fill()

    def __init__(self, parent):
        self._parent = parent
