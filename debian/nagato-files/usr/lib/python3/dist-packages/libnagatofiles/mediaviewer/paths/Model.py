
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.mediaviewer.paths import Columns
from libnagatofiles.mediaviewer.paths.DirectoryReader import (
    NagatoDirectoryReader
    )


class NagatoModel(Gtk.ListStore, NagatoObject):

    def _yuki_n_append(self, user_data):
        yuki_matched, yuki_row_data = user_data
        yuki_tree_iter = self.append(yuki_row_data)
        if yuki_matched:
            self._current_iter = yuki_tree_iter

    def set_path(self, path):
        self.clear()
        self._directory_reader.non_recursive(path)
        yuki_path = self[self._current_iter][Columns.FULLPATH]
        self._raise("YUKI.N > path realized", yuki_path)

    def shift_path(self, shift):
        yuki_current_path = self.get_path(self._current_iter)
        yuki_current_index = int(yuki_current_path.to_string())
        yuki_new_index = (yuki_current_index+shift)%len(self)
        self._current_iter = self.get_iter(yuki_new_index)
        yuki_path = self[self._current_iter][Columns.FULLPATH]
        self._raise("YUKI.N > path realized", yuki_path)

    def __init__(self, parent):
        self._parent = parent
        self._current_iter = None
        Gtk.ListStore.__init__(self, *Columns.TYPES)
        self.set_sort_column_id(Columns.PATH_FOR_SORT, Gtk.SortType.ASCENDING)
        self._directory_reader = NagatoDirectoryReader(self)
