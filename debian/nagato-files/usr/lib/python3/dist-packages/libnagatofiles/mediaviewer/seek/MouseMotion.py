
from libnagato4.Object import NagatoObject
from libnagatofiles.mediaviewer.seek import MouseEvents
from libnagatofiles.mediaviewer.signal import Playback as Signal


class NagatoMouseMotion(NagatoObject):

    @classmethod
    def new_for_event_source(cls, parent_as_event_source):
        yuki_mouse_motion = NagatoMouseMotion(parent_as_event_source)
        yuki_mouse_motion.set_events(parent_as_event_source)
        return yuki_mouse_motion

    def _on_motion_notify(self, widget, event):
        yuki_width = widget.get_allocated_width()
        self._position = event.x/yuki_width
        self._raise("YUKI.N > queue draw")

    def _on_leave_notify(self, widget, event):
        self._position = None

    def _on_button_press(self, widget, event):
        yuki_width = widget.get_allocated_width()
        yuki_rate = event.x/yuki_width
        yuki_data = Signal.SEEK, yuki_rate
        self._raise("YUKI.N > playback signal", yuki_data)

    def set_events(self, event_source):
        event_source.set_events(MouseEvents.EVENTS)
        event_source.connect("motion-notify-event", self._on_motion_notify)
        event_source.connect("leave-notify-event", self._on_leave_notify)
        event_source.connect("button-press-event", self._on_button_press)

    def __init__(self, parent):
        self._parent = parent
        self._position = None

    @property
    def pointed_position(self):
        return self._position
