
from gi.repository import Gtk
from libnagato4.Ux import Unit
from libnagato4.popover.SingleStack import AbstractSingleStack
from libnagatofiles.mediaviewer.volume.Scale import NagatoScale

SIZE = Unit(3), Unit(30)


class NagatoPopover(AbstractSingleStack):

    def _inform_size(self):
        return SIZE

    def _add_to_single_stack(self):
        self.set_position(Gtk.PositionType.TOP)
        NagatoScale(self)
