
from gi.repository import Gtk
from libnagatofiles.filer.bar.button.Button import NagatoButton
from libnagatofiles.mediaviewer.signal import Playback as Signal


class NagatoPause(NagatoButton):

    NAME = "media-playback-pause-symbolic"
    TOOLTIP = "Pause"
    SIZE = Gtk.IconSize.SMALL_TOOLBAR

    def _on_clicked(self, button):
        yuki_data = Signal.PAUSE, None
        self._raise("YUKI.N > playback signal", yuki_data)
