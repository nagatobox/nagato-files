
from gi.repository import Gdk
from libnagato4.Object import NagatoObject

COLOR_QUERY = "css", "highlight_bg_color", "rgba(96, 114, 129, 1)"


class NagatoColor(NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        yuki_color = self._enquiry("YUKI.N > settings", COLOR_QUERY)
        yuki_rgba = Gdk.RGBA()
        yuki_rgba.parse(yuki_color)
        self._rgba = (
            yuki_rgba.red,
            yuki_rgba.green,
            yuki_rgba.blue,
            yuki_rgba.alpha
            )

    @property
    def rgba(self):
        return self._rgba
