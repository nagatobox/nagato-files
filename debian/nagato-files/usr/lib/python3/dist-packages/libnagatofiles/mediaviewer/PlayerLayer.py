
from libnagato4.Object import NagatoObject
from libnagato4.Transmitter import HaruhiTransmitter
from libnagatofiles.mediaviewer.WidgetsLayer import NagatoWidgetsLayer
from libnagatofiles.mediaviewer.player.Player import NagatoPlayer


class NagatoPlayerLayer(NagatoObject):

    def _yuki_n_playback_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def _yuki_n_drawing_area_syncable(self, xid):
        self._player = NagatoPlayer(self, xid)

    def _yuki_n_register_playback_object(self, object_):
        self._transmitter.register_listener(object_)

    def __init__(self, parent):
        self._parent = parent
        self._state = None
        self._transmitter = HaruhiTransmitter()
        NagatoWidgetsLayer(self)
