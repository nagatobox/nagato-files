
from libnagato4.widget.VBox import AbstractVBox
from libnagatofiles.mediaviewer.DrawingArea import NagatoDrawingArea
from libnagatofiles.mediaviewer.ActionBar import NagatoActionBar


class NagatoPageBox(AbstractVBox):

    def open_directory(self, directory):
        self._raise("YUKI.N > add new tab", directory)

    def get_id(self):
        return self._enquiry("YUKI.N > page id")

    def set_path(self, path):
        self._raise("YUKI.N > new path", path)

    def _on_initialize(self):
        NagatoDrawingArea(self)
        NagatoActionBar(self)
