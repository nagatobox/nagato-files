
from gi.repository import PangoCairo
from libnagato4.Object import NagatoObject
from libnagatofiles.mediaviewer.seek.Layout import NagatoLayout


class NagatoPaintMarkup(NagatoObject):

    def paint(self, cairo_context):
        yuki_current, yuki_duration = self._enquiry("YUKI.N > timing")
        cairo_context.set_source_rgba(0, 0, 0, 1)
        yuki_response = self._layout.get_for_context(cairo_context)
        yuki_layout, yuki_y_offset = yuki_response
        cairo_context.move_to(0, yuki_y_offset)
        PangoCairo.update_layout(cairo_context, yuki_layout)
        PangoCairo.show_layout(cairo_context, yuki_layout)

    def __init__(self, parent):
        self._parent = parent
        self._layout = NagatoLayout(self)
