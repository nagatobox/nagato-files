
from gi.repository import Gdk

POINTER_MOTION = Gdk.EventMask.POINTER_MOTION_MASK
BUTTON_PRESS = Gdk.EventMask.BUTTON_PRESS_MASK
LEAVE_NOTIFY = Gdk.EventMask.LEAVE_NOTIFY_MASK
EVENTS = POINTER_MOTION | BUTTON_PRESS | LEAVE_NOTIFY

