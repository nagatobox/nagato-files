
from gi.repository import Gst
from gi.repository import GstPlayer
from gi.repository.GstPlayer import PlayerVideoOverlayVideoRenderer
from libnagato4.Object import NagatoObject
from libnagatofiles.mediaviewer.player.Events import NagatoEvents
from libnagatofiles.mediaviewer.player.Receivers import AsakuraReceivers


class NagatoPlayer(NagatoObject):

    def _inform_player(self):
        return self._player

    def _initialize_player(self, handle):
        Gst.init()
        yuki_overlay = PlayerVideoOverlayVideoRenderer.new(handle)
        self._player = GstPlayer.Player.new(yuki_overlay, None)
        self._player.set_visualization_enabled(True)
        self._player.set_visualization("libvisual_infinite")

    def __init__(self, parent, handle):
        self._parent = parent
        self._initialize_player(handle)
        AsakuraReceivers(self)
        NagatoEvents(self)
