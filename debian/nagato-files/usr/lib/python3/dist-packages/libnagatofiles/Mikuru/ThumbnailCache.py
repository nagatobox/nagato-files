
from gi.repository import Gio

ATTRIBUTE = "metadata::nagato-thumbnail-path"


def save_wait(thumbnail, source_path):
    yuki_gio_file = Gio.File.new_for_path(source_path)
    yuki_file_info = yuki_gio_file.query_info("*", 0, None)
    yuki_cache_path = yuki_file_info.get_attribute_as_string(ATTRIBUTE)
    thumbnail.savev(yuki_cache_path, "png", [], [])
    yuki_file_info.set_attribute_string(ATTRIBUTE, yuki_cache_path)
