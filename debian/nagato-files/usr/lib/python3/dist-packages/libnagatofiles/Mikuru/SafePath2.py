
import re
from gi.repository import GLib
from gi.repository import Gio


def _get_stem_and_index(stem):
    yuki_match = re.search(r"\((\d+)\)\Z", stem)
    if yuki_match is None:
        return stem, 0
    yuki_matched_string = yuki_match.group()
    yuki_index = int(yuki_matched_string[1:-1])
    return stem[:-1*len(yuki_matched_string)], yuki_index


def _build(directory, stem, index, suffix):
    while True:
        index += 1
        yuki_basename = "{}({}){}".format(stem, index, suffix)
        yuki_path = GLib.build_filenamev([directory, yuki_basename])
        if not GLib.file_test(yuki_path, GLib.FileTest.EXISTS):
            return yuki_path


def _get_numbered_name(destination):
    yuki_gio_file = Gio.File.new_for_path(destination)
    yuki_directory = yuki_gio_file.get_parent().get_path()
    yuki_basename = yuki_gio_file.get_basename()
    yuki_names = yuki_basename.rsplit(".", 1)
    yuki_stem, yuki_index = _get_stem_and_index(yuki_names[0])
    yuki_suffix = "" if len(yuki_names) == 1 else "."+yuki_names[1]
    return _build(yuki_directory, yuki_stem, yuki_index, yuki_suffix)


def get_path(destination):
    if not GLib.file_test(destination, GLib.FileTest.EXISTS):
        return destination
    return _get_numbered_name(destination)
