
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.pdfviewer.model import Columns
from libnagatofiles.pdfviewer.model.ContentSize import NagatoContentSize
from libnagatofiles.pdfviewer.model.Document import NagatoDocument
from libnagatofiles.pdfviewer.model.PixbufLoader import NagatoPixbufLoader


class NagatoListStore(Gtk.ListStore, NagatoObject):

    def _inform_allocated_width(self):
        return self._content_size.width

    def _yuki_n_append(self, user_data):
        self.append(user_data)

    def receive_transmission(self, path):
        self.clear()
        self._document.reset_path(path)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ListStore.__init__(self, *Columns.TYPES)
        self._content_size = NagatoContentSize(self)
        self._document = NagatoDocument(self)
        NagatoPixbufLoader(self)
        self._raise("YUKI.N > register path object", self)
