
from gi.repository import Gio
from gi.repository import GLib
from gi.repository import Poppler
from libnagato4.Object import NagatoObject
from libnagato4.preview.pdf.Pixbuf import NagatoPixbuf


class NagatoDocument(NagatoObject):

    def _append(self, document, path):
        yuki_page = document.get_page(self._index)
        if yuki_page is None or path != self._path:
            return GLib.SOURCE_REMOVE
        if 100 >= self._index:
            yuki_pixbuf = self._pixbuf.get_from_page(yuki_page)
            yuki_data = self._index+1, yuki_page, yuki_pixbuf
        else:
            yuki_data = self._index+1, yuki_page, None
        self._raise("YUKI.N > append", yuki_data)
        self._index += 1
        return document.get_n_pages() > self._index

    def reset_path(self, path):
        self._path = path
        yuki_gio_file = Gio.File.new_for_path(path)
        yuki_document = Poppler.Document.new_from_gfile(yuki_gio_file)
        self._index = 0
        GLib.timeout_add(100, self._append, yuki_document, path)

    def __init__(self, parent):
        self._parent = parent
        self._pixbuf = NagatoPixbuf(self)
