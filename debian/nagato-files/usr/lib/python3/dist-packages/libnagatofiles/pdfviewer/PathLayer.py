
from libnagato4.Object import NagatoObject
from libnagato4.Transmitter import HaruhiTransmitter
from libnagatofiles.pdfviewer.ModelLayer import NagatoModelLayer


class NagatoPathLayer(NagatoObject):

    def _yuki_n_new_path(self, path):
        self.set_path(path)

    def _yuki_n_register_path_object(self, object_):
        self._transmitter.register_listener(object_)

    def _inform_path(self):
        return self._path

    def set_path(self, path):
        self.path = path
        self._transmitter.transmit(path)

    def __init__(self, parent):
        self._parent = parent
        self._path = None
        self._transmitter = HaruhiTransmitter()
        NagatoModelLayer(self)
