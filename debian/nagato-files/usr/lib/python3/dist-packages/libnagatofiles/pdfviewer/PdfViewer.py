
from libnagato4.Object import NagatoObject
from libnagatofiles.pdfviewer.PathLayer import NagatoPathLayer


class NagatoPdfViewer(NagatoObject):

    @classmethod
    def new_for_path(cls, parent, path):
        yuki_image_viewer = NagatoPdfViewer(parent)
        yuki_image_viewer.set_path(path)

    def _inform_page_id(self):
        return "pdf-viewer"

    def set_path(self, path):
        self._path.set_path(path)

    def __init__(self, parent):
        self._parent = parent
        self._path = NagatoPathLayer(self)
