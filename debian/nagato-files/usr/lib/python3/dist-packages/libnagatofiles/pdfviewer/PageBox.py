
from libnagato4.widget.VBox import AbstractVBox
from libnagatofiles.pdfviewer.content.Content import NagatoContent
# from libnagatofiles.imageviewer.bar.Bar import NagatoBar


class NagatoPageBox(AbstractVBox):

    def _on_destroy(self, *args):
        self._raise("YUKI.N > clear all")

    def open_directory(self, directory):
        self._raise("YUKI.N > add new tab", directory)

    def get_id(self):
        return self._enquiry("YUKI.N > page id")

    def set_path(self, path):
        self._raise("YUKI.N > new path", path)

    def _on_initialize(self):
        self.set_hexpand(True)
        self.set_vexpand(True)
        self.connect("destroy", self._on_destroy)
        NagatoContent(self)
        # NagatoBar(self)
