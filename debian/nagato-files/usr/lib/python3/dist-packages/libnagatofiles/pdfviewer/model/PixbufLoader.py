
from libnagato4.Object import NagatoObject
from libnagatofiles.pdfviewer.signal import Document as Signal
from libnagatofiles.pdfviewer.model import Columns
from libnagato4.preview.pdf.Pixbuf import NagatoPixbuf


class NagatoPixbufLoader(NagatoObject):

    def _load_pixbuf(self, model, begin, end):
        for yuki_path in range(begin, end+1):
            yuki_row = model[yuki_path]
            if yuki_row[Columns.PIXBUF] is not None:
                continue
            yuki_page = yuki_row[Columns.POPPLER_PAGE]
            yuki_pixbuf = self._pixbuf.get_from_page(yuki_page)
            yuki_row[Columns.PIXBUF] = yuki_pixbuf.copy()

    def receive_transmission(self, user_data):
        yuki_signal, yuki_data = user_data
        if yuki_signal != Signal.VISIBLE_AREA_CHANGED:
            return
        yuki_begin, yuki_end = yuki_data
        yuki_model = self._enquiry("YUKI.N > model")
        yuki_end = min(yuki_end+20, len(yuki_model)-1)
        self._load_pixbuf(yuki_model, yuki_begin, yuki_end)

    def __init__(self, parent):
        self._parent = parent
        self._pixbuf = NagatoPixbuf(self)
        self._raise("YUKI.N > register document object", self)
