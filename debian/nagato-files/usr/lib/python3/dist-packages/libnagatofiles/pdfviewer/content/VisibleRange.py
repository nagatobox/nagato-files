
from libnagato4.Object import NagatoObject
from libnagatofiles.pdfviewer.signal import Document as Signal


class NagatoVisibleRange(NagatoObject):

    def update_buffer(self, icon_view):
        if (yuki_result := icon_view.get_visible_range()) is None:
            return
        yuki_model = self._enquiry("YUKI.N > model")
        yuki_begin_path, yuki_end_path = yuki_result
        yuki_begin = int(yuki_begin_path.to_string())
        yuki_end = int(yuki_end_path.to_string())
        if yuki_begin != self._begin or yuki_end != self._end:
            if (yuki_end)//20*20 >= self._loaded:
                self._loaded += 20
                yuki_data = Signal.VISIBLE_AREA_CHANGED, (yuki_begin, yuki_end)
                self._raise("YUKI.N > document signals", yuki_data)
        self._begin = yuki_begin
        self._end = yuki_end

    def __init__(self, parent):
        self._parent = parent
        self._loaded = 100
        self._begin = -1
        self._end = -1
