
from libnagato4.mainline.MainPopoverLayer import AbstractMainPopoverLayer
from libnagatofiles.mainline.HeaderLayer import NagatoHeaderLayer


class NagatoMainPopoverLayer(AbstractMainPopoverLayer):

    def _on_initialize(self):
        NagatoHeaderLayer(self)
