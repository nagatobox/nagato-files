
from libnagato4.mainline.MainGridLayer import AbstractMainGridLayer
from libnagatofiles.filesystem.layer.PortalLayer import NagatoPortalLayer


class NagatoMainGridLayer(AbstractMainGridLayer):

    def _yuki_n_loopback_add_main_widget(self, parent):
        NagatoPortalLayer(parent)
