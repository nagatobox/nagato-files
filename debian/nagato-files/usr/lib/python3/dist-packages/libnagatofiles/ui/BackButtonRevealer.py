
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.ui.BackButton import NagatoBackButton


class NagatoBackButtonRevealer(Gtk.Revealer, NagatoObject):

    def _yuki_n_add_to_revealer(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Revealer.__init__(self)
        NagatoBackButton(self)
        self.set_reveal_child(False)
        self._raise("YUKI.N > add to box", self)
