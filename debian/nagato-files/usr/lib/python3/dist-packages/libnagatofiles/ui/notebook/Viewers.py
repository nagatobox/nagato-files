
from libnagatofiles.mediaviewer.MediaViewer import NagatoMediaViewer
from libnagatofiles.imageviewer.ImageViewer import NagatoImageViewer
from libnagatofiles.ui.notebook.ViewerLauncher import HaruhiViewerLauncher


class HaruhiViewers:

    @classmethod
    def new_for_notebook(cls, notebook):
        return HaruhiViewers(notebook)

    def _try_show_page(self, type_, fullpath):
        for yuki_position in range(0, self._notebook.get_n_pages()):
            yuki_page_box = self._notebook.get_nth_page(yuki_position)
            if type_ == yuki_page_box.get_id():
                self._notebook.set_current_page(yuki_position)
                yuki_page_box.set_path(fullpath)
                return True
        return False

    def launch(self, user_data):
        yuki_type, yuki_path = user_data
        if self._try_show_page(yuki_type, yuki_path):
            return
        self._launcher.launch(yuki_type, yuki_path)

    def __init__(self, notebook):
        self._notebook = notebook
        self._launcher = HaruhiViewerLauncher(notebook)
