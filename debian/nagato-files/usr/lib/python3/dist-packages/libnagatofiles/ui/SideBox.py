
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.ui.SideStack import NagatoSideStack
from libnagatofiles.ui.BackButtonRevealer import NagatoBackButtonRevealer


class NagatoSideBox(Gtk.Box, NagatoObject):

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)

    def show_preview(self, fullpath):
        self._side_stack.show_preview(fullpath)

    def _yuki_n_back_to_bookmark(self):
        self._side_stack.back_to_bookmark()
        self._back_button_revealer.set_reveal_child(False)

    def _yuki_n_stack_switched(self, name):
        self._back_button_revealer.set_reveal_child(True)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self._back_button_revealer = NagatoBackButtonRevealer(self)
        self._side_stack = NagatoSideStack(self)
        self._raise("YUKI.N > add to paned 1", self)
