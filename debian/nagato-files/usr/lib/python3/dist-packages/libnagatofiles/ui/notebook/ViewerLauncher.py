
from libnagato4.Object import NagatoObject
from libnagatofiles.mediaviewer.MediaViewer import NagatoMediaViewer
from libnagatofiles.imageviewer.ImageViewer import NagatoImageViewer
from libnagatofiles.pdfviewer.PdfViewer import NagatoPdfViewer

VIEWERS = {
    "media-viewer": NagatoMediaViewer,
    "image-viewer": NagatoImageViewer,
    "pdf-viewer": NagatoPdfViewer
    }


class HaruhiViewerLauncher(NagatoObject):

    def launch(self, type_, path):
        if type_ in VIEWERS:
            yuki_class = VIEWERS[type_]
            yuki_class.new_for_path(self._notebook, path)

    def __init__(self, notebook):
        self._notebook = notebook
