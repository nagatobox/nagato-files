
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.ui.TabVisibility import HaruhiTabVisibility
from libnagatofiles.filer.Filer import NagatoFiler
from libnagatofiles.ui.GlobalAction import NagatoGlobalAction
from libnagatofiles.mediaviewer.MediaViewer import NagatoMediaViewer
from libnagatofiles.trash.model.PortalLayer import (
    NagatoPortalLayer as TrashBinPortalLayer
    )


class NagatoNotebook(Gtk.Notebook, NagatoObject):

    def _yuki_n_insert_page_at_last(self, user_data):
        yuki_page_widgets_layer, yuki_tab = user_data
        yuki_position = self.get_current_page()
        yuki_args = yuki_page_widgets_layer, yuki_tab, yuki_position+1
        yuki_inserted_position = self.insert_page(*yuki_args)
        self.set_tab_reorderable(yuki_page_widgets_layer, True)
        self.show_all()
        self.set_current_page(yuki_inserted_position)

    def _yuki_n_detach_tab(self, page_ux_container):
        self.detach_tab(page_ux_container)

    def _inform_current_page_box(self):
        return self.get_nth_page(self.get_current_page())

    def add_new_page(self, directory=None):
        NagatoFiler.new_for_directory(self, directory)
        # NagatoPortalLayer(self, directory)

    def add_new_trash_bin(self):
        for yuki_position in range(0, self.get_n_pages()):
            yuki_page_box = self.get_nth_page(yuki_position)
            if "trash-bin" == yuki_page_box.get_id():
                self.set_current_page(yuki_position)
                return
        TrashBinPortalLayer(self)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Notebook.__init__(self)
        self.set_scrollable(True)
        HaruhiTabVisibility(self)
        NagatoGlobalAction(self)
        self._raise("YUKI.N > add to paned 2", self)
