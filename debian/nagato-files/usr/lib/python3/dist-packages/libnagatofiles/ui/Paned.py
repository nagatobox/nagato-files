
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.Ux import Unit
from libnagatofiles.ui.PanedWidgets import NagatoPanedWidgets


class NagatoPaned(Gtk.Paned, NagatoObject):

    def _yuki_n_size_allocate(self):
        yuki_data = "side_pane", "width", self.get_position()
        self._raise("YUKI.N > settings", yuki_data)

    def _yuki_n_add_to_paned_1(self, widget):
        self.add1(widget)

    def _yuki_n_add_to_paned_2(self, widget):
        self.add2(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Paned.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        NagatoPanedWidgets(self)
        self.set_wide_handle(True)
        yuki_query = "side_pane", "width", Unit(24)
        yuki_width = self._enquiry("YUKI.N > settings", yuki_query)
        self.set_position(yuki_width)
        self._raise("YUKI.N > attach to grid", (self, (0, 0, 1, 1)))
