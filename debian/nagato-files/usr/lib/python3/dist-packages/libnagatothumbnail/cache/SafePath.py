
from gi.repository import Gio
from gi.repository import GLib

DEFAULT_SUB_DIRECTORY = "nagato-thumbnail"
DEFAULT_SUFFIX = ".png"


class HaruhiSafePath:

    def get_safe_path(self, suffix=DEFAULT_SUFFIX):
        while True:
            yuki_basename = str(GLib.random_int())+str(GLib.random_int())
            yuki_names = [self._cache_directory, yuki_basename+suffix]
            yuki_safe_path = GLib.build_filenamev(yuki_names)
            if not GLib.file_test(yuki_safe_path, GLib.FileTest.EXISTS):
                return yuki_safe_path

    def __init__(self, sub_directory=DEFAULT_SUB_DIRECTORY):
        yuki_names = [GLib.get_user_cache_dir(), sub_directory]
        self._cache_directory = GLib.build_filenamev(yuki_names)
        if not GLib.file_test(self._cache_directory, GLib.FileTest.EXISTS):
            yuki_gio_file = Gio.File.new_for_path(self._cache_directory)
            yuki_gio_file.make_directory_with_parents(None)
