
from gi.repository import GLib
from gi.repository import GdkPixbuf
from libnagatothumbnail.cache.SafePath import HaruhiSafePath

ATTRIBUTE = "metadata::nagato-thumbnail-path"


class AbstractCache:

    def get(self, file_info):
        yuki_cache_path = file_info.get_attribute_as_string(ATTRIBUTE)
        if yuki_cache_path is None:
            return None
        if not GLib.file_test(yuki_cache_path, GLib.FileTest.EXISTS):
            return None
        return GdkPixbuf.Pixbuf.new_from_file(yuki_cache_path)

    def __init__(self):
        self._safe_path = HaruhiSafePath()
