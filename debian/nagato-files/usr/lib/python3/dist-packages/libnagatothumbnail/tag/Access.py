
import math
from libnagato4.Ux import Unit
from libnagatothumbnail.tag.Tag import NagatoTag

HEIGHT = Unit(16)*0.6
OFFSET_RATE = 1/math.sqrt(2)


class HaruhiAccess(NagatoTag):

    ANGLE = -45

    def _get_access(self, file_info):
        if not file_info.get_attribute_boolean("access::can-read"):
            return "Unreadable", "DON'T\nACCESS"
        if not file_info.get_attribute_boolean("access::can-write"):
            return "Read Only", "READ ONLY"
        return "Read Write", ""

    def _get_offset_positions(self, layout_height):
        yuki_offset = layout_height*OFFSET_RATE
        return 0-yuki_offset, HEIGHT-yuki_offset

    def paint(self, cairo_context, file_info):
        yuki_color, yuki_text = self._get_access(file_info)
        if yuki_color != "Read Write":
            self._shade.north_west(cairo_context, yuki_color)
            self._paint_text(cairo_context, yuki_text)
