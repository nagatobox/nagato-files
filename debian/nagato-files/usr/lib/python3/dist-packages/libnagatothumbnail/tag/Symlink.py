
import math
from libnagato4.Ux import Unit
from libnagatothumbnail.tag.Tag import NagatoTag

WIDTH = Unit(16)*0.4
OFFSET_RATE = 1/math.sqrt(2)


class NagatoSymlink(NagatoTag):

    ANGLE = 45

    def _get_offset_positions(self, layout_height):
        yuki_offset = layout_height*OFFSET_RATE
        return WIDTH+yuki_offset, 0-yuki_offset

    def paint(self, cairo_context, file_info):
        if not file_info.get_attribute_boolean("standard::is-symlink"):
            return
        self._shade.north_east(cairo_context, "symlink")
        self._paint_text(cairo_context, "SYMLINK")
