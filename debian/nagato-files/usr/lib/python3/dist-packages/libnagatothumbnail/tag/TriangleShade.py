
from libnagato4.Ux import Unit

SIZE_04 = Unit(16)*0.4
SIZE_06 = Unit(16)*0.6
SIZE_10 = Unit(16)
COLOR = {
    "Read Only": (1, 0.25, 0, 0.8),
    "Unreadable": (1, 0, 0, 0.8),
    "symlink": (0.5, 1, 0, 0.8),
    "xdg-user-dir": (0.5, 0.5, 1, 0.8),
    "broken image": (1, 0, 0, 0.8)
    }


class HaruhiTriagleShade:

    def north_east(self, cairo_context, tag):
        cairo_context.set_source_rgba(*COLOR[tag])
        cairo_context.move_to(SIZE_10, 0)
        cairo_context.line_to(SIZE_10, SIZE_06)
        cairo_context.line_to(SIZE_04, 0)
        cairo_context.fill()

    def north_west(self, cairo_context, tag):
        cairo_context.set_source_rgba(*COLOR[tag])
        cairo_context.move_to(0, 0)
        cairo_context.line_to(SIZE_06, 0)
        cairo_context.line_to(0, SIZE_06)
        cairo_context.fill()
