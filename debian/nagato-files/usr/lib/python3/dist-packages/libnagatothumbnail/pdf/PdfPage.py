
from gi.repository import Gio
from gi.repository import Poppler


class HaruhiPdfPage:

    def render_to_context(self, cairo_context):
        self._page.render(cairo_context)

    def get_size_int(self):
        yuki_width, yuki_height = self._page.get_size()
        return int(yuki_width), int(yuki_height)

    def __init__(self, fullpath):
        self._page = None
        yuki_gio_file = Gio.File.new_for_path(fullpath)
        yuki_document = Poppler.Document.new_from_gfile(yuki_gio_file, None)
        if yuki_document is not None:
            self._page = yuki_document.get_page(0)

    @property
    def is_valid(self):
        return self._page is not None
