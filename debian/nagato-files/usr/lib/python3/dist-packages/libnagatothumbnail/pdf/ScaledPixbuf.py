
from gi.repository import GdkPixbuf
from libnagato4.Ux import Unit
from libnagatothumbnail.pdf.Surface import HaruhiSurface

THUMBNAIL_SIZE = Unit(16)
INTERP_TYPE = GdkPixbuf.InterpType.HYPER


class HaruhiScaledPixbuf:

    def _get_thumbnail_size(self, pixbuf):
        yuki_width = pixbuf.get_width()
        yuki_height = pixbuf.get_height()
        yuki_rate = min(yuki_width/THUMBNAIL_SIZE, yuki_height/THUMBNAIL_SIZE)
        return yuki_rate, (yuki_width/yuki_rate), (yuki_height/yuki_rate)

    def _get_scaled(self, pixbuf):
        yuki_rate, yuki_width, yuki_height = self._get_thumbnail_size(pixbuf)
        if 1 >= yuki_rate:
            return pixbuf
        return pixbuf.scale_simple(yuki_width, yuki_height, INTERP_TYPE)

    def get_from_pdf_page(self, pdf_page):
        yuki_size = pdf_page.get_size_int()
        haruhi_surface = HaruhiSurface.new_pdf(yuki_size)
        pdf_page.render_to_context(haruhi_surface.cairo_context)
        yuki_pixbuf = haruhi_surface.get_pixbuf()
        return self._get_scaled(yuki_pixbuf)
