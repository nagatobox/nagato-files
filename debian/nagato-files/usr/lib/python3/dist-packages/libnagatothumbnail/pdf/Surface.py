
import cairo
from gi.repository import Gdk
from libnagatothumbnail.CairoContext import HaruhiCairoContext


class HaruhiSurface:

    @classmethod
    def new_pdf(cls, size):
        return HaruhiSurface(size, (0.9, 0.9, 0.9, 1))

    def get_pixbuf(self):
        return Gdk.pixbuf_get_from_surface(self._surface, 0, 0, *self._size)

    def paint_pixbuf(self, pixbuf):
        self._context.paint_pixbuf(pixbuf)

    def __init__(self, size, rgba=None):
        self._size = size
        self._surface = cairo.ImageSurface(cairo.Format.ARGB32, *self._size)
        self._context = HaruhiCairoContext(self._surface, size, rgba)

    @property
    def cairo_context(self):
        return self._context.cairo_context
