
from libnagatothumbnail.directory.Directory import HaruhiDirectory
from libnagatothumbnail.file.File import HaruhiFile
from libnagatothumbnail.image.Image import HaruhiImage
from libnagatothumbnail.video.Video import HaruhiVideo
from libnagatothumbnail.pdf.Pdf import HaruhiPdf

FILE_TYPES = {
    "inode/directory": "_directory",
    "image": "_image",
    "video": "_video",
    "application/pdf": "_pdf"
    }


class HaruhiTileSetter:

    def _get_type(self, file_info):
        yuki_content_type = file_info.get_content_type()
        for yuki_key, yuki_value in FILE_TYPES.items():
            if yuki_content_type.startswith(yuki_key):
                return yuki_value
        return "_file"

    def get_tiles(self, fullpath, file_info):
        yuki_builder = getattr(self, self._get_type(file_info))
        return yuki_builder.build_(fullpath, file_info)

    def __init__(self):
        self._directory = HaruhiDirectory()
        self._image = HaruhiImage()
        self._video = HaruhiVideo()
        self._pdf = HaruhiPdf()
        self._file = HaruhiFile()
