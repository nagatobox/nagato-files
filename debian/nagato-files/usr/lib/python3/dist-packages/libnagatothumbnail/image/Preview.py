
from libnagatothumbnail.image.Cache import HaruhiCache
from libnagatothumbnail.image.ScaledPixbuf import HaruhiScaledPixbuf


class HaruhiPreview:

    def _get_thumbnail_from_path(self, fullpath):
        pass

    def load(self, fullpath, file_info):
        yuki_thumbnail = self._cache.get(file_info)
        if yuki_thumbnail is None:
            yuki_thumbnail = self._scaled_pixbuf.get_from_fullpath(fullpath)
            self._cache.save(yuki_thumbnail, file_info)
        return yuki_thumbnail

    def __init__(self):
        self._scaled_pixbuf = HaruhiScaledPixbuf()
        self._cache = HaruhiCache()
