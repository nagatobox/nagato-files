
from gi.repository import GdkPixbuf
from gi.repository import GLib
from libnagato4.Ux import Unit
from libnagatothumbnail.image.ThemedIcon import HaruhiThemedIcon

THUMBNAIL_SIZE = Unit(16)
INTERP_TYPE = GdkPixbuf.InterpType.HYPER


class HaruhiScaledPixbuf:

    def _get_thumbnail_size(self, pixbuf):
        yuki_width = pixbuf.get_width()
        yuki_height = pixbuf.get_height()
        yuki_rate = min(yuki_width/THUMBNAIL_SIZE, yuki_height/THUMBNAIL_SIZE)
        return yuki_rate, (yuki_width/yuki_rate), (yuki_height/yuki_rate)

    def _get_scaled(self, pixbuf):
        yuki_rate, yuki_width, yuki_height = self._get_thumbnail_size(pixbuf)
        if 1 >= yuki_rate:
            return pixbuf
        return pixbuf.scale_simple(yuki_width, yuki_height, INTERP_TYPE)

    def get_from_fullpath(self, fullpath):
        try:
            yuki_pixbuf = GdkPixbuf.Pixbuf.new_from_file(fullpath)
            return self._get_scaled(yuki_pixbuf)
        except GLib.Error:
            return self._themed_icon.get_error_icon()

    def __init__(self):
        self._themed_icon = HaruhiThemedIcon()
