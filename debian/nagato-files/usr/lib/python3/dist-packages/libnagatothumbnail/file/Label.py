
from libnagato4.Ux import Unit
from libnagatothumbnail.pangolayout.PangoLayout import HaruhiPangoLayout

TILE_SIZE = Unit(16)
MARGIN = Unit("margin")


class HaruhiLabel:

    def paint(self, cairo_context, fullpath):
        yuki_layout = HaruhiPangoLayout(cairo_context, fullpath)
        yuki_width, yuki_height = yuki_layout.get_pixel_size()
        cairo_context.set_source_rgba(1, 1, 1, 1)
        cairo_context.move_to(MARGIN, TILE_SIZE-yuki_height-MARGIN)
        yuki_layout.paint()
