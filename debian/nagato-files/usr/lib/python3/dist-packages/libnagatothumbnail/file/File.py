
from libnagatothumbnail.Surface import HaruhiSurface
from libnagatothumbnail.selected.Selected import HaruhiSelected
from libnagatothumbnail.file.Icon import HaruhiIcon
from libnagatothumbnail.label.Labels import HaruhiLabels


class HaruhiFile:

    # BACKGROUND_COLOR = 112/255, 128/255, 144/255, 0.85
    BACKGROUND_COLOR = 128/255, 128/255, 128/255, 0.85

    def build_(self, fullpath, file_info):
        haruhi_surface = HaruhiSurface.new_tile(self.BACKGROUND_COLOR)
        yuki_icon_pixbuf = self._icon.get_icon_from_file_info(file_info)
        yuki_cairo_context = haruhi_surface.paint_pixbuf(yuki_icon_pixbuf)
        self._labels.paint(yuki_cairo_context, file_info)
        yuki_unselected_tile = haruhi_surface.get_pixbuf()
        self._selected.paint(yuki_cairo_context)
        yuki_selected_tile = haruhi_surface.get_pixbuf()
        return yuki_unselected_tile, yuki_selected_tile

    def __init__(self):
        self._icon = HaruhiIcon()
        self._labels = HaruhiLabels()
        self._selected = HaruhiSelected()
