
from libnagatothumbnail.label.ShadeLabel import HaruhiShadeLabel
from libnagatothumbnail.tag.Symlink import NagatoSymlink
from libnagatothumbnail.tag.Access import HaruhiAccess


class HaruhiLabels:

    def paint(self, cairo_context, file_info):
        self._label.paint(cairo_context, file_info)
        self._access.paint(cairo_context, file_info)
        self._symlink.paint(cairo_context, file_info)

    def __init__(self, rgba=None):
        self._access = HaruhiAccess(self)
        self._symlink = NagatoSymlink(self)
        self._label = HaruhiShadeLabel(rgba)
