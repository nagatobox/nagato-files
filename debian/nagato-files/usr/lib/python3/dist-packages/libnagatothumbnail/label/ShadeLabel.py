
from libnagato4.Ux import Unit
from libnagatothumbnail.pangolayout.PangoLayout import HaruhiPangoLayout

TILE_SIZE = Unit(16)
MARGIN = Unit("margin")


class HaruhiShadeLabel:

    def _get_label_geometries(self, height):
        yuki_height = height+MARGIN*2
        yuki_geometries = 0, TILE_SIZE-yuki_height, TILE_SIZE, yuki_height
        return yuki_geometries

    def _paint_shade(self, cairo_context, height):
        cairo_context.set_source_rgba(*self._shade_color)
        cairo_context.rectangle(*self._get_label_geometries(height))
        cairo_context.fill()

    def paint(self, cairo_context, file_info):
        yuki_layout = HaruhiPangoLayout(cairo_context, file_info.get_name())
        yuki_width, yuki_height = yuki_layout.get_pixel_size()
        if self._shade_color is not None:
            self._paint_shade(cairo_context, yuki_height)
        cairo_context.set_source_rgba(1, 1, 1, 1)
        cairo_context.move_to(MARGIN, TILE_SIZE-yuki_height-MARGIN)
        yuki_layout.paint()

    def __init__(self, rgba=None):
        self._shade_color = rgba
