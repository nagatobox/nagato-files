
from libnagatothumbnail.directory.Directory import HaruhiDirectory
from libnagatothumbnail.file.File import HaruhiFile
from libnagatothumbnail.image.Image import HaruhiImage
from libnagatothumbnail.video.Video import HaruhiVideo
from libnagatothumbnail.pdf.Pdf import HaruhiPdf
from libnagatothumbnail.mp4.Mp4 import HaruhiMp4
from libnagatothumbnail.mp3.Mp3 import HaruhiMp3


BUILDERS = {
    "inode/directory": HaruhiDirectory(),
    "image": HaruhiImage(),
    "video": HaruhiVideo(),
    "application/pdf": HaruhiPdf(),
    "audio/mp4": HaruhiMp4(),
    "audio/mpeg": HaruhiMp3()
    }


class HaruhiThumbnail:

    def _get_builder(self, file_info):
        yuki_content_type = file_info.get_content_type()
        for yuki_key, yuki_value in BUILDERS.items():
            if yuki_content_type.startswith(yuki_key):
                return yuki_value
        return self._file

    def get_tiles(self, fullpath, file_info):
        yuki_builder = self._get_builder(file_info)
        return yuki_builder.build_(fullpath, file_info)

    def __init__(self):
        self._file = HaruhiFile()
