
from libnagatothumbnail.video.Cache import HaruhiCache
from libnagatothumbnail.video.VideoContext import HaruhiVideoContext
from libnagatothumbnail.video.FFMpeg import HaruhiFFMpeg
from libnagatothumbnail.image.ThemedIcon import HaruhiThemedIcon


class HaruhiPreview:

    def _get_from_ffmpeg(self, in_path, file_info):
        yuki_out_path = self._cache.get_safe_path()
        haruhi_video_context = HaruhiVideoContext(in_path, yuki_out_path)
        if self._thumbnailer.try_create(haruhi_video_context):
            self._cache.save(yuki_out_path, file_info, in_path)
            return haruhi_video_context.get_pixbuf()
        else:
            return self._icon.get_error_icon()

    def load(self, fullpath, file_info):
        yuki_thumbnail = self._cache.get(file_info)
        if yuki_thumbnail is not None:
            return yuki_thumbnail
        return self._get_from_ffmpeg(fullpath, file_info)

    def __init__(self):
        self._thumbnailer = HaruhiFFMpeg()
        self._cache = HaruhiCache()
        self._icon = HaruhiThemedIcon()
