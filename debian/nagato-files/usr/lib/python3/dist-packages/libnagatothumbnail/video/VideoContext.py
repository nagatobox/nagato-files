
from libnagatothumbnail.video.VideoPath import HaruhiVideoPath
from libnagatothumbnail.video.VideoTrack import HaruhiVideoTrack


class HaruhiVideoContext:

    def get_pixbuf(self):
        return self._path.get_pixbuf()

    def __init__(self, in_path, out_path):
        self._path = HaruhiVideoPath(in_path, out_path)
        self._track = HaruhiVideoTrack(in_path)

    @property
    def in_path(self):
        return self._path.in_path

    @property
    def out_path(self):
        return self._path.out_path

    @property
    def is_valid(self):
        return self._track.is_valid

    @property
    def thumbnail_size(self):
        return self._track.thumbnail_size
