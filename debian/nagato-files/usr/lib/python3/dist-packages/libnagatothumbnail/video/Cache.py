
from gi.repository import Gio
from libnagatothumbnail.cache.Cache import AbstractCache

ATTRIBUTE = "metadata::nagato-thumbnail-path"


class HaruhiCache(AbstractCache):

    def _abort(self, subprocess, task):
        pass

    def get_safe_path(self):
        return self._safe_path.get_safe_path()

    def save(self, cache_path, file_info, source_path):
        yuki_command = ["gio", "set", source_path, ATTRIBUTE, cache_path]
        yuki_subprocess = Gio.Subprocess.new(yuki_command, 0)
        yuki_subprocess.communicate_utf8_async(None, None, self._abort)
        """
        # THIS DOESN'T WORK. WHY ?
        print("saved path", file_info.get_name(), cache_path)
        file_info.remove_attribute(ATTRIBUTE)
        file_info.set_attribute_string(ATTRIBUTE, cache_path)
        print("current path", file_info.get_attribute_as_string(ATTRIBUTE))
        print("status", file_info.get_attribute_status(ATTRIBUTE))
        """
