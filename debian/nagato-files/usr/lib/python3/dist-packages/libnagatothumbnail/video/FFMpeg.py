
from gi.repository import Gio

FLAG = Gio.SubprocessFlags.NONE
DEFAULT_POSITION_PERCENT = 10


class HaruhiFFMpeg:

    def _call_sync(self, position, video_context):
        yuki_args = [
            "ffmpegthumbnailer",
            "-i", video_context.in_path,
            "-o", video_context.out_path,
            "-t", "{}%".format(position),
            "-s", str(video_context.thumbnail_size)
            ]
        yuki_subprocess = Gio.Subprocess.new(yuki_args, FLAG)
        yuki_success, _, _ = yuki_subprocess.communicate_utf8()
        return yuki_success

    def try_create(self, video_context, position=DEFAULT_POSITION_PERCENT):
        if not video_context.is_valid:
            return False
        return self._call_sync(position, video_context)
