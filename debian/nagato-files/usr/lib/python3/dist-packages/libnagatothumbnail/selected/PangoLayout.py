
from gi.repository import PangoCairo
from libnagatothumbnail.pangolayout import LayoutFactory


class HaruhiPangoLayout:

    def get_pixel_size(self):
        return self._layout.get_pixel_size()

    def paint(self):
        PangoCairo.update_layout(self._context, self._layout)
        PangoCairo.show_layout(self._context, self._layout)

    def __init__(self, cairo_context):
        self._context = cairo_context
        self._layout = LayoutFactory.get_from_cairo_context(cairo_context)
        self._layout.set_markup("SELECTED")
