
from libnagato4.Ux import Unit
from libnagatothumbnail.selected.Shade import HaruhiShade
from libnagatothumbnail.selected.PangoLayout import HaruhiPangoLayout

MARGIN = Unit("margin")


class HaruhiSelected:

    def paint(self, cairo_context):
        haruhi_layout = HaruhiPangoLayout(cairo_context)
        _, yuki_height = haruhi_layout.get_pixel_size()
        self._shade.paint(cairo_context, yuki_height)
        cairo_context.set_source_rgba(1, 1, 1, 1)
        cairo_context.move_to(MARGIN, MARGIN)
        haruhi_layout.paint()

    def __init__(self):
        self._shade = HaruhiShade()
