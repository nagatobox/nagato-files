
from gi.repository import GdkPixbuf
from gi.repository import GLib
from libnagato4.Ux import Unit
from libnagatothumbnail.icon.ThemedIcon import HaruhiThemedIcon
from libnagatothumbnail.mp3.Mutagen import HaruhiMutagen

THUMBNAIL_SIZE = Unit(16)
INTERP_TYPE = GdkPixbuf.InterpType.HYPER


class HaruhiScaledPixbuf:

    def _get_thumbnail_size(self, pixbuf):
        yuki_width = pixbuf.get_width()
        yuki_height = pixbuf.get_height()
        yuki_rate = min(yuki_width/THUMBNAIL_SIZE, yuki_height/THUMBNAIL_SIZE)
        return yuki_rate, (yuki_width/yuki_rate), (yuki_height/yuki_rate)

    def _get_scaled(self, pixbuf):
        yuki_rate, yuki_width, yuki_height = self._get_thumbnail_size(pixbuf)
        if 1 >= yuki_rate:
            return pixbuf
        return pixbuf.scale_simple(yuki_width, yuki_height, INTERP_TYPE)

    def get(self, fullpath, file_info):
        yuki_stream = self._mutagen.get_stream_from_path(fullpath)
        if yuki_stream is not None:
            yuki_pixbuf = GdkPixbuf.Pixbuf.new_from_stream(yuki_stream, None)
            return self._get_scaled(yuki_pixbuf)
        return self._themed_icon.get_icon_from_file_info(file_info)

    def __init__(self):
        self._themed_icon = HaruhiThemedIcon()
        self._mutagen = HaruhiMutagen()
