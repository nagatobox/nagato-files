
from libnagatothumbnail.Surface import HaruhiSurface
from libnagatothumbnail.icon.ThemedIcon import HaruhiThemedIcon


class HaruhiLoader:

    def _get_rgba(self, file_info):
        if file_info.get_content_type() == "inode/directory":
            return (0.4, 0.4, 1, 0.9)
        else:
            return (0.5, 0.5, 0.5, 0.5)

    def get_from_file_info(self, file_info):
        yuki_rgba = self._get_rgba(file_info)
        haruhi_surface = HaruhiSurface.new_tile(yuki_rgba)
        yuki_pixbuf = self._themed_icon.get_icon_from_file_info(file_info)
        haruhi_surface.paint_pixbuf(yuki_pixbuf)
        return haruhi_surface.get_pixbuf()

    def __init__(self):
        self._themed_icon = HaruhiThemedIcon()
