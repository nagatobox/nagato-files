
from pymediainfo import MediaInfo
from libnagato4.Ux import Unit


class HaruhiVideoTrack:

    def _set_thumbnail_size(self, track):
        yuki_edge_length = max(track.width, track.height)
        yuki_rate = Unit(16)/min(track.width, track.height)
        self._thumbnail_size = yuki_edge_length*yuki_rate

    def __init__(self, in_path):
        self._is_valid = False
        self._thumbnail_size = 0
        yuki_media_info = MediaInfo.parse(in_path)
        for yuki_track in yuki_media_info.tracks:
            if yuki_track.track_type == "Video":
                self._is_valid = True
                self._set_thumbnail_size(yuki_track)
                break

    @property
    def is_valid(self):
        return self._is_valid

    @property
    def thumbnail_size(self):
        return self._thumbnail_size
