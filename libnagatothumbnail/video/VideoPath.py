
from gi.repository import GdkPixbuf


class HaruhiVideoPath:

    def get_pixbuf(self):
        yuki_pixbuf = GdkPixbuf.Pixbuf.new_from_file(self._out_path)
        return yuki_pixbuf

    def __init__(self, in_path, out_path):
        self._in_path = in_path
        self._out_path = out_path

    @property
    def in_path(self):
        return self._in_path

    @property
    def out_path(self):
        return self._out_path
