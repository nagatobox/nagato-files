
from libnagato4.Ux import Unit

TILE_SIZE = Unit(16)
MARGIN = Unit("margin")


class AbstractLabel:

    def _get_label_geometries(self, height):
        yuki_height = height+MARGIN*2
        yuki_geometries = 0, 0, TILE_SIZE, yuki_height
        return yuki_geometries
