
from libnagatothumbnail.image.ThemedIcon import HaruhiThemedIcon
from libnagatothumbnail.pdf.Cache import HaruhiCache
from libnagatothumbnail.pdf.PdfPage import HaruhiPdfPage
from libnagatothumbnail.pdf.ScaledPixbuf import HaruhiScaledPixbuf


class HaruhiPreview:

    def _get_from_poppler(self, fullpath, file_info):
        haruhi_pdf_page = HaruhiPdfPage(fullpath)
        if not haruhi_pdf_page.is_valid:
            return self._icon.get_error_icon()
        yuki_pixbuf = self._scaled_pixbuf.get_from_pdf_page(haruhi_pdf_page)
        self._cache.save_pixbuf(yuki_pixbuf, fullpath)
        return yuki_pixbuf

    def load(self, fullpath, file_info):
        yuki_thumbnail = self._cache.get(file_info)
        if yuki_thumbnail is not None:
            return yuki_thumbnail
        return self._get_from_poppler(fullpath, file_info)

    def __init__(self):
        self._scaled_pixbuf = HaruhiScaledPixbuf()
        self._cache = HaruhiCache()
        self._icon = HaruhiThemedIcon()
