
from libnagatothumbnail.Surface import HaruhiSurface
from libnagatothumbnail.selected.Selected import HaruhiSelected
from libnagatothumbnail.label.Labels import HaruhiLabels
from libnagatothumbnail.pdf.Preview import HaruhiPreview

SHADE_COLOR_RGBA = 233/255, 11/255, 87/255, 0.6


class HaruhiPdf:

    def build_(self, fullpath, file_info):
        haruhi_surface = HaruhiSurface.new_tile()
        yuki_pixbuf = self._preview.load(fullpath, file_info)
        yuki_cairo_context = haruhi_surface.paint_pixbuf(yuki_pixbuf)
        self._labels.paint(yuki_cairo_context, file_info)
        yuki_unselected_tile = haruhi_surface.get_pixbuf()
        self._selected.paint(yuki_cairo_context)
        yuki_selected_tile = haruhi_surface.get_pixbuf()
        return yuki_unselected_tile, yuki_selected_tile

    def __init__(self):
        self._preview = HaruhiPreview()
        self._labels = HaruhiLabels(SHADE_COLOR_RGBA)
        self._selected = HaruhiSelected()
