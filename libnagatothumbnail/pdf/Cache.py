
from gi.repository import Gio
from libnagatothumbnail.cache.Cache import AbstractCache

ATTRIBUTE = "metadata::nagato-thumbnail-path"


class HaruhiCache(AbstractCache):

    def _save_pixbuf(self, user_data):
        pixbuf, cache_path = user_data
        pixbuf.savev(cache_path, "png", [], [])

    def _abort(self, subprocess, task):
        pass

    def get_safe_path(self):
        return self._safe_path.get_safe_path()

    def save_pixbuf(self, pixbuf, source_path):
        yuki_cache_path = self._safe_path.get_safe_path()
        self._save_pixbuf((pixbuf, yuki_cache_path))
        yuki_command = ["gio", "set", source_path, ATTRIBUTE, yuki_cache_path]
        yuki_subprocess = Gio.Subprocess.new(yuki_command, 0)
        yuki_subprocess.communicate_utf8_async(None, None, self._abort)
