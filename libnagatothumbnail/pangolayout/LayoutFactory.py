
import math
from gi.repository import Pango
from gi.repository import PangoCairo
from libnagato4.Ux import Unit
from libnagatothumbnail.pangolayout import GtkFont

FONT_DESCRIPTION = GtkFont.get(-1)
MAXIMUM_TEXT_HEIGHT = Unit(8)
TILE_SIZE = Unit(16)
MARGIN = Unit("margin")

TRIANGLE_HEIGHT = Unit(16)*0.6
TRIANGLE_WIDTH = math.sqrt((TRIANGLE_HEIGHT*TRIANGLE_HEIGHT)*2)


def get_triangle(cairo_context):
    yuki_layout = PangoCairo.create_layout(cairo_context)
    yuki_layout.set_alignment(Pango.Alignment.CENTER)
    yuki_layout.set_font_description(FONT_DESCRIPTION)
    yuki_layout.set_width(TRIANGLE_WIDTH*Pango.SCALE)
    yuki_layout.set_height(-1)
    yuki_layout.set_wrap(Pango.WrapMode.WORD_CHAR)
    yuki_layout.set_ellipsize(Pango.EllipsizeMode.MIDDLE)
    return yuki_layout.copy()


def get_from_cairo_context(cairo_context):
    yuki_layout = PangoCairo.create_layout(cairo_context)
    yuki_layout.set_alignment(Pango.Alignment.CENTER)
    yuki_layout.set_font_description(FONT_DESCRIPTION)
    yuki_layout.set_width((TILE_SIZE-MARGIN*2)*Pango.SCALE)
    yuki_layout.set_height(MAXIMUM_TEXT_HEIGHT*Pango.SCALE)
    yuki_layout.set_wrap(Pango.WrapMode.WORD_CHAR)
    yuki_layout.set_ellipsize(Pango.EllipsizeMode.MIDDLE)
    return yuki_layout.copy()
