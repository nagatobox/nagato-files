
from libnagato4.Ux import Unit

TILE_SIZE = Unit(16)
MARGIN = Unit("margin")
SHADE_COLOR_RGBA = (1, 0.5, 0, 0.8)


class HaruhiShade:

    def _get_label_geometries(self, height):
        yuki_height = height+MARGIN*2
        yuki_geometries = 0, 0, TILE_SIZE, yuki_height
        return yuki_geometries

    def paint(self, cairo_context, height):
        cairo_context.set_source_rgba(*SHADE_COLOR_RGBA)
        cairo_context.rectangle(*self._get_label_geometries(height))
        cairo_context.fill()
