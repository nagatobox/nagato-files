
import mutagen
from gi.repository import GLib
from gi.repository import Gio


class HaruhiMutagen:

    def _get_for_mp4(self, mp4_cover):
        yuki_bytes = GLib.Bytes.new(mp4_cover)
        return Gio.MemoryInputStream.new_from_bytes(yuki_bytes)

    def get_stream_from_path(self, fullpath):
        yuki_file = mutagen.File(fullpath)
        if "covr" in yuki_file:
            return self._get_for_mp4(yuki_file["covr"][0])
        return None
