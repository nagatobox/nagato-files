
from gi.repository import Gtk
from gi.repository import GLib
from libnagato4.Ux import Unit

ICON_SIZE = Unit(8)
FLAG = Gtk.IconLookupFlags.GENERIC_FALLBACK


class HaruhiThemedIcon:

    def _get_pixbuf(self, names, size, fallback="text-x-generic"):
        try:
            yuki_pixbuf = self._icon_theme.load_icon(names[0], size, FLAG)
            return yuki_pixbuf
        except GLib.Error:
            # when suitable icon data not found in icon theme.
            return self._get_pixbuf([fallback], size)

    def get_icon_from_file_info(self, file_info):
        yuki_icon = file_info.get_icon()
        yuki_icon_names = yuki_icon.get_names()
        return self._get_pixbuf(yuki_icon_names, ICON_SIZE)

    def __init__(self):
        self._icon_theme = Gtk.IconTheme.get_default()
