
from gi.repository import GLib
from libnagatothumbnail.cache.Cache import AbstractCache

ATTRIBUTE = "metadata::nagato-thumbnail-path"
CACHE_FORMAT = "png"


class HaruhiCache(AbstractCache):

    def _save_pixbuf(self, user_data):
        yuki_pixbuf, yuki_cache_path = user_data
        yuki_pixbuf.savev(yuki_cache_path, CACHE_FORMAT, [], [])

    def save(self, pixbuf, file_info):
        yuki_cache_path = self._safe_path.get_safe_path()
        GLib.idle_add(self._save_pixbuf, (pixbuf, yuki_cache_path))
        file_info.set_attribute_string(ATTRIBUTE, yuki_cache_path)
