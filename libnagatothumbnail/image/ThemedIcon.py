
from gi.repository import Gtk
from gi.repository import GLib
from libnagato4.Ux import Unit

ICON_SIZE = Unit(8)
FLAG = Gtk.IconLookupFlags.GENERIC_FALLBACK


class HaruhiThemedIcon:

    def _get_pixbuf(self, name, size, fallback="text-x-generic"):
        try:
            yuki_pixbuf = self._icon_theme.load_icon(name, size, FLAG)
            return yuki_pixbuf
        except GLib.Error:
            # when suitable icon data not found in icon theme.
            return self._get_pixbuf(fallback, size)

    def get_error_icon(self):
        return self._get_pixbuf("dialog-error", ICON_SIZE)

    def __init__(self):
        self._icon_theme = Gtk.IconTheme.get_default()
