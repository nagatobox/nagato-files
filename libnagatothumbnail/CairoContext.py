
import cairo
from gi.repository import Gdk
from libnagatothumbnail.PixbufPosition import HaruhiPixbufPosition


class HaruhiCairoContext:

    def paint_pixbuf(self, pixbuf):
        yuki_x, yuki_y = self._position.get_positions(pixbuf)
        Gdk.cairo_set_source_pixbuf(self._context, pixbuf, yuki_x, yuki_y)
        self._context.paint()

    def __init__(self, surface, size, rgba=None):
        self._context = cairo.Context(surface)
        self._position = HaruhiPixbufPosition(size)
        if rgba is None:
            rgba = (1, 1, 1, 0.25)
        self._context.set_source_rgba(*rgba)
        self._context.rectangle(0, 0, *size)
        self._context.fill()

    @property
    def cairo_context(self):
        return self._context
