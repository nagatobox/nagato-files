
import math
from libnagato4.Object import NagatoObject
from gi.repository import PangoCairo
from libnagatothumbnail.pangolayout import LayoutFactory
from libnagatothumbnail.tag.TriangleShade import HaruhiTriagleShade


class NagatoTag(NagatoObject):

    ANGLE = 0

    def _get_offset_positions(self, layout_height):
        print("YUKI.N > this method must be overridden.")
        raise NotImplementedError

    def _paint_text(self, cairo_context, markup_):
        yuki_layout = LayoutFactory.get_triangle(cairo_context)
        yuki_layout.set_markup(markup_)
        _, yuki_height = yuki_layout.get_pixel_size()
        cairo_context.set_source_rgba(1, 1, 1, 1)
        cairo_context.move_to(*self._get_offset_positions(yuki_height))
        cairo_context.save()
        cairo_context.rotate(math.radians(self.ANGLE))
        PangoCairo.update_layout(cairo_context, yuki_layout)
        PangoCairo.show_layout(cairo_context, yuki_layout)
        cairo_context.restore()

    def __init__(self, parent):
        self._parent = parent
        self._shade = HaruhiTriagleShade()
