
import mutagen
from mutagen.id3 import ID3
from gi.repository import GLib
from gi.repository import Gio


class HaruhiMutagen:

    def _get_stream(self, id3):
        for yuki_key in id3:
            if not yuki_key.startswith("APIC"):
                continue
            yuki_apic = id3.get(yuki_key)
            yuki_bytes = GLib.Bytes.new(yuki_apic.data)
            return Gio.MemoryInputStream.new_from_bytes(yuki_bytes)

    def _get_id3(self, path):
        try:
            yuki_id3 = ID3(path)
            return yuki_id3
        except mutagen.MutagenError:
            return None

    def get_stream_from_path(self, path):
        yuki_id3 = self._get_id3(path)
        if yuki_id3 is None:
            return None
        return self._get_stream(yuki_id3)

    """
    def _get_for_mp4(self, mp4_cover):
        yuki_bytes = GLib.Bytes.new(mp4_cover)
        return Gio.MemoryInputStream.new_from_bytes(yuki_bytes)

    def get_stream_from_path(self, fullpath):
        yuki_file = mutagen.File(fullpath)
        if "covr" in yuki_file:
            return self._get_for_mp4(yuki_file["covr"][0])
        return None
    """
