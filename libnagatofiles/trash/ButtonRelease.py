
from libnagato4.Object import NagatoObject
from libnagatofiles.filer.LastSelected import NagatoLastSelected
from libnagatofiles.trash.popover.Popovers import NagatoPopovers


class NagatoButtonRelease(NagatoObject):

    def _get_tree_path(self, widget, event):
        yuki_value = widget.get_path_at_pos(event.x, event.y)
        return yuki_value[0] if isinstance(yuki_value, tuple) else yuki_value

    def _on_button_release(self, widget, event):
        yuki_tree_path = self._get_tree_path(widget, event)
        if event.button in (1, 3):
            self._selection.set_last_selected(widget, event, yuki_tree_path)
        if event.button == 3:
            self._popovers.popup(widget, event, yuki_tree_path)

    def __init__(self, parent):
        self._parent = parent
        yuki_widget = self._enquiry("YUKI.N > tree like widget")
        yuki_widget.connect("button-release-event", self._on_button_release)
        self._selection = NagatoLastSelected(yuki_widget)
        self._popovers = NagatoPopovers(self)
