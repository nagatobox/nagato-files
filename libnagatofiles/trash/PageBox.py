
from libnagato4.widget.VBox import AbstractVBox
from libnagatofiles.trash.bar.Navigation import NagatoNavigation
from libnagatofiles.trash.Stack import NagatoStack
from libnagatofiles.trash.bar.Action import NagatoAction


class NagatoPageBox(AbstractVBox):

    def open_directory(self, directory):
        self._raise("YUKI.N > add new tab", directory)

    def get_id(self):
        return self._enquiry("YUKI.N > page id")

    def raise_message(self, message):
        pass

    def _yuki_n_switch_to(self, visible_child):
        self._stack.switch_to(visible_child)

    def _on_initialize(self):
        NagatoNavigation(self)
        self._stack = NagatoStack(self)
        NagatoAction(self)
