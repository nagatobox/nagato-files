
from gi.repository import Gio
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagatofiles.util.FileRowData import HaruhiFileRowData
from libnagatofiles.trash.rowdata.TrashInfo import HaruhiTrashInfo
from libnagatofiles.Mikuru import SourcePriority

PRIORITY = SourcePriority.TRASH_BIN_PARSING


class NagatoAppend(NagatoObject):

    def _append_async(self, user_data):
        fullpath, file_info = user_data
        yuki_row_data = self._file_row_data.get(fullpath, file_info)
        yuki_row_data += self._trash_row_data.get(file_info)
        self._raise("YUKI.N > append row data", yuki_row_data)
        if file_info.get_name() == self._enquiry("YUKI.N > last file name"):
            self._raise("YUKI.N > trash data initialized")
        return GLib.SOURCE_REMOVE

    def append_for_fullpath(self, fullpath):
        yuki_gio_file = Gio.File.new_for_path(fullpath)
        yuki_file_info = yuki_gio_file.query_info("*", 0, None)
        yuki_row_data = self._file_row_data.get(fullpath, yuki_file_info)
        yuki_row_data += self._trash_row_data.get(yuki_file_info)
        self._raise("YUKI.N > append row data", yuki_row_data)

    def append(self, fullpath, file_info):
        yuki_data = fullpath, file_info
        GLib.idle_add(self._append_async, yuki_data, priority=PRIORITY)

    def __init__(self, parent):
        self._parent = parent
        self._file_row_data = HaruhiFileRowData()
        self._trash_row_data = HaruhiTrashInfo()
