
from gi.repository import GLib
from libnagato4.file import HomeDirectory
from libnagatofiles.Mikuru import File
from libnagatofiles.trash import Directories

TIME_ZONE = GLib.TimeZone.new_local()


class HaruhiTrashInfo:

    def _get_text(self, key_file, key):
        yuki_text = key_file.get_string("Trash Info", key)
        return yuki_text.rstrip()

    def get(self, file_info):
        yuki_key_file = GLib.KeyFile()
        yuki_names = [Directories.INFO_DIR, file_info.get_name()+".trashinfo"]
        yuki_path = GLib.build_filenamev(yuki_names)
        yuki_key_file.load_from_file(yuki_path, GLib.KeyFileFlags.NONE)
        yuki_origin = self._get_text(yuki_key_file, "Path")
        yuki_date_text = self._get_text(yuki_key_file, "DeletionDate")
        yuki_date = GLib.DateTime.new_from_iso8601(yuki_date_text, TIME_ZONE)
        yuki_unix_time = yuki_date.to_unix()
        yuki_origin_path, _ = GLib.filename_from_uri("file://"+yuki_origin)
        yuki_age = File.get_age(yuki_unix_time)
        yuki_origin_path_shorten = HomeDirectory.shorten(yuki_origin_path)
        return (
            None,
            yuki_path,
            yuki_origin_path,
            yuki_age,
            yuki_unix_time,
            "{}\n({})".format(yuki_origin_path_shorten, yuki_age)
            )
