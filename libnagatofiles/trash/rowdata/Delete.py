
from libnagato4.Object import NagatoObject
from libnagatofiles.trash.column import Types


class NagatoDelete(NagatoObject):

    def __call__(self, name):
        yuki_model = self._enquiry("YUKI.N > treeview model")
        for yuki_tree_row in yuki_model:
            if yuki_tree_row[Types.FILE_NAME] == name:
                yuki_model.remove(yuki_tree_row.iter)
                break

    def __init__(self, parent):
        self._parent = parent
