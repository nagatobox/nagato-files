
from gi.repository import Gio
from gi.repository import GdkPixbuf

FULLPATH = 0
FILE_NAME = 1
IS_DIR = 2
PATH_FOR_SORT = 3
SELECTED = 4
MIME_TYPE = 5
READBLE_PERMISSION = 6
AGE = 7
AGE_UNIX_TIME = 8
ICON_IMAGE = 9
TAG = 10
FILE_INFO = 11
TILE_TYPE = 12
UNSELECTED_TILE = 13
SELECTED_TILE = 14
TRASH_INFO_FULLPATH = 15
ORIGIN = 16
DELETION = 17
DELETION_UNIX_TIME = 18
TOOLTIP = 19

TYPES = (str, str, bool, str, bool, str, str,
         str, int, GdkPixbuf.Pixbuf, str, Gio.FileInfo, str,
         GdkPixbuf.Pixbuf, GdkPixbuf.Pixbuf,
         str, str, str, int, str
         )
