
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.Ux import Unit


class NagatoSearchEntry(Gtk.SearchEntry, NagatoObject):

    def _on_changed(self, search_entry):
        self._raise("YUKI.N > filter changed", self.get_text())

    def __init__(self, parent):
        self._parent = parent
        Gtk.SearchEntry.__init__(self)
        self.set_hexpand(True)
        self.set_margin_top(Unit("margin"))
        self.set_margin_bottom(Unit("margin"))
        self.set_margin_start(Unit("margin"))
        self.set_margin_end(Unit("margin"))
        self._raise("YUKI.N > add to box", self)
        self.connect("changed", self._on_changed)
