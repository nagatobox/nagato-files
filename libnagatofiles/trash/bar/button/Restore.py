
from libnagatofiles.filer.bar.button.Button import NagatoButton


class NagatoRestore(NagatoButton):

    NAME = "edit-undo-symbolic"
    TOOLTIP = "restore trash item"

    def _on_clicked(self, button):
        self._raise("YUKI.N > restore trash items")
