
from libnagatofiles.filer.bar.button.Button import NagatoButton


class NagatoErase(NagatoButton):

    NAME = "edit-delete-symbolic"
    TOOLTIP = "erase trash item from file system"

    def _on_clicked(self, button):
        self._raise("YUKI.N > erase trash items")
