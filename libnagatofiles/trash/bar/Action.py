
from libnagato4.Ux import Unit
from libnagato4.widget.HBox import AbstractHBox
from libnagatofiles.filer.bar.button.ViewMore import NagatoViewMore
from libnagatofiles.filer.bar.status.Label import NagatoLabel
from libnagatofiles.trash.bar.button.Restore import NagatoRestore
from libnagatofiles.trash.bar.button.Erase import NagatoErase


class NagatoAction(AbstractHBox):

    def _on_initialize(self):
        NagatoViewMore(self)
        NagatoLabel(self)
        NagatoRestore(self)
        NagatoErase(self)
        self.set_size_request(-1, Unit(5))
        self._raise("YUKI.N > css", (self, "toolbar"))
        self._raise("YUKI.N > add to box", self)
