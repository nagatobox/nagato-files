
from libnagato4.Object import NagatoObject
from libnagatofiles.trash.popover.ItemPopover import NagatoItemPopover


class NagatoPopovers(NagatoObject):

    def popup(self, widget, event, tree_path=None):
        if tree_path is None:
            print("popup void")
        else:
            self._item_popover.popup_for_widget(widget, event)

    def __init__(self, parent):
        self._parent = parent
        self._item_popover = NagatoItemPopover(self)
