
from libnagato4.popover.Box import NagatoBox
from libnagato4.popover.Separator import NagatoSeparator
from libnagatofiles.filer.popover.ToApps import NagatoToApps
# from libnagatofiles.filer.popover.ChangePreview import NagatoChangePreview
# from libnagatofiles.filer.popover.Rename import NagatoRename
# from libnagatofiles.filer.popover.CopyUri import NagatoCopyUri
# from libnagatofiles.filer.popover.CopyPath import NagatoCopyPath
from libnagatofiles.filer.popover.ToSort import NagatoToSort
from libnagatofiles.filer.popover.ToSelect import NagatoToSelect
# from libnagatofiles.filer.popover.Copy import NagatoCopy
# from libnagatofiles.filer.popover.Move import NagatoMove
# from libnagatofiles.filer.popover.Delete import NagatoDelete


class NagatoItemBox(NagatoBox):

    NAME = "main"

    def _on_initialize(self):
        NagatoToApps(self)
        NagatoSeparator(self)
        NagatoToSort(self)
        NagatoToSelect(self)
