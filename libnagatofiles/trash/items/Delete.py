
from gi.repository import GLib
from libnagato4.Object import NagatoObject


class NagatoDelete(NagatoObject):

    def _delete_children(self, gio_file):
        for yuki_file_info in gio_file.enumerate_children("*", 0, None):
            yuki_gio_file = gio_file.get_child(yuki_file_info.get_name())
            self.delete(yuki_gio_file)

    def delete(self, gio_file):
        if GLib.file_test(gio_file.get_path(), GLib.FileTest.IS_DIR):
            self._delete_children(gio_file)
        gio_file.delete(None)

    def __init__(self, parent):
        self._parent = parent
