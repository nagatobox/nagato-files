
from gi.repository import Gio
from libnagatofiles.Mikuru import SafePath2
from libnagatofiles.trash.column import Types
from libnagatofiles.trash.items.Items import NagatoItems
from libnagato4.file.copy.Recursive import NagatoRecursive

FLAG = Gio.FileCopyFlags.ALL_METADATA


class NagatoRestore(NagatoItems):

    def _queue_async(self, tree_row):
        yuki_source = Gio.File.new_for_path(tree_row[Types.FULLPATH])
        yuki_source_path = yuki_source.get_path()
        yuki_origin = SafePath2.get_path(tree_row[Types.ORIGIN])
        NagatoRecursive(yuki_source_path, yuki_origin)
        self._delete.delete(yuki_source)
        yuki_trash_info_path = tree_row[Types.TRASH_INFO_FULLPATH]
        yuki_trash_info = Gio.File.new_for_path(yuki_trash_info_path)
        yuki_trash_info.delete(None)
