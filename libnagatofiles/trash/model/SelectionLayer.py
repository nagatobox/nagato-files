
from libnagato4.Object import NagatoObject
from libnagatofiles.trash.model.PrimePathLayer import NagatoPrimePathLayer
from libnagatofiles.filer.selection.Difference import NagatoDifference
from libnagatofiles.filer.selection.Items import NagatoItems


class NagatoSelectionLayer(NagatoObject):

    def _inform_selection(self):
        return self._items.get_all()

    def _inform_selection_can_move(self):
        return self._items.can_move()

    def _yuki_n_clear_selection(self):
        self._items.clear_all()

    def _yuki_n_select_all(self):
        self._items.select_all()

    def _yuki_n_invert_selection(self):
        self._items.invert_selection()

    def __init__(self, parent):
        self._parent = parent
        self._items = NagatoItems(self)
        NagatoPrimePathLayer(self)
        NagatoDifference(self)
