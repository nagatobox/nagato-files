
from libnagato4.Object import NagatoObject
from libnagatofiles.trash.Enumerator import NagatoEnumerator
from libnagatofiles.trash.Monitor import NagatoMonitor
from libnagatofiles.trash.model.SelectionLayer import NagatoSelectionLayer


class NagatoMonitorLayer(NagatoObject):

    def _yuki_n_trash_data_initialized(self):
        self._raise("YUKI.N > trash data initialized")
        NagatoMonitor(self)

    def __init__(self, parent):
        self._parent = parent
        NagatoEnumerator(self)
        NagatoSelectionLayer(self)
