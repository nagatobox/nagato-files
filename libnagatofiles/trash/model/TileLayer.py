
from libnagato4.Object import NagatoObject
from libnagatofiles.tile.Loader import NagatoLoader
from libnagatofiles.trash.model.MonitorLayer import NagatoMonitorLayer


class NagatoTileLayer(NagatoObject):

    def _yuki_n_trash_data_initialized(self):
        self._loader.initialize_directory()

    def _yuki_n_refresh_tile(self, fullpath):
        self._loader.refresh_tile(fullpath)

    def __init__(self, parent):
        self._parent = parent
        self._loader = NagatoLoader(self)
        NagatoMonitorLayer(self)
