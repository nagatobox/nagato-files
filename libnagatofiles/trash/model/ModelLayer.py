
from libnagato4.Object import NagatoObject
from libnagatofiles.trash import Directories
from libnagatofiles.trash.model.BaseModel import NagatoBaseModel
from libnagatofiles.trash.model.FilterModelLayer import (
    NagatoFilterModelLayer
    )


class NagatoModelLayer(NagatoObject):

    def _inform_current_directory(self):
        return Directories.FILE_DIR

    def _inform_show_hidden(self):
        return True

    def _yuki_n_append_row_data(self, user_data):
        self._model.append(user_data)
        self._raise("YUKI.N > trash changed")

    def _inform_treeview_model(self):
        return self._model

    def _inform_trash_bin_row(self, user_data):
        yuki_index, yuki_data = user_data
        for yuki_tree_row in self._model:
            if yuki_tree_row[yuki_index] == yuki_data:
                return yuki_tree_row
        return None

    def __init__(self, parent):
        self._parent = parent
        self._model = NagatoBaseModel(self)
        NagatoFilterModelLayer(self)
