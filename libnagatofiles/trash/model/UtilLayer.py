
from libnagato4.Object import NagatoObject
from libnagatofiles.trash.items.Erase import NagatoErase
from libnagatofiles.trash.items.Restore import NagatoRestore
from libnagatofiles.trash.model.TrashBinLayer import NagatoTrashBinLayer


class NagatoUtilLayer(NagatoObject):

    def _yuki_n_restore_trash_items(self):
        self._restore.queue()

    def _yuki_n_erase_trash_items(self):
        self._erase.queue()

    def __init__(self, parent):
        self._parent = parent
        self._restore = NagatoRestore(self)
        self._erase = NagatoErase(self)
        NagatoTrashBinLayer(self)
