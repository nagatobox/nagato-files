
from libnagato4.Object import NagatoObject
from libnagatofiles.trash.column import Types
from libnagatofiles.trash.model.SortingLayer import NagatoSortingLayer


class NagatoPrimePathLayer(NagatoObject):

    def _reset_selection(self, force_select):
        if force_select:
            self._last_selected_row[Types.SELECTED] = False
        else:
            yuki_selected = not self._last_selected_row[Types.SELECTED]
            self._last_selected_row[Types.SELECTED] = yuki_selected

    def _inform_prime_path(self):
        return self._last_selected_row[Types.FULLPATH]

    def _inform_prime_is_dir(self):
        return self._last_selected_row[Types.MIME_TYPE] == "inode/directory"

    def _inform_prime_last_selected_row(self):
        return self._last_selected_row

    def _yuki_n_last_selected_item_changed(self, user_data):
        yuki_row, _, yuki_force_select = user_data
        self._last_selected_row = yuki_row
        self._reset_selection(yuki_force_select)

    def __init__(self, parent):
        self._parent = parent
        self._last_selected_row = None
        NagatoSortingLayer(self)
