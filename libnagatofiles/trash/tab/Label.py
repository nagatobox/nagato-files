
from gi.repository import Gtk
from libnagato4.Object import NagatoObject


class NagatoLabel(Gtk.Label, NagatoObject):

    def _get_text(self):
        yuki_model = self._enquiry("YUKI.N > treeview model")
        yuki_length = len(yuki_model)
        if yuki_length == 0:
            return "Trash Bin (Empty)"
        return "Trash Bin ({})".format(yuki_length)

    def receive_transmission(self, user_data):
        yuki_type, yuki_data = user_data
        if yuki_type == "trash-changed":
            self.set_text(self._get_text())

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self)
        self.set_can_focus(False)
        self.set_text(self._get_text())
        self._raise("YUKI.N > add to event box", self)
        self._raise("YUKI.N > register navigation widget", self)
