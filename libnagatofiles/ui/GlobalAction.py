
from libnagato4.Object import NagatoObject


class NagatoGlobalAction(NagatoObject):

    def receive_transmission(self, action):
        if action.startswith("filer"):
            yuki_page_box = self._enquiry("YUKI.N > current page box")
            yuki_page_box.raise_message(action.replace("filer", "YUKI.N >"))

    def __init__(self, parent):
        self._parent = parent
        self._raise("YUKI.N > register action object", self)
