
from libnagato4.Object import NagatoObject
from libnagatofiles.ui.Notebook import NagatoNotebook
from libnagatofiles.filer.Initializer import NagatoInitializer
from libnagatofiles.ui.SideBox import NagatoSideBox
from libnagatofiles.ui.notebook.Viewers import HaruhiViewers


class NagatoPanedWidgets(NagatoObject):

    def _yuki_n_open_directory(self, directory):
        yuki_current_page_index = self._notebook.get_current_page()
        yuki_page_box = self._notebook.get_nth_page(yuki_current_page_index)
        yuki_page_box.open_directory(directory)

    def _yuki_n_add_new_tab(self, directory=None):
        self._notebook.add_new_page(directory)

    def _yuki_n_add_new_trash_bin(self):
        self._notebook.add_new_trash_bin()

    def _yuki_n_add_new_viewer(self, user_data):
        self._viewers.launch(user_data)

    def _yuki_n_show_preview(self, fullpath):
        self._side_box.show_preview(fullpath)

    def __init__(self, parent):
        self._parent = parent
        self._notebook = NagatoNotebook(self)
        self._side_box = NagatoSideBox(self)
        self._viewers = HaruhiViewers.new_for_notebook(self._notebook)
        NagatoInitializer(self)
