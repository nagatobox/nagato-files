

from gi.repository import Gtk
from libnagato4.Ux import Unit
from libnagato4.Object import NagatoObject

ICON_NAME = "go-previous-symbolic"
ICON_SIZE = Gtk.IconSize.LARGE_TOOLBAR


class NagatoBackButton(Gtk.Button, NagatoObject):

    def _on_clicked(self, *args):
        self._raise("YUKI.N > back to bookmark")

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, "Back to Bookmark")
        yuki_image = Gtk.Image.new_from_icon_name(ICON_NAME, ICON_SIZE)
        self.set_image(yuki_image)
        self.set_margin_bottom(Unit("padding"))
        self.connect("clicked", self._on_clicked)
        self._raise("YUKI.N > add to revealer", self)
