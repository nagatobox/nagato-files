

class HaruhiTabVisibility(object):

    def _on_order_changed(self, notebook, child, page_num):
        notebook.set_show_tabs(notebook.get_n_pages() > 1)

    def __init__(self, notebook):
        notebook.connect("page-added", self._on_order_changed)
        notebook.connect("page-reordered", self._on_order_changed)
        notebook.connect("page-removed", self._on_order_changed)
