
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.pdfviewer.model import Columns


class NagatoIconView(Gtk.IconView, NagatoObject):

    def _on_scrolled(self, adjustment):
        print("scrolled", self.get_visible_range())

    def __init__(self, parent):
        self._parent = parent
        Gtk.IconView.__init__(self)
        self.set_hexpand(True)
        self.set_vexpand(True)
        yuki_model = self._enquiry("YUKI.N > model")
        self.set_model(yuki_model)
        self.set_pixbuf_column(Columns.PIXBUF)
        self._raise("YUKI.N > add to scrolled window", self)
        self.show_all()
