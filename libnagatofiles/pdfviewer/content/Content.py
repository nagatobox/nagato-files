
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.pdfviewer.signal import Document as Signal
from libnagatofiles.pdfviewer.content.IconView import NagatoIconView
from libnagatofiles.pdfviewer.content.VisibleRange import NagatoVisibleRange


class NagatoContent(Gtk.ScrolledWindow, NagatoObject):

    def _yuki_n_add_to_scrolled_window(self, widget):
        self.add(widget)

    def _on_draw(self, scrolled_window, cairo_context, icon_view):
        yuki_size, _ = scrolled_window.get_allocated_size()
        yuki_data = Signal.SCROLLED_AREA_SIZE_CHANGED, yuki_size
        self._raise("YUKI.N > document signals", yuki_data)
        self._visible_range.update_buffer(icon_view)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ScrolledWindow.__init__(self)
        self.set_hexpand(True)
        self.set_vexpand(True)
        yuki_icon_view = NagatoIconView(self)
        self._visible_range = NagatoVisibleRange(self)
        self.connect("draw", self._on_draw, yuki_icon_view)
        self._raise("YUKI.N > add to box", self)
