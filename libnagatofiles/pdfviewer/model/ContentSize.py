
from libnagato4.Object import NagatoObject
from libnagatofiles.pdfviewer.signal import Document as Signal


class NagatoContentSize(NagatoObject):

    def receive_transmission(self, user_data):
        yuki_type, yuki_data = user_data
        if yuki_type == Signal.SCROLLED_AREA_SIZE_CHANGED:
            self._width = yuki_data.width

    def __init__(self, parent):
        self._parent = parent
        self._width = 0
        self._raise("YUKI.N > register document object", self)

    @property
    def width(self):
        return self._width
