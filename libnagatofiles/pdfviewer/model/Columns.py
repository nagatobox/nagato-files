
from gi.repository import Poppler
from gi.repository import GdkPixbuf

ID = 0
POPPLER_PAGE = 1
PIXBUF = 2
NUMBER_OF_COLUMNS = 3

TYPES = int, Poppler.Page, GdkPixbuf.Pixbuf
