
from libnagato4.Object import NagatoObject
from libnagato4.Transmitter import HaruhiTransmitter
from libnagatofiles.pdfviewer.model.ListStore import NagatoListStore
from libnagatofiles.pdfviewer.WidgetsLayer import NagatoWidgetsLayer


class NagatoModelLayer(NagatoObject):

    def _yuki_n_clear_all(self):
        for yuki_row in self._model:
            yuki_pixbuf = yuki_row[2]
            if yuki_pixbuf is not None:
                yuki_row[2] = None
        self._model.clear()

    def _yuki_n_document_signals(self, user_data):
        self._transmitter.transmit(user_data)

    def _yuki_n_register_document_object(self, object_):
        self._transmitter.register_listener(object_)

    def _inform_model(self):
        return self._model

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = HaruhiTransmitter()
        self._model = NagatoListStore(self)
        NagatoWidgetsLayer(self)
