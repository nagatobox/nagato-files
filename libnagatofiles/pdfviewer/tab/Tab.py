
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.filer.popover import GdkRectangle
from libnagatofiles.pdfviewer.tab.Label import NagatoLabel
from libnagatofiles.filer.popover.TabPopover import NagatoTabPopover


class NagatoTab(Gtk.EventBox, NagatoObject):

    def _on_button_press(self, button, event):
        if event.button != 3:
            return
        yuki_rectangle = GdkRectangle.get_from_event(event)
        self._popover.set_pointing_to(yuki_rectangle)
        self._popover.set_relative_to(button)
        self._popover.show_all()

    def _yuki_n_add_to_event_box(self, widget):
        self.add(widget)

    def __init__(self, parent, container=None):
        self._parent = parent
        Gtk.EventBox.__init__(self)
        self._popover = NagatoTabPopover(self)
        self.connect("button-press-event", self._on_button_press)
        NagatoLabel(self)
        self.show_all()
