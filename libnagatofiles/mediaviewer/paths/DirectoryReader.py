
from gi.repository import Gio
from gi.repository import GLib
from libnagato4.Object import NagatoObject


class NagatoDirectoryReader(NagatoObject):

    def _is_media(self, file_info):
        yuki_content_type = file_info.get_content_type()
        return yuki_content_type.split("/")[0] in ("video", "audio")

    def _append(self, target, directory, file_info):
        yuki_names = [directory, file_info.get_name()]
        yuki_fullpath = GLib.build_filenamev(yuki_names)
        yuki_matched = (yuki_fullpath == target)
        yuki_path_for_sort = GLib.utf8_strdown(yuki_fullpath, -1)
        yuki_row_data = yuki_fullpath, file_info, yuki_path_for_sort
        self._raise("YUKI.N > append", (yuki_matched, yuki_row_data))

    def non_recursive(self, path):
        yuki_directory = GLib.path_get_dirname(path)
        yuki_gio_file = Gio.File.new_for_path(yuki_directory)
        for yuki_file_info in yuki_gio_file.enumerate_children("*", 0):
            if self._is_media(yuki_file_info):
                self._append(path, yuki_directory, yuki_file_info)

    def __init__(self, parent):
        self._parent = parent
