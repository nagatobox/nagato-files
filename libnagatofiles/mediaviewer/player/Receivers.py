
from libnagatofiles.mediaviewer.player.PathsReceiver import NagatoPathsReceiver
from libnagatofiles.mediaviewer.player.PlaybackReceiver import (
    NagatoPlaybackReceiver
    )
from libnagatofiles.mediaviewer.player.SettingsReceiver import (
    NagatoSettingsReceiver
    )


class AsakuraReceivers:

    def __init__(self, parent):
        NagatoPlaybackReceiver(parent)
        NagatoPathsReceiver(parent)
        NagatoSettingsReceiver(parent)
