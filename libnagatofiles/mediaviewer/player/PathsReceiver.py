
from gi.repository import GLib
from libnagato4.Object import NagatoObject


class NagatoPathsReceiver(NagatoObject):

    def receive_transmission(self, path):
        yuki_player = self._enquiry("YUKI.N > player")
        yuki_uri = GLib.filename_to_uri(path)
        yuki_player.set_uri(yuki_uri)
        yuki_player.play()

    def __init__(self, parent):
        self._parent = parent
        self._raise("YUKI.N > register paths object", self)
