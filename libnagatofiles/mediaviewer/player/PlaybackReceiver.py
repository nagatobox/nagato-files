
from libnagato4.Object import NagatoObject
from libnagatofiles.mediaviewer.signal import Playback as Signal

DIRECT = {Signal.PLAY: "play", Signal.PAUSE: "pause"}
DISPATCH = {Signal.SEEK: "_seek_for_rate", Signal.REWIND: "_rewind"}


class NagatoPlaybackReceiver(NagatoObject):

    def _seek_for_rate(self, rate):
        self._player.seek(self._player.get_duration()*rate)

    def _rewind(self, void_data=None):
        yuki_rate = self._player.get_position()/self._player.get_duration()
        if 0.01 >= yuki_rate:
            self._raise("YUKI.N > move path", -1)
        else:
            self._seek_for_rate(0)

    def receive_transmission(self, user_data):
        yuki_signal, yuki_data = user_data
        if yuki_signal in DIRECT:
            yuki_method = getattr(self._player, DIRECT[yuki_signal])
            yuki_method()
        elif yuki_signal in DISPATCH:
            yuki_method = getattr(self, DISPATCH[yuki_signal])
            yuki_method(yuki_data)

    def __init__(self, parent):
        self._parent = parent
        self._player = self._enquiry("YUKI.N > player")
        self._raise("YUKI.N > register playback object", self)
