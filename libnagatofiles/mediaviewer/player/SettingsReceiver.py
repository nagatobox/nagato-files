
from libnagato4.Object import NagatoObject


class NagatoSettingsReceiver(NagatoObject):

    def receive_transmission(self, user_data):
        yuki_type, yuki_key, yuki_value = user_data
        if yuki_type == "player" and yuki_key == "software_volume":
            yuki_rate = float(yuki_value)
            yuki_player = self._enquiry("YUKI.N > player")
            yuki_player.set_volume(yuki_rate)

    def __init__(self, parent):
        self._parent = parent
        self._raise("YUKI.N > register settings object", self)
