
from libnagato4.Object import NagatoObject
from libnagatofiles.mediaviewer.signal import Playback as Signal


class NagatoEvents(NagatoObject):

    def _on_position_updated(self, player, position):
        yuki_data = Signal.POSITION_UPDATED, (position, self._duration)
        self._raise("YUKI.N > playback signal", yuki_data)

    def _on_duration_changed(self, player, duration):
        self._duration = duration

    def _on_end_of_stream(self, player):
        self._raise("YUKI.N > move path", 1)

    def __init__(self, parent):
        self._parent = parent
        self._duration = 0
        yuki_player = self._enquiry("YUKI.N > player")
        yuki_player.connect("position-updated", self._on_position_updated)
        yuki_player.connect("duration-changed", self._on_duration_changed)
        yuki_player.connect("end-of-stream", self._on_end_of_stream)
