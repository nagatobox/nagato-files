
from libnagato4.Ux import Unit
from libnagato4.widget.HBox import AbstractHBox
from libnagatofiles.mediaviewer.button.Rewind import NagatoRewind
from libnagatofiles.mediaviewer.button.Play import NagatoPlay
from libnagatofiles.mediaviewer.button.Pause import NagatoPause
from libnagatofiles.mediaviewer.button.Next import NagatoNext
from libnagatofiles.mediaviewer.seek.Bar import NagatoBar
from libnagatofiles.mediaviewer.volume.Button import NagatoButton


class NagatoActionBar(AbstractHBox):

    def _on_initialize(self):
        self.set_size_request(-1, Unit(5))
        NagatoRewind(self)
        NagatoPlay(self)
        NagatoPause(self)
        NagatoNext(self)
        NagatoBar(self)
        NagatoButton(self)
        self._raise("YUKI.N > css", (self, "toolbar"))
        self._raise("YUKI.N > add to box", self)
