
from gi.repository import Gtk
from gi.repository import GLib
from libnagato4.Object import NagatoObject


class NagatoLabel(Gtk.Label, NagatoObject):

    def _get_text(self):
        return "media viewer"

    def receive_transmission(self, path):
        yuki_text = GLib.path_get_basename(path)
        self.set_text(yuki_text)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self)
        self.set_can_focus(False)
        self._raise("YUKI.N > add to event box", self)
        self._raise("YUKI.N > register paths object", self)
