
from libnagato4.Object import NagatoObject
from libnagato4.popover.scale.Cache import NagatoCache
from libnagato4.popover.scale.Delay import NagatoDelay
from libnagatofiles.mediaviewer.volume.DrawingArea import NagatoDrawingArea


class NagatoData(NagatoObject):

    def _yuki_n_rate(self, rate):
        self._cache.try_set_cache(rate)

    def _yuki_n_box_mapped(self):
        self._cache.reset()

    def _inform_rate(self):
        return self._cache.progress

    def _yuki_n_register_data_object(self, object_):
        self._cache.register_data_object(object_)

    def __init__(self, parent):
        self._parent = parent
        self._cache = NagatoCache(self)
        NagatoDelay(self)
        NagatoDrawingArea(self)
