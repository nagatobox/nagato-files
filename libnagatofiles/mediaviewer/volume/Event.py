
from gi.repository import Gdk
from libnagato4.Object import NagatoObject
from libnagato4.Ux import Unit

MASK = Gdk.EventMask.BUTTON_PRESS_MASK | Gdk.EventMask.BUTTON_MOTION_MASK
OFFSET = Unit(2)


class NagatoEvent(NagatoObject):

    def _get_new_rate(self, mouse_x, drawing_area_width):
        yuki_rate = 1-(mouse_x-OFFSET)/(drawing_area_width-OFFSET*2)
        return max(0, min(1, round(yuki_rate, 2)))

    def _on_event(self, drawing_area, event):
        yuki_height = drawing_area.get_allocated_height()
        yuki_rate = self._get_new_rate(event.y, yuki_height)
        self._raise("YUKI.N > rate", yuki_rate)
        return True  # to cancel popover popdown

    def __init__(self, parent):
        self._parent = parent
        yuki_drawing_area = self._enquiry("YUKI.N > drawing area")
        yuki_drawing_area.add_events(MASK)
        yuki_drawing_area.connect("motion-notify-event", self._on_event)
        yuki_drawing_area.connect("button-press-event", self._on_event)
