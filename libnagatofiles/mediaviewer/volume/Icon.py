
from gi.repository import Gtk

ICON_NAMES = {
    0: "audio-volume-muted-symbolic",
    0.2: "audio-volume-low-symbolic",
    0.4: "audio-volume-medium-symbolic",
    1: "audio-volume-high-symbolic",
}

SIZE = Gtk.IconSize.SMALL_TOOLBAR


def get_for_rate(rate):
    for yuki_rate, yuki_name in ICON_NAMES.items():
        if yuki_rate >= rate:
            return Gtk.Image.new_from_icon_name(yuki_name, SIZE)
