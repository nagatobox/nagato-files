
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.Ux import Unit
from libnagatofiles.mediaviewer.volume.Painter import NagatoPainter
from libnagatofiles.mediaviewer.volume.Event import NagatoEvent


class NagatoDrawingArea(Gtk.DrawingArea, NagatoObject):

    def receive_transmission(self, user_data):
        self.queue_draw()

    def _inform_drawing_area(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        Gtk.DrawingArea.__init__(self)
        NagatoPainter(self)
        NagatoEvent(self)
        self.set_size_request(Unit(4), Unit(20))
        self._raise("YUKI.N > add to box", self)
        self._raise("YUKI.N > register data object", self)
