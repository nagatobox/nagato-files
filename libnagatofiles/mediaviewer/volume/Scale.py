
# this module should be removed.

from libnagato4.Object import NagatoObject
from libnagatofiles.mediaviewer.volume.Data import NagatoData


class NagatoScale(NagatoObject):

    MESSAGE = "YUKI.N > settings"
    CONFIG_QUERY = "player", "software_volume"

    def _yuki_n_progress(self, progress):
        yuki_data = self.CONFIG_QUERY+(str(progress),)
        self._raise(self.MESSAGE, yuki_data)

    def __init__(self, parent):
        self._parent = parent
        NagatoData(self)
