
from libnagatofiles.filer.bar.button.Button import NagatoButton
from libnagatofiles.mediaviewer.volume.Settings import NagatoSettings
from libnagatofiles.mediaviewer.volume.Popover import NagatoPopover


class NagatoButton(NagatoButton):

    NAME = "audio-volume-high-symbolic"

    def _inform_button(self):
        return self

    def _inform_progress(self):
        return self._settings.get_rate()

    def _on_clicked(self, button):
        self._popover.set_relative_to(button)
        self._popover.show_all()

    def _post_initialization(self):
        self._settings = NagatoSettings(self)
        self._popover = NagatoPopover(self)
