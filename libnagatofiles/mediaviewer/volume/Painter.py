
from gi.repository import GLib
from libnagato4.Ux import Unit
from libnagato4.Object import NagatoObject
from libnagato4.popover.scale.RGBA import NagatoRGBA

LINE_WIDTH = Unit(1)/4
OFFSET = Unit(2)
VALUE_RGBA = 1, 1, 1, 1
RADIUS = Unit(1)


class NagatoPainter(NagatoObject):

    def _paint_line(self, cairo_context, size):
        yuki_x = size.width/2
        cairo_context.set_line_width(LINE_WIDTH)
        cairo_context.move_to(yuki_x, OFFSET)
        cairo_context.line_to(yuki_x, size.height-OFFSET)
        cairo_context.stroke()

    def _paint_arc(self, cairo_context, size):
        yuki_rate = self._enquiry("YUKI.N > rate")
        yuki_height = size.height-2*OFFSET
        yuki_y = OFFSET+yuki_height*(1-yuki_rate)
        cairo_context.arc(size.width/2, yuki_y, RADIUS, 0, 2*GLib.PI)
        cairo_context.fill()

    def _on_draw(self, drawing_area, cairo_context):
        self._rgba.reset(cairo_context)
        yuki_size, _ = drawing_area.get_allocated_size()
        self._paint_line(cairo_context, yuki_size)
        self._paint_arc(cairo_context, yuki_size)

    def __init__(self, parent):
        self._parent = parent
        self._rgba = NagatoRGBA(self)
        yuki_drawing_area = self._enquiry("YUKI.N > drawing area")
        yuki_drawing_area.connect("draw", self._on_draw)
