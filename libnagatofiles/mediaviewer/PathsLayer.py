
from libnagato4.Object import NagatoObject
from libnagato4.Transmitter import HaruhiTransmitter
from libnagatofiles.mediaviewer.PlayerLayer import NagatoPlayerLayer
from libnagatofiles.mediaviewer.paths.Model import NagatoModel


class NagatoPathsLayer(NagatoObject):

    def _yuki_n_path_realized(self, path):
        self._transmitter.transmit(path)

    def _yuki_n_register_paths_object(self, object_):
        self._transmitter.register_listener(object_)

    def _yuki_n_new_path(self, path):
        self.set_path(path)

    def _yuki_n_move_path(self, shift):
        self._model.shift_path(shift)

    def set_path(self, path):
        self._model.set_path(path)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = HaruhiTransmitter()
        self._model = NagatoModel(self)
        NagatoPlayerLayer(self)
