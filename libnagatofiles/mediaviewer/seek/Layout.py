
from gi.repository import Pango
from gi.repository import PangoCairo
from libnagato4.Object import NagatoObject
from libnagatofiles.mediaviewer.seek import GtkFont
from libnagatofiles.mediaviewer.seek.Markup import NagatoMarkup

FONT_DESCRIPTION = GtkFont.get(size=0, weight=400)


class NagatoLayout(NagatoObject):

    def _get_y_offset(self, pango_layout, height):
        _, yuki_rectangle_logical = pango_layout.get_extents()
        yuki_layout_height = yuki_rectangle_logical.height/Pango.SCALE
        return (height-yuki_layout_height)/2

    def get_for_context(self, cairo_context):
        yuki_size = self._enquiry("YUKI.N > allocated size")
        yuki_layout = PangoCairo.create_layout(cairo_context)
        yuki_layout.set_alignment(Pango.Alignment.CENTER)
        yuki_layout.set_markup(self._markup.get_current(), -1)
        yuki_layout.set_width(yuki_size.width*Pango.SCALE)
        yuki_layout.set_height(yuki_size.height*Pango.SCALE)
        yuki_layout.set_font_description(FONT_DESCRIPTION)
        return yuki_layout, self._get_y_offset(yuki_layout, yuki_size.height)

    def __init__(self, parent):
        self._parent = parent
        self._markup = NagatoMarkup(self)
