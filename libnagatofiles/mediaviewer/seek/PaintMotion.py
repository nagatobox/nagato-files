
from libnagato4.Ux import Unit
from libnagato4.Object import NagatoObject

HEIGHT = Unit(1)


class NagatoPaintMotion(NagatoObject):

    def paint(self, cairo_context):
        yuki_pointed = self._enquiry("YUKI.N > pointed position")
        if yuki_pointed is None:
            return
        yuki_size = self._enquiry("YUKI.N > allocated size")
        yuki_rgba = self._enquiry("YUKI.N > rgba")
        yuki_red, yuki_green, yuki_blue, yuki_alpha = yuki_rgba
        yuki_new_rgba = yuki_red, yuki_green, yuki_blue, yuki_alpha*0.5
        cairo_context.set_source_rgba(*yuki_new_rgba)
        cairo_context.rectangle(
            0,
            0,
            yuki_size.width * yuki_pointed,
            yuki_size.height
            )
        cairo_context.fill()

    def __init__(self, parent):
        self._parent = parent
