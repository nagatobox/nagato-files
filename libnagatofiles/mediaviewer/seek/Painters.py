
from libnagato4.Object import NagatoObject
from libnagatofiles.mediaviewer.seek.Timings import NagatoTimings
from libnagatofiles.mediaviewer.seek.Color import NagatoColor
from libnagatofiles.mediaviewer.seek.PaintProgress import NagatoPaintProgress
from libnagatofiles.mediaviewer.seek.PaintMarkup import NagatoPaintMarkup
from libnagatofiles.mediaviewer.seek.PaintMotion import NagatoPaintMotion


class NagatoPainters(NagatoObject):

    def _inform_rgba(self):
        return self._color.rgba

    def _inform_timing(self):
        return self._timings.timing

    def paint(self, cairo_context):
        self._motion.paint(cairo_context)
        self._progress.paint(cairo_context)
        self._markup.paint(cairo_context)

    def __init__(self, parent):
        self._parent = parent
        self._color = NagatoColor(self)
        self._timings = NagatoTimings(self)
        self._progress = NagatoPaintProgress(self)
        self._markup = NagatoPaintMarkup(self)
        self._motion = NagatoPaintMotion(self)
