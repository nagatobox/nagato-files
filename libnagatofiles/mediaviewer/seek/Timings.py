
from libnagato4.Object import NagatoObject
from libnagatofiles.mediaviewer.signal import Playback as Signal


class NagatoTimings(NagatoObject):

    def receive_transmission(self, user_data):
        yuki_signal, yuki_data = user_data
        if yuki_signal == Signal.POSITION_UPDATED:
            self._current = yuki_data
            self._raise("YUKI.N > queue draw")

    def __init__(self, parent):
        self._parent = parent
        self._current = 0, 1
        self._raise("YUKI.N > register playback object", self)

    @property
    def timing(self):
        return self._current
