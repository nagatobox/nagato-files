
from gi.repository import Gtk
from libnagato4.Object import NagatoObject


class NagatoDrawingArea(Gtk.DrawingArea, NagatoObject):

    def _on_realize(self, *args):
        yuki_gdk_window = self.get_window()
        yuki_xid = yuki_gdk_window.get_xid()
        self._raise("YUKI.N > drawing area syncable", yuki_xid)

    def __init__(self, parent):
        self._parent = parent
        Gtk.DrawingArea.__init__(self)
        self.set_hexpand(True)
        self.set_vexpand(True)
        self.connect("realize", self._on_realize)
        self._raise("YUKI.N > add to box", self)
