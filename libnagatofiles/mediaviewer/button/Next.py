
from gi.repository import Gtk
from libnagatofiles.filer.bar.button.Button import NagatoButton
from libnagatofiles.mediaviewer.signal import Playback as Signal


class NagatoNext(NagatoButton):

    NAME = "media-seek-forward-symbolic"
    TOOLTIP = "Forward"
    SIZE = Gtk.IconSize.SMALL_TOOLBAR

    def _on_clicked(self, button):
        self._raise("YUKI.N > move path", 1)
