
from libnagato4.mainline.NotificationLayer import AbstractNotificationLayer
from libnagatofiles.mainline.MainPopoverLayer import (
    NagatoMainPopoverLayer
    )


class NagatoHeaderNotifyLayer(AbstractNotificationLayer):

    def _on_initialize(self):
        NagatoMainPopoverLayer(self)
