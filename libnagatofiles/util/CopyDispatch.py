
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagatofiles.util.CopyFile import HaruhiCopyFile
from libnagatofiles.util.CopyDirectory import NagatoCopyDirectory


class NagatoCopyDispatch(NagatoObject):

    def _yuki_n_directory_found(self, user_data):
        self.dispatch(*user_data)

    def dispatch(self, directory, source_path):
        if GLib.file_test(source_path, GLib.FileTest.IS_DIR):
            self._copy_directory.to_directory(directory, source_path)
        else:
            self._copy_file.to_directory(directory, source_path)

    def __init__(self, parent):
        self._parent = parent
        self._copy_file = HaruhiCopyFile()
        self._copy_directory = NagatoCopyDirectory(self)
