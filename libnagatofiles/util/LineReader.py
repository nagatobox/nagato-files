
from gi.repository import Gio


class HaruhiLineReader:

    def _initialize_stream(self, path=None):
        if path is None:
            return
        if path != self._path:
            self._path = path
            yuki_gio_file = Gio.File.new_for_path(self._path)
            self._stream = Gio.DataInputStream.new(yuki_gio_file.read(None))

    def read(self, path=None):
        self._initialize_stream(path)
        while self._path is not None:
            yuki_line, yuki_length = self._stream.read_line_utf8(None)
            if yuki_line is None:
                break
            yield yuki_line

    def __init__(self, path=None):
        self._path = None
        self._initialize_stream(path)
