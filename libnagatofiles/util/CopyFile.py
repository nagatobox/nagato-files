
from gi.repository import GLib
from gi.repository import Gio
from libnagatofiles.Mikuru import SafePath2

COPY_FLAG = Gio.FileCopyFlags.ALL_METADATA


class HaruhiCopyFile:

    def _get_destination_file(self, directory, source_path):
        yuki_basename = GLib.path_get_basename(source_path)
        yuki_target = GLib.build_filenamev([directory, yuki_basename])
        yuki_safe_path = SafePath2.get_path(yuki_target)
        return Gio.File.new_for_path(yuki_safe_path)

    def to_directory(self, directory, source_path):
        yuki_source_file = Gio.File.new_for_path(source_path)
        yuki_dest_file = self._get_destination_file(directory, source_path)
        yuki_source_file.copy(yuki_dest_file, COPY_FLAG)
