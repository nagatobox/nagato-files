
from gi.repository import GLib
from libnagato4.Object import NagatoObject


class NagatoAddBookmark(NagatoObject):

    def _get_bookmark_file_path(self):
        yuki_config_directory = GLib.get_user_config_dir()
        yuki_names = [yuki_config_directory, "gtk-3.0", "bookmarks"]
        return GLib.build_filenamev(yuki_names)

    def _is_directory(self, uri):
        yuki_path, _ = GLib.filename_from_uri(uri)
        return GLib.file_test(yuki_path, GLib.FileTest.IS_DIR)

    def _get_addtional_lines(self, uris):
        yuki_data = ""
        for yuki_uri in uris:
            if not self._is_directory(yuki_uri):
                continue
            yuki_path, _ = GLib.filename_from_uri(yuki_uri)
            yuki_basename = GLib.filename_display_basename(yuki_path)
            yuki_data += "{} {}\n".format(yuki_uri, yuki_basename)
        return yuki_data

    def add_uris(self, uris):
        yuki_addtional_lines = self._get_addtional_lines(uris)
        if yuki_addtional_lines == "":
            return
        _, yuki_contents = GLib.file_get_contents(self._path)
        yuki_contents = yuki_contents.decode("utf-8")+yuki_addtional_lines
        GLib.file_set_contents(self._path, yuki_contents.encode())
        self._raise("YUKI.N > bookmark changed")

    def __init__(self, parent):
        self._parent = parent
        self._path = self._get_bookmark_file_path()
