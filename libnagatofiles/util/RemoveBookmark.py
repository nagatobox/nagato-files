
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagatofiles.util.LineReader import HaruhiLineReader


class NagatoRemoveBookmark(NagatoObject):

    def _get_bookmark_file_path(self):
        yuki_names = [GLib.get_user_config_dir(), "gtk-3.0", "bookmarks"]
        return GLib.build_filenamev(yuki_names)

    def _get_addtional_line(self, path):
        yuki_uri = GLib.filename_to_uri(path)
        yuki_basename = GLib.filename_display_basename(path)
        return "{} {}".format(yuki_uri, yuki_basename), yuki_uri

    def _get_contents(self, path):
        yuki_removal = self._get_addtional_line(path)
        yuki_contents = ""
        yuki_reader = HaruhiLineReader()
        for yuki_line in yuki_reader.read(self._path):
            if yuki_line in yuki_removal:
                continue
            yuki_contents += yuki_line+"\n"
        return yuki_contents

    def remove_path(self, path):
        yuki_contents = self._get_contents(path)
        if yuki_contents != "":
            GLib.file_set_contents(self._path, yuki_contents.encode())
            self._raise("YUKI.N > bookmark changed")

    def __init__(self, parent):
        self._parent = parent
        self._path = self._get_bookmark_file_path()
