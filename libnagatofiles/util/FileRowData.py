
from libnagatofiles.Mikuru import File
from libnagatothumbnail.dummy.Loader import HaruhiLoader


class HaruhiFileRowData:

    def get(self, fullpath, file_info):
        yuki_file_type = file_info.get_content_type()
        yuki_is_dir = (yuki_file_type == "inode/directory")
        yuki_time_modified = file_info.get_attribute_uint64("time::modified")
        yuki_file_type = file_info.get_content_type()
        yuki_basename = file_info.get_name()
        return (
            fullpath,
            file_info.get_name(),
            None,
            File.get_path_for_sort(yuki_basename, yuki_is_dir),
            True,
            yuki_file_type,
            File.get_access_new(file_info),
            File.get_age(yuki_time_modified),
            yuki_time_modified,
            self._loader.get_from_file_info(file_info),
            None,
            file_info,
            None,
            None
            )

    def __init__(self):
        self._loader = HaruhiLoader()
