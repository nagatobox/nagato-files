
from libnagato4.Object import NagatoObject


class NagatoLastSelected(NagatoObject):

    def set_last_selected(self, widget, event, tree_path):
        if tree_path is None:
            return
        yuki_model = widget.get_model()
        yuki_tree_row = yuki_model[tree_path]
        yuki_data = yuki_tree_row, tree_path, (event.button == 3)
        self._raise("YUKI.N > last selected item changed", yuki_data)

    def __init__(self, parent):
        self._parent = parent
