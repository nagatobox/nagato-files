
from gi.repository import Gio
from gi.repository import GLib
from libnagato4.Object import NagatoObject


class NagatoCurrent(NagatoObject):

    def _get_startup_directory(self):
        yuki_directory = self._enquiry("YUKI.N > args", "path")
        if yuki_directory is not None:
            return yuki_directory
        return GLib.get_home_dir()

    def _check_moved(self, directory):
        yuki_directory = Gio.File.new_for_path(directory).get_path()
        if self._current_directory != yuki_directory:
            self._current_directory = yuki_directory
            self._raise("YUKI.N > directory moved", yuki_directory)

    def check(self, directory=None):
        if directory is None:
            directory = self._get_startup_directory()
        if GLib.file_test(directory, GLib.FileTest.IS_DIR):
            self._check_moved(directory)

    def __init__(self, parent):
        self._parent = parent
        self._current_directory = ""

    @property
    def current_directory(self):
        return self._current_directory
