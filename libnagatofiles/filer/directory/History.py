
from libnagato4.Object import NagatoObject


class NagatoHistory(NagatoObject):

    def _add_history(self, path):
        if path in self._log:
            yuki_index = self._log.index(path)
            del self._log[yuki_index]
        self._log.insert(0, path)
        if len(self._log) > 12:
            self._log.pop()

    def get_history(self):
        return self._log.copy()

    def receive_transmission(self, directory):
        self._add_history(directory)

    def __init__(self, parent):
        self._parent = parent
        self._log = []
        self._raise("YUKI.N > register current directory object", self)
