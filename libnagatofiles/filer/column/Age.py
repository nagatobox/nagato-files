
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.Mikuru import Columns
from libnagatofiles.filer.column.Interfaces import HaruhiInterfaces
from libnagatofiles.filer.column.RendererText import HaruhiRenderer


class NagatoAge(Gtk.TreeViewColumn, NagatoObject, HaruhiInterfaces):

    def __init__(self, parent):
        self._parent = parent
        yuki_renderer = HaruhiRenderer.new(align=1)
        Gtk.TreeViewColumn.__init__(
            self,
            title="Last Changed",
            cell_renderer=yuki_renderer,
            text=Columns.AGE
            )
        self.set_expand(False)
        self._bind_interfaces(yuki_renderer, Columns.AGE_UNIX_TIME)
        self._raise("YUKI.N > insert column", self)
