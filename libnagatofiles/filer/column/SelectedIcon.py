
from gi.repository import Gtk
from gi.repository import Gio
from libnagato4.Object import NagatoObject
from libnagatofiles.Mikuru import Columns
from libnagatofiles.filer.column.RendererPixbuf import HaruhiRenderer
from libnagatofiles.filer.column.Zebra import HaruhiZebra

ICON_NAME = "object-select-symbolic"


class NagatoSelectedIcon(Gtk.TreeViewColumn, NagatoObject, HaruhiZebra):

    def _addtional_data_func(self, renderer, tree_row):
        if not tree_row[Columns.SELECTED]:
            yuki_icon = Gio.content_type_get_symbolic_icon(ICON_NAME)
            renderer.set_property("gicon", yuki_icon)
        else:
            renderer.set_property("gicon", None)

    def __init__(self, parent):
        self._parent = parent
        yuki_renderer = HaruhiRenderer()
        Gtk.TreeViewColumn.__init__(
            self,
            title="",
            cell_renderer=yuki_renderer
            )
        self._raise("YUKI.N > insert column", self)
        self._bind_zebra_column(yuki_renderer)
