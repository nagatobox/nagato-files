
from libnagatofiles.filer.column.SelectedIcon import NagatoSelectedIcon
from libnagatofiles.filer.column.FileIcon import NagatoFileIcon
from libnagatofiles.filer.column.FileName import NagatoFileName
from libnagatofiles.filer.column.MimeType import NagatoMimeType
from libnagatofiles.filer.column.Permission import NagatoPermission
from libnagatofiles.filer.column.Age import NagatoAge


class AsakuraColumns:

    def __init__(self, parent):
        NagatoSelectedIcon(parent)
        NagatoFileIcon(parent)
        NagatoFileName(parent)
        NagatoMimeType(parent)
        NagatoPermission(parent)
        NagatoAge(parent)
