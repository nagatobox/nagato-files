
SORT_COLUMN_CHANGED = 0     # paired with column-id as integer
SORT_ORDER_CHANGED = 1      # paired with order as integer
FORCE_SORT_COLUMN = 2       # paired with column-id and order as integer
DIRECTORY_REALIZED = 3      # paired with None
REQUEST_REFRESH_TILE = 4    # paired with fullpath as string
