
from libnagato4.Object import NagatoObject
from libnagatofiles.filer.drag.Drag import NagatoDrag
from libnagatofiles.filer.ButtonRelease import NagatoButtonRelease
from libnagatofiles.filer.PathAtPointer import NagatoPathAtPointer


class NagatoUserInput(NagatoObject):

    def _inform_path_at_pointer(self):
        return self._path_at_pointer.get()

    def _inform_is_valid_position(self):
        return self._path_at_pointer.is_valid_position()

    def __init__(self, parent):
        self._parent = parent
        self._path_at_pointer = NagatoPathAtPointer(self)
        NagatoButtonRelease(self)
        NagatoDrag(self)
