
from libnagato4.Object import NagatoObject
from libnagatofiles.filer.mainline.WidgetsLayer import NagatoWidgetsLayer
from libnagatofiles.util.Trash import NagatoTrash
from libnagatofiles.util.CopyTo import NagatoCopyTo
from libnagatofiles.util.SendTo import NagatoSendTo


class NagatoUtilityLayer(NagatoObject):

    def _yuki_n_move_selection(self):
        self._send_to.execute()

    def _yuki_n_copy_selection(self):
        self._copy_to.execute()

    def _yuki_n_delete_selection(self):
        self._trash.execute()

    def __init__(self, parent):
        self._parent = parent
        self._trash = NagatoTrash(self)
        self._copy_to = NagatoCopyTo(self)
        self._send_to = NagatoSendTo(self)
        NagatoWidgetsLayer(self)
