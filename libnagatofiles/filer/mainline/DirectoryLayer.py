
from libnagato4.Object import NagatoObject
from libnagato4.Transmitter import HaruhiTransmitter
from libnagatofiles.filer.mainline.ModelLayer import NagatoModelLayer
from libnagatofiles.filer.directory.Current import NagatoCurrent
from libnagatofiles.filer.directory.History import NagatoHistory


class NagatoDirectoryLayer(NagatoObject):

    def _inform_recent_directories(self):
        return self._history.get_history()

    def _inform_current_directory(self):
        return self._current.current_directory

    def _yuki_n_path_activated(self, path=None):
        if path is not None:
            self._current.check(path)

    def _yuki_n_directory_moved(self, directory):
        self._transmitter.transmit(directory)

    def _yuki_n_register_current_directory_object(self, object_):
        self._transmitter.register_listener(object_)

    def __init__(self, parent, directory=None):
        self._parent = parent
        self._transmitter = HaruhiTransmitter()
        self._current = NagatoCurrent(self)
        self._history = NagatoHistory(self)
        NagatoModelLayer(self)
        self._current.check(directory)
