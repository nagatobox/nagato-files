
from libnagato4.widget.VBox import AbstractVBox
from libnagatofiles.filer.bar.Navigation import NagatoNavigation
from libnagatofiles.filer.bar.Action import NagatoAction
from libnagatofiles.filer.Stack import NagatoStack


class NagatoPageBox(AbstractVBox):

    def open_directory(self, directory):
        self._raise("YUKI.N > path activated", directory)

    def get_id(self):
        return self._enquiry("YUKI.N > page id")

    def raise_message(self, message, user_data=None):
        if user_data is not None:
            self._raise(message, user_data)
        else:
            self._raise(message)

    def _yuki_n_switch_to(self, visible_child):
        self._stack.switch_to(visible_child)

    def _on_initialize(self):
        NagatoNavigation(self)
        self._stack = NagatoStack(self)
        NagatoAction(self)
