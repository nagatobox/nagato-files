
from libnagatofiles.filer.bar.button.Selection import NagatoSelection


class NagatoTrash(NagatoSelection):

    NAME = "user-trash-symbolic"
    TOOLTIP = "send to trash bin"

    def _check_addtional_conditions(self):
        yuki_can_move = self._enquiry("YUKI.N > selection can move")
        if not yuki_can_move:
            self.set_sensitive(yuki_can_move)

    def _on_clicked(self, button):
        self._raise("YUKI.N > delete selection")
