
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.filer.popover.ViewMorePopover import NagatoViewMorePopover


class NagatoViewMore(Gtk.Button, NagatoObject):

    NAME = "view-more-symbolic"
    SIZE = Gtk.IconSize.LARGE_TOOLBAR

    def _on_clicked(self, button):
        self._popover.set_relative_to(self)
        self._popover.show_all()

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self)
        yuki_image = Gtk.Image.new_from_icon_name(self.NAME, self.SIZE)
        self.set_image(yuki_image)
        # self.set_can_focus(False)
        self._popover = NagatoViewMorePopover(self)
        self.connect("clicked", self._on_clicked)
        self._raise("YUKI.N > add to box", self)
        self._raise("YUKI.N > css", (self, "toolbar-button"))
