
from libnagatofiles.filer.bar.button.Button import NagatoButton


class NagatoSelection(NagatoButton):

    def _check_addtional_conditions(self):
        pass

    def receive_transmission(self, user_data):
        yuki_type, yuki_data = user_data
        if yuki_type == "selection-changed":
            self.set_sensitive(yuki_data)
            self._check_addtional_conditions()

    def _post_initialization(self):
        self._raise("YUKI.N > register navigation widget", self)
        self.set_sensitive(False)
