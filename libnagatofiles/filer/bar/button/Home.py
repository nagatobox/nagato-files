
from gi.repository import GLib
from libnagatofiles.filer.bar.button.Navigation import NagatoNavigation


class NagatoHome(NagatoNavigation):

    NAME = "go-home-symbolic"
    MESSAGE = "YUKI.N > path activated"
    USER_DATA = "home"
    TOOLTIP = "Home"

    def _on_clicked(self, button):
        self._raise(self.MESSAGE, GLib.get_home_dir())

    def _refresh_sensitive(self):
        yuki_directory = self._enquiry("YUKI.N > current directory")
        self.set_sensitive(yuki_directory != GLib.get_home_dir())
