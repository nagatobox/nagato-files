
from libnagatofiles.filer.bar.button.Navigation import NagatoNavigation


class NagatoSwitchToList(NagatoNavigation):

    NAME = "view-list-symbolic"
    MESSAGE = "YUKI.N > switch to"
    USER_DATA = "treeview"
    TOOLTIP = "Switch To List View"

    def _refresh_sensitive(self):
        pass
