
from libnagatofiles.filer.bar.button.Selection import NagatoSelection
from libnagatofiles.filer.popover.ArchivePopover import NagatoArchivePopover


class NagatoArchive(NagatoSelection):

    NAME = "folder-download-symbolic"
    TOOLTIP = "Create Archive"

    def _on_clicked(self, *args):
        self._popover.set_relative_to(self)
        self._popover.show_all()

    def _post_initialization(self):
        self._raise("YUKI.N > register navigation widget", self)
        self.set_sensitive(False)
        self._popover = NagatoArchivePopover(self)
