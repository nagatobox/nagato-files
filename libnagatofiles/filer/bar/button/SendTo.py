
from libnagatofiles.filer.bar.button.Selection import NagatoSelection


class NagatoSendTo(NagatoSelection):

    NAME = "send-to-symbolic"
    TOOLTIP = "send to"

    def _check_addtional_conditions(self):
        yuki_can_move = self._enquiry("YUKI.N > selection can move")
        if not yuki_can_move:
            self.set_sensitive(yuki_can_move)

    def _on_clicked(self, button):
        self._raise("YUKI.N > move selection")
