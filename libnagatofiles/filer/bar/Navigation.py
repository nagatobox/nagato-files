
from libnagato4.widget.HBox import AbstractHBox
from libnagatofiles.filer.bar.entry.Entry import NagatoEntry
from libnagatofiles.filer.bar.button.SwitchToList import NagatoSwitchToList
from libnagatofiles.filer.bar.button.SwitchToGrid import NagatoSwitchToGrid
from libnagatofiles.filer.bar.button.Home import NagatoHome
from libnagatofiles.filer.bar.button.Recent import NagatoRecent
from libnagatofiles.filer.bar.button.Up import NagatoUp


class NagatoNavigation(AbstractHBox):

    def _on_initialize(self):
        NagatoHome(self)
        NagatoRecent(self)
        NagatoUp(self)
        NagatoEntry(self)
        NagatoSwitchToGrid(self)
        NagatoSwitchToList(self)
        self._raise("YUKI.N > css", (self, "toolbar"))
        self._raise("YUKI.N > add to box", self)
