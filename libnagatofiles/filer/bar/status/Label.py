
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.filer.bar.status.Count import NagatoCount
from libnagatofiles.filer.bar.status.Selection import NagatoSelection


class NagatoLabel(Gtk.Label, NagatoObject):

    def _yuki_n_selection_cleared(self):
        self._count.refresh()

    def _yuki_n_set_text(self, text):
        self.set_text(text)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self)
        self.set_hexpand(True)
        NagatoSelection(self)
        self._count = NagatoCount(self)
        self._raise("YUKI.N > add to box", self)
