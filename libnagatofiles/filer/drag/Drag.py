
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.filer.drag.Event import NagatoEvent
from libnagatofiles.filer.drag.Source import NagatoSource


class NagatoDrag(NagatoObject):

    def _inform_target_entries(self):
        return [self._target_entry]

    def __init__(self, parent):
        self._parent = parent
        yuki_id = self._enquiry("YUKI.N > page id")
        self._target_entry = Gtk.TargetEntry.new("text/uri-list", 0, yuki_id)
        NagatoEvent(self)
        NagatoSource(self)
