
from gi.repository import GLib
from libnagatofiles.Mikuru import Columns
from libnagato4.Object import NagatoObject


class NagatoDirectory(NagatoObject):

    def _get_target_directory(self):
        yuki_tree_path = self._enquiry("YUKI.N > path at pointer")
        if yuki_tree_path is None:
            return self._enquiry("YUKI.N > current directory")
        yuki_model = self._enquiry("YUKI.N > filtered directory model")
        yuki_row = yuki_model[yuki_tree_path]
        if yuki_row[Columns.MIME_TYPE] == "inode/drctory":
            return yuki_row[Columns.FULLPATH]
        return None

    def reset(self):
        self._directory = self._get_target_directory()
        if self._directory is None:
            return False
        return GLib.file_test(self._directory, GLib.FileTest.IS_EXECUTABLE)

    def __init__(self, parent):
        self._parent = parent

    @property
    def target(self):
        return self._directory
