
from libnagato4.Object import NagatoObject
from libnagatofiles.filer.drag.Directory import NagatoDirectory
from libnagatofiles.filer.drag.Action import NagatoAction


class NagatoSafeQueue(NagatoObject):

    def _inform_directory(self):
        return self._directory.target

    def _inform_uris(self):
        return self._uris

    def queue(self, drag_data):
        if not self._directory.reset():
            return
        self._uris = drag_data.get_uris()
        if self._uris:
            self._action.queue()

    def __init__(self, parent):
        self._parent = parent
        self._directory = NagatoDirectory(self)
        self._action = NagatoAction(self)
