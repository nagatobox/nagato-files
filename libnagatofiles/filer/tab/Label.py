
from gi.repository import Gtk
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagatopath import HomeDirectory


class NagatoLabel(Gtk.Label, NagatoObject):

    def _set_text(self, text):
        yuki_text = text if text else "Root Directory"
        self.set_text(yuki_text)
        self.show_all()

    def receive_transmission(self, directory):
        yuki_basename = GLib.basename(directory)
        self.props.tooltip_text = HomeDirectory.shorten(directory)
        self._set_text(yuki_basename)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self)
        self.set_can_focus(False)
        self._raise("YUKI.N > add to event box", self)
        self._raise("YUKI.N > register current directory object", self)
