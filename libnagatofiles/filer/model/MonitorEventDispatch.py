
from gi.repository import Gio
from libnagato4.Object import NagatoObject

DISPATCH = {
    Gio.FileMonitorEvent.MOVED_IN: "_created",
    Gio.FileMonitorEvent.CREATED: "_created",
    Gio.FileMonitorEvent.MOVED_OUT: "_deleted",
    Gio.FileMonitorEvent.DELETED: "_deleted",
    Gio.FileMonitorEvent.RENAMED: "_renamed"
    }


class NagatoEventDispatch(NagatoObject):

    def _created(self, file_alfa, file_bravo=None):
        self._raise("YUKI.N > create", file_alfa)

    def _deleted(self, file_alfa, file_bravo=None):
        yuki_gio_file = self._enquiry("YUKI.N > gio file")
        if file_alfa.get_path() == yuki_gio_file.get_path():
            self._raise("YUKI.N > directory lost")
        else:
            self._raise("YUKI.N > delete", file_alfa)

    def _renamed(self, file_alfa, file_bravo):
        self._raise("YUKI.N > delete", file_alfa)
        self._raise("YUKI.N > create", file_bravo)

    def dispatch(self, _, file_alfa, file_bravo, event):
        if event in DISPATCH:
            yuki_method = getattr(self, DISPATCH[event])
            yuki_method(file_alfa, file_bravo)

    def __init__(self, parent):
        self._parent = parent
