
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagatofiles.Mikuru import Columns
from libnagatothumbnail.Thumbnail import HaruhiThumbnail

TEST_FLAGS = GLib.FileTest.EXISTS | GLib.FileTest.IS_SYMLINK


class NagatoTileSetter(NagatoObject):

    def _get_tree_row(self, path):
        for yuki_row in self._enquiry("YUKI.N > enumerate rows"):
            if yuki_row[Columns.FULLPATH] == path:
                return yuki_row

    def set_for_fullpath(self, fullpath):
        if GLib.file_test(fullpath, TEST_FLAGS):
            yuki_tree_row = self._get_tree_row(fullpath)
            self.set_for_tree_row(yuki_tree_row)

    def set_for_tree_row(self, tree_row):
        try:
            yuki_file_info = tree_row[Columns.FILE_INFO]
        except GLib.Error:
            print("lost or directory has been moved.")
            return
        yuki_fullpath = tree_row[Columns.FULLPATH]
        yuki_tiles = self._thumbnail.get_tiles(yuki_fullpath, yuki_file_info)
        yuki_unselected_tile, yuki_selected_tile = yuki_tiles
        tree_row[Columns.ICON_IMAGE] = yuki_unselected_tile
        tree_row[Columns.UNSELECTED_TILE] = yuki_unselected_tile
        tree_row[Columns.SELECTED_TILE] = yuki_selected_tile

    def __init__(self, parent):
        self._parent = parent
        self._thumbnail = HaruhiThumbnail()
