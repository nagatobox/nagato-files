
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.Mikuru import Columns
from libnagatofiles.filer.basemodel.Observer import NagatoObserver


class NagatoBaseModel(Gtk.ListStore, NagatoObject):

    def _yuki_n_append_data(self, user_data):
        if user_data[Columns.FULLPATH] in self._iters:
            return
        self.append(user_data)
        self._iters[user_data[Columns.FULLPATH]] = user_data

    def _yuki_n_remove_iter(self, fullpath):
        self._iters.pop(fullpath)

    def _yuki_n_clear_base_model(self):
        self._iters.clear()
        self.clear()

    def __init__(self, parent):
        self._parent = parent
        self._iters = {}
        Gtk.ListStore.__init__(self, *Columns.TYPES)
        self.set_sort_column_id(Columns.PATH_FOR_SORT, Gtk.SortType.ASCENDING)
        NagatoObserver(self)
