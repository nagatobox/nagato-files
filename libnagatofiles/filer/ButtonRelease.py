
from libnagato4.Object import NagatoObject
from libnagatofiles.filer.LastSelected import NagatoLastSelected
from libnagatofiles.filer.popover.Popovers import NagatoPopovers


class NagatoButtonRelease(NagatoObject):

    def _get_tree_path(self, widget, event):
        # wrapper method for treeview and iconview defference.
        yuki_value = widget.get_path_at_pos(event.x, event.y)
        return yuki_value[0] if isinstance(yuki_value, tuple) else yuki_value

    def _dispatch(self, widget, event):
        yuki_tree_path = self._get_tree_path(widget, event)
        if event.button in (1, 3):
            self._selection.set_last_selected(widget, event, yuki_tree_path)
        if event.button == 3:
            self._popovers.popup(widget, event, yuki_tree_path)

    def _on_button_release(self, widget, event):
        # abort selection event when header clicked.
        # sort event is handled by treeview-column.
        _, yuki_border = widget.get_border()
        if event.window.get_height() != yuki_border.top:
            self._dispatch(widget, event)

    def __init__(self, parent):
        self._parent = parent
        yuki_widget = self._enquiry("YUKI.N > tree like widget")
        yuki_widget.connect("button-release-event", self._on_button_release)
        self._selection = NagatoLastSelected(parent)
        self._popovers = NagatoPopovers(self)
