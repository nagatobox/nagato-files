
from gi.repository import Gdk
from libnagato4.Object import NagatoObject


class NagatoDevicePosition(NagatoObject):

    def get_position(self):
        yuki_widget = self._enquiry("YUKI.N > tree like widget")
        yuki_window = yuki_widget.get_window()
        yuki_device_position = yuki_window.get_device_position(self._pointer)
        yuki_x, yuki_y = yuki_widget.convert_widget_to_bin_window_coords(
            yuki_device_position.x,
            yuki_device_position.y
            )
        return yuki_x, yuki_y

    def __init__(self, parent):
        self._parent = parent
        yuki_display = Gdk.Display.get_default()
        yuki_seat = yuki_display.get_default_seat()
        self._pointer = yuki_seat.get_pointer()
