
from libnagatofiles.filer.IconView import NagatoIconView
from libnagatofiles.filer.scrolledwindow.ScrolledWindow import (
    NagatoScrolledWindow
    )


class NagatoForIconView(NagatoScrolledWindow):

    NAME = "iconview"

    def _on_initialize(self):
        NagatoIconView(self)
