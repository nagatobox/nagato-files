
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.filer.scrolledwindow.ForIconView import NagatoForIconView
from libnagatofiles.filer.scrolledwindow.ForTreeView import NagatoForTreeView

TRANSITION_TYPE = Gtk.StackTransitionType.SLIDE_LEFT_RIGHT


class NagatoStack(Gtk.Stack, NagatoObject):

    def _yuki_n_add_to_stack_named(self, user_data):
        yuki_widget, yuki_name = user_data
        self.add_named(yuki_widget, yuki_name)

    def switch_to(self, visible_child):
        self.set_visible_child_full(visible_child, TRANSITION_TYPE)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(self)
        NagatoForIconView(self)
        NagatoForTreeView(self)
        self._raise("YUKI.N > add to box", self)
