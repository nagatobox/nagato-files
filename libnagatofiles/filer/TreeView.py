
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.filer.column.Columns import AsakuraColumns
from libnagatofiles.filer.UserInput import NagatoUserInput
from libnagatofiles.Mikuru import Columns


class NagatoTreeView(Gtk.TreeView, NagatoObject):

    def _inform_adjustments(self):
        _, yuki_border = self.get_border()
        return -1*yuki_border.top, -1*yuki_border.left

    def _on_row_activated(self, tree_view, tree_path, column):
        yuki_model = tree_view.get_model()
        yuki_path = yuki_model[tree_path][Columns.FULLPATH]
        self._raise("YUKI.N > path activated", yuki_path)

    def _inform_tree_like_widget(self):
        return self

    def _yuki_n_insert_column(self, column):
        self.insert_column(column, -1)

    def __init__(self, parent):
        self._parent = parent
        yuki_model = self._enquiry("YUKI.N > filtered directory model")
        Gtk.TreeView.__init__(self, model=yuki_model)
        AsakuraColumns(self)
        NagatoUserInput(self)
        self._raise("YUKI.N > add to scrolled window", self)
        self.connect("row-activated", self._on_row_activated)
