
from libnagatofiles.Mikuru import Columns
from libnagatofiles.filer.popover.launch.Viewer import AbstractViewer


class NagatoPdfViewer(AbstractViewer):

    LABEL = "Show in PDF Viewer"
    ID = "pdf-viewer"

    def _on_map(self, revealer):
        yuki_tree_row = self._enquiry("YUKI.N > prime last selected row")
        yuki_type = yuki_tree_row[Columns.MIME_TYPE]
        yuki_revealed = (yuki_type == "application/pdf")
        revealer.set_reveal_child(yuki_revealed)
