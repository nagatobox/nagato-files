
from libnagatofiles.Mikuru import Columns
from libnagatofiles.filer.popover.launch.Viewer import AbstractViewer


class NagatoImageViewer(AbstractViewer):

    LABEL = "Show in Image Viewer"
    ID = "image-viewer"

    def _on_map(self, revealer):
        yuki_tree_row = self._enquiry("YUKI.N > prime last selected row")
        yuki_type = yuki_tree_row[Columns.MIME_TYPE]
        yuki_revealed = yuki_type.startswith("image")
        revealer.set_reveal_child(yuki_revealed)
