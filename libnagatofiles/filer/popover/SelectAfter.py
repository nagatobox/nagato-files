
from libnagatofiles.filer.popover.MultiSelection import NagatoMultiSelection


class NagatoSelectAfter(NagatoMultiSelection):

    METHOD = "iter_next"
    SELECTION = False
    TITLE = "Select After"
