
from libnagatofiles.filer.popover.FilerPopover import NagatoFilePopover
from libnagatofiles.filer.popover.VoidBox import NagatoVoidBox
from libnagatofiles.filer.popover.SortBox import NagatoSortBox
from libnagatofiles.filer.popover.MakeDirectoryBox import (
    NagatoMakeDirectoryBox
    )


class NagatoVoidPopover(NagatoFilePopover):

    def _yuki_n_loopback_add_popover_groups(self, parent):
        NagatoVoidBox(parent)
        NagatoSortBox(parent)
        NagatoMakeDirectoryBox(parent)
