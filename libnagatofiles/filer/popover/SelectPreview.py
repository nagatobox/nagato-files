
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.Mikuru import Columns
from libnagatofiles.filer.popover.ChangePreview import NagatoChangePreview


class NagatoSelectPreview(Gtk.Revealer, NagatoObject):

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)

    def _on_map(self, revealer):
        yuki_tree_row = self._enquiry("YUKI.N > prime last selected row")
        yuki_type = yuki_tree_row[Columns.MIME_TYPE]
        revealer.set_reveal_child(yuki_type.startswith("video"))

    def __init__(self, parent):
        self._parent = parent
        Gtk.Revealer.__init__(self)
        self.set_transition_type(Gtk.RevealerTransitionType.NONE)
        NagatoChangePreview(self)
        self.set_reveal_child(False)
        self.connect("map", self._on_map)
        self._raise("YUKI.N > add to box", self)
