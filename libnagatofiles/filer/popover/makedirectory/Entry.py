
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.Ux import Unit
from libnagato4.widget import Margin


class NagatoEntry(Gtk.Entry, NagatoObject):

    def _on_activate(self, entry):
        if self._enquiry("YUKI.N > appliable"):
            self._raise("YUKI.N > popdown")
            self._raise("YUKI.N > apply")

    def _on_map(self, entry):
        self._raise("YUKI.N > mapped")
        self.grab_focus()
        yuki_basename = "new directory"
        self.set_text(yuki_basename)
        self.select_region(0, len(yuki_basename))

    def _on_changed(self, *args):
        self._raise("YUKI.N > edited", self.get_text())

    def __init__(self, parent):
        self._parent = parent
        Gtk.Entry.__init__(self)
        Margin.set_with_unit(self, 1, 1, 1, 1)
        self.set_size_request(Unit(60), Unit(2))
        self.connect("map", self._on_map)
        self.connect("changed", self._on_changed)
        self.connect("activate", self._on_activate)
        self._raise("YUKI.N > add to box", self)
