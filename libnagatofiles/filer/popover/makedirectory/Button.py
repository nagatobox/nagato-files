
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.widget import Margin


class NagatoButton(Gtk.Button, NagatoObject):

    def _on_clicked(self, button):
        self._raise("YUKI.N > popdown")
        self._raise("YUKI.N > apply")

    def receive_transmission(self, appliable):
        yuki_appliable = self._enquiry("YUKI.N > appliable")
        self.set_sensitive(yuki_appliable)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, "Make Directory")
        self.props.tooltip_text = "Make Directory"
        self.set_relief(Gtk.ReliefStyle.NONE)
        Margin.set_with_unit(self, 1, 1, 0, 1)
        self.connect("clicked", self._on_clicked)
        self._raise("YUKI.N > add to box", self)
        self._raise("YUKI.N > register apply object", self)
