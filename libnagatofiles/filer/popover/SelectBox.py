
from libnagato4.popover.Box import NagatoBox
from libnagato4.popover.main.BackToMain import NagatoBackToMain
from libnagato4.popover.Separator import NagatoSeparator
from libnagatofiles.filer.popover.SelectBefore import NagatoSelectBefore
from libnagatofiles.filer.popover.UnselectBefore import NagatoUnselectBefore
from libnagatofiles.filer.popover.SelectAfter import NagatoSelectAfter
from libnagatofiles.filer.popover.UnselectAfter import NagatoUnselectAfter
from libnagatofiles.filer.popover.SelectAll import NagatoSelectAll
from libnagatofiles.filer.popover.UnselectAll import NagatoUnselectAll
from libnagatofiles.filer.popover.InvertSelection import NagatoInvertSelection


class NagatoSelectBox(NagatoBox):

    NAME = "select"

    def _on_initialize(self):
        NagatoBackToMain(self)
        NagatoSeparator(self)
        NagatoSelectBefore(self)
        NagatoUnselectBefore(self)
        NagatoSelectAfter(self)
        NagatoUnselectAfter(self)
        NagatoSeparator(self)
        NagatoSelectAll(self)
        NagatoUnselectAll(self)
        NagatoInvertSelection(self)
