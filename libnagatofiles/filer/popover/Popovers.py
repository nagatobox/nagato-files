
from libnagato4.Object import NagatoObject
from libnagatofiles.filer.popover.FilePopover import NagatoFilePopover
from libnagatofiles.filer.popover.ViodPopover import NagatoVoidPopover
from libnagatofiles.filer.popover.DirectoryPopover import (
    NagatoDirectoryPopover
    )


class NagatoPopovers(NagatoObject):

    def _popup_context_menu(self, widget, event):
        if self._enquiry("YUKI.N > prime is dir"):
            self._directory_popover.popup_for_widget(widget, event)
        else:
            self._file_popover.popup_for_widget(widget, event)

    def popup(self, widget, event, tree_path):
        if tree_path is not None:
            self._popup_context_menu(widget, event)
        else:
            self._void_popover.popup_for_widget(widget, event)

    def __init__(self, parent):
        self._parent = parent
        self._directory_popover = NagatoDirectoryPopover(self)
        self._file_popover = NagatoFilePopover(self)
        self._void_popover = NagatoVoidPopover(self)
