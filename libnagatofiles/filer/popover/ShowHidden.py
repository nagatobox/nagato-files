
from gi.repository import Gtk
from libnagato4.popover.item.Item import NagatoItem

ICON_NAME = "object-select-symbolic"
ICON_SIZE = Gtk.IconSize.MENU


class NagatoShowHidden(NagatoItem):

    TITLE = "Show Hidden"
    MESSAGE = "YUKI.N > toggle show hidden"

    def _on_map(self, *args):
        if self._enquiry("YUKI.N > show hidden"):
            self.set_image(Gtk.Image.new_from_icon_name(ICON_NAME, ICON_SIZE))
        else:
            self.set_image(None)

    def _on_initialize(self):
        self.set_image_position(Gtk.PositionType.LEFT)
