
from libnagatofiles.filer.popover.maneuver.CopyItem import NagatoCopyItem
from libnagatofiles.filer.popover.maneuver.Move import NagatoMove
from libnagatofiles.filer.popover.maneuver.Delete import NagatoDelete


class AsakuraManeuvers:

    def __init__(self, parent):
        NagatoCopyItem(parent)
        NagatoMove(parent)
        NagatoDelete(parent)
