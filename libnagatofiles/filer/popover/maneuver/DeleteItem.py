
from libnagato4.popover.item.Item import NagatoItem


class NagatoDeleteItem(NagatoItem):

    TITLE = "Delete"
    MESSAGE = "YUKI.N > delete selection"
    USER_DATA = None
