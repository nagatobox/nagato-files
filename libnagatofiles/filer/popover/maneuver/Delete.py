
from libnagatofiles.popover.Revealer import AbstractRevealer
from libnagatofiles.filer.popover.maneuver.DeleteItem import NagatoDeleteItem


class NagatoDelete(AbstractRevealer):

    def _on_map(self, revealer):
        yuki_can_move = self._enquiry("YUKI.N > selection can move")
        self.set_reveal_child(yuki_can_move)

    def _on_initialize(self):
        NagatoDeleteItem(self)
