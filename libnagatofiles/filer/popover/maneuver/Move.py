
from libnagatofiles.popover.Revealer import AbstractRevealer
from libnagatofiles.filer.popover.maneuver.MoveItem import NagatoMoveItem


class NagatoMove(AbstractRevealer):

    def _on_map(self, revealer):
        yuki_can_move = self._enquiry("YUKI.N > selection can move")
        self.set_reveal_child(yuki_can_move)

    def _on_initialize(self):
        NagatoMoveItem(self)
