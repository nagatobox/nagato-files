
from libnagato4.popover.item.Item import NagatoItem


class NagatoCopyItem(NagatoItem):

    TITLE = "Copy To..."
    MESSAGE = "YUKI.N > copy selection"
    USER_DATA = None
