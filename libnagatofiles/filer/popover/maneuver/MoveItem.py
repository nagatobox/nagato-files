
from libnagato4.popover.item.Item import NagatoItem


class NagatoMoveItem(NagatoItem):

    TITLE = "Move To..."
    MESSAGE = "YUKI.N > move selection"
    USER_DATA = None
