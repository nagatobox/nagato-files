
from libnagatofiles.Mikuru import Columns
from libnagato4.popover.item.Item import NagatoItem


class NagatoMultiSelection(NagatoItem):

    METHOD = "iter_next"  # or "iter_previous"
    SELECTION = True  # or False
    TITLE = ""  # define title here.

    def _on_clicked(self, button):
        self._raise("YUKI.N > popdown")
        yuki_row = self._enquiry("YUKI.N > prime last selected row")
        yuki_iter = yuki_row.iter
        yuki_model = self._enquiry("YUKI.N > filtered directory model")
        while yuki_iter is not None:
            yuki_model[yuki_iter][Columns.SELECTED] = self.SELECTION
            yuki_method = getattr(yuki_model, self.METHOD)
            yuki_iter = yuki_method(yuki_iter)
