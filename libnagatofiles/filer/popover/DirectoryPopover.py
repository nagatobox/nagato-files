
from libnagatofiles.filer.popover.FilerPopover import NagatoFilePopover
from libnagatofiles.filer.popover.DirectoryBox import NagatoDirectoryBox
from libnagatofiles.filer.popover.SortBox import NagatoSortBox
from libnagatofiles.filer.popover.SelectBox import NagatoSelectBox
from libnagatofiles.filer.popover.RenameBox import NagatoRenameBox


class NagatoDirectoryPopover(NagatoFilePopover):

    def _yuki_n_loopback_add_popover_groups(self, parent):
        NagatoDirectoryBox(parent)
        NagatoSortBox(parent)
        NagatoSelectBox(parent)
        NagatoRenameBox(parent)
