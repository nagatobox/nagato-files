
from libnagato4.popover.Box import NagatoBox
from libnagato4.popover.Separator import NagatoSeparator
from libnagatofiles.filer.popover.ToApps import NagatoToApps
from libnagatofiles.filer.popover.SelectPreview import NagatoSelectPreview
from libnagatofiles.filer.popover.Extract import NagatoExtract
from libnagatofiles.filer.popover.launch.MediaViewer import NagatoMediaViewer
from libnagatofiles.filer.popover.launch.ImageViewer import NagatoImageViewer
from libnagatofiles.filer.popover.launch.PdfViewer import NagatoPdfViewer
from libnagatofiles.filer.popover.EditTag import NagatoEditTag
from libnagatofiles.filer.popover.ShowPreview import NagatoShowPreview
from libnagatofiles.filer.popover.rename.ToRename import NagatoToRename
from libnagatofiles.filer.popover.CopyUri import NagatoCopyUri
from libnagatofiles.filer.popover.CopyPath import NagatoCopyPath
from libnagatofiles.filer.popover.ToSort import NagatoToSort
from libnagatofiles.filer.popover.ToSelect import NagatoToSelect
from libnagatofiles.filer.popover.maneuver.Maneuvers import AsakuraManeuvers


class NagatoFileBox(NagatoBox):

    NAME = "main"

    def _on_initialize(self):
        NagatoToApps(self)
        NagatoMediaViewer(self)
        NagatoImageViewer(self)
        NagatoPdfViewer(self)
        NagatoExtract(self)
        NagatoEditTag(self)
        NagatoSelectPreview(self)
        NagatoShowPreview(self)
        NagatoToRename(self)
        NagatoCopyUri(self)
        NagatoCopyPath(self)
        NagatoSeparator(self)
        NagatoToSort(self)
        NagatoToSelect(self)
        NagatoSeparator(self)
        AsakuraManeuvers(self)
