
from gi.repository import Gtk
from libnagatofiles.Mikuru import Columns
from libnagato4.popover.item.Item import NagatoItem
from libnagatofiles.filer.signal import Model as Signal
from libnagatofiles.chooser.mp4tag.Context import NagatoContext
from libnagatofiles.chooser.mp4tag.Dialog import NagatoDialog


class NagatoEditTagItem(NagatoItem):

    TITLE = "Edit Media Tag"

    def _try_set_mp4_tags(self, path):
        yuki_context = NagatoContext.new_for_path(self, path)
        yuki_response = NagatoDialog.run_with_context(self, yuki_context)
        if yuki_response != Gtk.ResponseType.APPLY:
            return
        yuki_context.apply()
        if yuki_context["coverart-path"] != "":
            yuki_data = Signal.REQUEST_REFRESH_TILE, path
            self._raise("YUKI.N > model signal", yuki_data)

    def _on_clicked(self, button):
        self._raise("YUKI.N > popdown")
        yuki_tree_row = self._enquiry("YUKI.N > prime last selected row")
        yuki_path = yuki_tree_row[Columns.FULLPATH]
        yuki_type = yuki_tree_row[Columns.MIME_TYPE]
        if yuki_type == "audio/mp4":
            self._try_set_mp4_tags(yuki_path)
