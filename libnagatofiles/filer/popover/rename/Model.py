
from gi.repository import Gio
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagato4.Transmitter import HaruhiTransmitter
from libnagatofiles.Mikuru import SafePath2
from libnagatofiles.filer.popover.rename.Box import NagatoBox


class NagatoModel(NagatoObject):

    def _inform_basename(self):
        return self._source_file.get_basename()

    def _yuki_n_apply(self):
        yuki_names = [self._source_file.get_parent().get_path(), self._edited]
        yuki_target_path = GLib.build_filenamev(yuki_names)
        yuki_safe_path = SafePath2.get_path(yuki_target_path)
        yuki_destination_file = Gio.File.new_for_path(yuki_safe_path)
        self._source_file.move(yuki_destination_file, Gio.FileCopyFlags.NONE)

    def _yuki_n_edited(self, basename):
        self._edited = basename
        yuki_appliable = (basename != "" and basename != self._original)
        self._transmitter.transmit(yuki_appliable)

    def _yuki_n_mapped(self):
        yuki_path = self._enquiry("YUKI.N > prime path")
        self._source_file = Gio.File.new_for_path(yuki_path)
        self._original = self._source_file.get_basename()
        self._edited = self._original

    def _yuki_n_register_apply_object(self, object_):
        self._transmitter.register_listener(object_)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = HaruhiTransmitter()
        NagatoBox(self)
