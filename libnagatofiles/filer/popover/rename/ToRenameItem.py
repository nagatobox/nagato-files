
from libnagato4.popover.item.ToStack import NagatoToStack


class NagatoToRenameItem(NagatoToStack):

    TITLE = "Rename"
    WHERE_TO_GO = "rename"
