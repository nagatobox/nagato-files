
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.filer.popover.rename.Entry import NagatoEntry
from libnagatofiles.filer.popover.rename.Button import NagatoButton


class NagatoBox(Gtk.Box, NagatoObject):

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self._raise("YUKI.N > add to box", self)
        NagatoEntry(self)
        NagatoButton(self)
