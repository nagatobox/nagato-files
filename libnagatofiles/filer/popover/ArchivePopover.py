
from libnagato4.popover.SingleStack import AbstractSingleStack
# from libnagatofiles.filer.popover.archive.Rar import NagatoRar
from libnagatofiles.filer.popover.archive.Tar import NagatoTar
from libnagatofiles.filer.popover.archive.TarGz import NagatoTarGz
from libnagatofiles.filer.popover.archive.TarXz import NagatoTarXz
from libnagatofiles.filer.popover.archive.Zip import NagatoZip


class NagatoArchivePopover(AbstractSingleStack):

    def _add_to_single_stack(self):
        # NagatoRar(self)
        NagatoTar(self)
        NagatoTarGz(self)
        NagatoTarXz(self)
        NagatoZip(self)
