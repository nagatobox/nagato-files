
from gi.repository import Gtk
from libnagato4.popover.SingleStack import AbstractSingleStack
from libnagatofiles.filer.popover.CloseTab import NagatoCloseTab


class NagatoTabPopover(AbstractSingleStack):

    def _add_to_single_stack(self):
        self.set_position(Gtk.PositionType.BOTTOM)
        NagatoCloseTab(self)
