
from gi.repository import Gio
from libnagato4.popover.item.Item import NagatoItem


class NagatoOpenWith(NagatoItem):

    @classmethod
    def new_for_app_info(cls, parent, app_info):
        yuki_item = cls(parent)
        yuki_item.set_app_info(app_info)

    def _on_clicked(self, button):
        self._raise("YUKI.N > popdown")
        yuki_path = self._enquiry("YUKI.N > prime path")
        yuki_gio_file = Gio.File.new_for_path(yuki_path)
        self._app_info.launch([yuki_gio_file], None)

    def set_app_info(self, app_info):
        self._app_info = app_info
        self.props.tooltip_text = app_info.get_description()
        self.set_label(app_info.get_name())
