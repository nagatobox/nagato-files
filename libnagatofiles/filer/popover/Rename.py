
from libnagatofiles.dialog.Rename import NagatoRename as Dialog
from gi.repository import Gio
from gi.repository import GLib
from libnagatofiles.Mikuru import SafePath2
from libnagato4.popover.item.Item import NagatoItem


class NagatoRename(NagatoItem):

    TITLE = "Rename"

    def _on_clicked(self, button):
        self._raise("YUKI.N > popdown")
        yuki_path = self._enquiry("YUKI.N > prime path")
        yuki_source_file = Gio.File.new_for_path(yuki_path)
        yuki_response = Dialog.call(default=yuki_source_file.get_basename())
        if not yuki_response:
            return
        yuki_names = [yuki_source_file.get_parent().get_path(), yuki_response]
        yuki_target_path = GLib.build_filenamev(yuki_names)
        yuki_safe_path = SafePath2.get_path(yuki_target_path)
        yuki_destination_file = Gio.File.new_for_path(yuki_safe_path)
        yuki_source_file.move(yuki_destination_file, Gio.FileCopyFlags.NONE)
