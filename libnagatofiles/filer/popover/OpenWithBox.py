
from libnagato4.widget.VBox import AbstractVBox
from libnagatofiles.data.AppInfoCache import NagatoAppInfoCache
from libnagatofiles.filer.popover.OpenWith import NagatoOpenWith


class NagatoOpenWithBox(AbstractVBox):

    def _clear(self):
        for yuki_child in self.get_children():
            yuki_child.destroy()

    def refresh(self):
        self._clear()
        for _, yuki_app_info in self._app_info_cache.get_humansorted():
            NagatoOpenWith.new_for_app_info(self, yuki_app_info)
        self.show_all()

    def _on_initialize(self):
        self._app_info_cache = NagatoAppInfoCache(self)
        self._raise("YUKI.N > add to box", self)
