
from libnagatofiles.Mikuru import Columns
from libnagato4.popover.item.Item import NagatoItem
from libnagatofiles.filer.signal import Model as Signal


class NagatoByMimeType(NagatoItem):

    TITLE = "By Mime Type"
    MESSAGE = "YUKI.N > model signal"
    USER_DATA = Signal.SORT_COLUMN_CHANGED, Columns.MIME_TYPE
