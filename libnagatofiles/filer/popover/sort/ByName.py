
from libnagatofiles.Mikuru import Columns
from libnagato4.popover.item.Item import NagatoItem
from libnagatofiles.filer.signal import Model as Signal


class NagatoByName(NagatoItem):

    TITLE = "By Name"
    MESSAGE = "YUKI.N > model signal"
    USER_DATA = Signal.SORT_COLUMN_CHANGED, Columns.PATH_FOR_SORT
