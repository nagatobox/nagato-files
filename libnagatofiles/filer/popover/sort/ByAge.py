
from libnagatofiles.Mikuru import Columns
from libnagato4.popover.item.Item import NagatoItem
from libnagatofiles.filer.signal import Model as Signal


class NagatoByAge(NagatoItem):

    TITLE = "By Age"
    MESSAGE = "YUKI.N > model signal"
    USER_DATA = Signal.SORT_COLUMN_CHANGED, Columns.AGE_UNIX_TIME
