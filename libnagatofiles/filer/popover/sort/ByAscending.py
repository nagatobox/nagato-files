
from gi.repository import Gtk
from libnagato4.popover.item.Item import NagatoItem
from libnagatofiles.filer.signal import Model as Signal


class NagatoByAscending(NagatoItem):

    TITLE = "Ascending"
    MESSAGE = "YUKI.N > model signal"
    USER_DATA = Signal.SORT_ORDER_CHANGED, Gtk.SortType.ASCENDING
