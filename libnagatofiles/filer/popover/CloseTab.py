
from libnagato4.popover.item.Item import NagatoItem


class NagatoCloseTab(NagatoItem):

    TITLE = "Close"
    MESSAGE = "YUKI.N > close current tab"
    USER_DATA = None
