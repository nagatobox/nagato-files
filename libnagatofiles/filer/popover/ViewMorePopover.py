
from libnagato4.popover.SingleStack import AbstractSingleStack
from libnagatofiles.filer.popover.SelectAll import NagatoSelectAll
from libnagatofiles.filer.popover.UnselectAll import NagatoUnselectAll
from libnagatofiles.filer.popover.InvertSelection import NagatoInvertSelection


class NagatoViewMorePopover(AbstractSingleStack):

    def _add_to_single_stack(self):
        NagatoSelectAll(self)
        NagatoUnselectAll(self)
        NagatoInvertSelection(self)
