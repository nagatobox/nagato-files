
from libnagato4.popover.item.Item import NagatoItem


class NagatoInvertSelection(NagatoItem):

    TITLE = "Invert Selection"
    MESSAGE = "YUKI.N > invert selection"
    USER_DATA = None
