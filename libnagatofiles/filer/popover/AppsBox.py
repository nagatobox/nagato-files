
from libnagato4.popover.Box import NagatoBox
from libnagato4.popover.Separator import NagatoSeparator
from libnagato4.popover.main.BackToMain import NagatoBackToMain
from libnagatofiles.filer.popover.OpenWithBox import NagatoOpenWithBox


class NagatoAppsBox(NagatoBox):

    NAME = "apps"

    def _on_map(self, box):
        self._open_with_box.refresh()

    def _on_initialize(self):
        NagatoBackToMain(self)
        NagatoSeparator(self)
        self._open_with_box = NagatoOpenWithBox(self)
        self.connect("map", self._on_map)
