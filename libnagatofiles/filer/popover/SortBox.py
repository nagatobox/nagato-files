
from libnagato4.popover.Box import NagatoBox
from libnagato4.popover.Separator import NagatoSeparator
from libnagato4.popover.main.BackToMain import NagatoBackToMain
from libnagatofiles.filer.popover.sort.BySelected import NagatoBySelected
from libnagatofiles.filer.popover.sort.ByName import NagatoByName
from libnagatofiles.filer.popover.sort.ByAge import NagatoByAge
from libnagatofiles.filer.popover.sort.ByMimeType import NagatoByMimeType
from libnagatofiles.filer.popover.sort.ByAscending import NagatoByAscending
from libnagatofiles.filer.popover.sort.ByDescending import NagatoByDescending


class NagatoSortBox(NagatoBox):

    NAME = "sort"

    def _on_initialize(self):
        NagatoBackToMain(self)
        NagatoSeparator(self)
        NagatoBySelected(self)
        NagatoByName(self)
        NagatoByAge(self)
        NagatoByMimeType(self)
        NagatoSeparator(self)
        NagatoByAscending(self)
        NagatoByDescending(self)
