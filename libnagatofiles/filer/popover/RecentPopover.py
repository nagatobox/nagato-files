
from gi.repository import Gtk
from libnagato4.popover.SingleStack import AbstractSingleStack
from libnagatofiles.filer.popover.RecentDirectory import NagatoRecentDirectory


class NagatoRecentPopover(AbstractSingleStack):

    def _clear_child_items(self):
        for yuki_child_item in self._box.get_children():
            yuki_child_item.destroy()

    def refresh_items(self):
        self._clear_child_items()
        self.set_position(Gtk.PositionType.BOTTOM)
        for yuki_path in self._enquiry("YUKI.N > recent directories")[1::]:
            NagatoRecentDirectory.new_for_path(self, yuki_path)

    def _add_to_single_stack(self):
        pass
