
import patoolib
from gi.repository import Gtk
from gi.repository import GLib
from libnagato4.popover.item.Item import NagatoItem
from libnagato4.chooser.selectfile.Dialog import NagatoDialog
from libnagatofiles.chooser.archive.Context import NagatoContext


class NagatoArchive(NagatoItem):

    TITLE = "zip"   # define title here.
    TYPE = ".zip"   # archive define type here.

    def _create_archive(self, user_data):
        # this method should be HEAVY PROCESS.
        patoolib.create_archive(*user_data, verbosity=1, interactive=False)
        self._raise("YUKI.N > clear selection")
        return GLib.SOURCE_REMOVE

    def _on_clicked(self, button):
        self._raise("YUKI.N > popdown")
        yuki_directory = self._enquiry("YUKI.N > current directory")
        yuki_context = NagatoContext.new(self, yuki_directory, self.TYPE)
        yuki_response = NagatoDialog.run_with_context(self, yuki_context)
        if yuki_response == Gtk.ResponseType.APPLY:
            yuki_paths = self._enquiry("YUKI.N > selection")
            yuki_data = yuki_context.get_path(), yuki_paths
            GLib.idle_add(self._create_archive, yuki_data)
