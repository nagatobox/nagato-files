
from libnagatofiles.filer.popover.archive.Archive import NagatoArchive


class NagatoTarGz(NagatoArchive):

    TITLE = "tar.gz"
    TYPE = ".tar.gz"
