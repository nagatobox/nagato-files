
from libnagato4.popover.item.ToStack import NagatoToStack


class NagatoToSelect(NagatoToStack):

    TITLE = "Select"
    WHERE_TO_GO = "select"
