
from gi.repository import GLib
from libnagato4.Object import NagatoObject


class NagatoInitializer(NagatoObject):

    def _add_tabs_for_args(self):
        yuki_has_tab = False
        while True:
            yuki_response = self._enquiry("YUKI.N > arg file")
            if yuki_response is None:
                break
            _, yuki_gio_file = yuki_response
            self._raise("YUKI.N > add new tab", yuki_gio_file.get_path())
            yuki_has_tab = True
        return yuki_has_tab

    def __init__(self, parent):
        self._parent = parent
        yuki_has_tab = self._add_tabs_for_args()
        if not yuki_has_tab:
            self._raise("YUKI.N > add new tab", GLib.get_current_dir())
