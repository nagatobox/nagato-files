
MIME = (
    "application/x-tar",
    "application/x-compressed-tar",
    "application/zip",
    "application/x-xz-compressed-tar",
    "application/vnd.debian.binary-package"
)
