
from gi.repository import Gtk
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagatopath import HomeDirectory


class NagatoLabel(Gtk.Label, NagatoObject):

    def receive_transmission(self, path):
        yuki_basename = GLib.filename_display_basename(path)
        self.props.tooltip_text = HomeDirectory.shorten(path)
        self.set_text(yuki_basename)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self)
        self.set_can_focus(False)
        self._raise("YUKI.N > add to event box", self)
        self._raise("YUKI.N > register path object", self)
