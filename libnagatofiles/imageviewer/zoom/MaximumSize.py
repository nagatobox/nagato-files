
from libnagato4.Object import NagatoObject
from libnagatofiles.imageviewer.signal import Pixbuf as Signal
from libnagatofiles.imageviewer.zoom.Rectangle import HaruhiRectangle


class NagatoMaximumSize(NagatoObject):

    def get_zoomed_size(self, pixbuf):
        yuki_width, yuki_height = pixbuf.get_width(), pixbuf.get_height()
        yuki_max_width, yuki_max_height = self._rectangle.get_maximum_size()
        yuki_rate = min(yuki_max_width/yuki_width, yuki_max_height/yuki_height)
        yuki_buffer_rate = min(1, yuki_rate)
        self._raise("YUKI.N > buffer rate", yuki_buffer_rate)
        return yuki_width*yuki_buffer_rate, yuki_height*yuki_buffer_rate

    def receive_transmission(self, user_data):
        yuki_signal, yuki_data = user_data
        if yuki_signal != Signal.SCROLLED_AREA_SIZE_CHANGED:
            return
        if self._rectangle.try_set_size(yuki_data):
            self._raise("YUKI.N > ready")
            self._raise("YUKI.N > request edit")

    def __init__(self, parent):
        self._parent = parent
        self._rectangle = HaruhiRectangle()
        self._raise("YUKI.N > register pixbuf object", self)
