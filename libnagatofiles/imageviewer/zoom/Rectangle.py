

class HaruhiRectangle:

    def _is_different(self, size):
        if self._rectangle.width != size.width:
            return True
        if self._rectangle.height != size.height:
            return True
        return False

    def get_maximum_size(self):
        return self._rectangle.width, self._rectangle.height

    def try_set_size(self, size):
        if self._rectangle is None or self._is_different(size):
            self._rectangle = size
            return True
        return False

    def __init__(self):
        self._rectangle = None

    @property
    def is_ready(self):
        return self._rectangle is not None
