
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.imageviewer.signal import Pixbuf as Signal
from libnagatofiles.imageviewer.content.DrawingArea import NagatoDrawingArea


class NagatoContent(Gtk.ScrolledWindow, NagatoObject):

    def _yuki_n_add_to_scrolled_window(self, widget):
        self.add(widget)

    def _on_changed(self, adjustment):
        yuki_value = (adjustment.get_upper()-adjustment.get_page_size())/2
        adjustment.set_value(yuki_value)

    def _on_draw(self, scrolled_window, cairo_context):
        yuki_size, _ = scrolled_window.get_allocated_size()
        yuki_data = Signal.SCROLLED_AREA_SIZE_CHANGED, yuki_size
        self._raise("YUKI.N > pixbuf signals", yuki_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ScrolledWindow.__init__(self)
        self.set_hexpand(True)
        self.set_vexpand(True)
        yuki_v_adjustment = self.get_vadjustment()
        yuki_v_adjustment.connect("changed", self._on_changed)
        yuki_h_adjustment = self.get_hadjustment()
        yuki_h_adjustment.connect("changed", self._on_changed)
        NagatoDrawingArea(self)
        self.connect("draw", self._on_draw)
        self._raise("YUKI.N > add to box", self)
