
from gi.repository import Gtk
from gi.repository import Gdk
from libnagato4.Object import NagatoObject
from libnagatofiles.imageviewer.content.Reloader import NagatoReloader
from libnagatofiles.imageviewer.painter.Painters import AsakuraPainters
from libnagatofiles.imageviewer.signal import Pixbuf as PixbufSignal
from libnagatofiles.imageviewer.signal import Zoom as ZoomSignal


class NagatoDrawingArea(Gtk.DrawingArea, NagatoObject):

    def _inform_drawing_area(self):
        return self

    def _inform_pixbuf(self):
        return self._reloader.pixbuf

    def _on_draw(self, drawing_area, cairo_context):
        self._painters.paint(cairo_context)

    """
    def _on_scroll(self, drawing_area, event):
        if event.direction == Gdk.ScrollDirection.UP:
            yuki_data = PixbufSignal.ZOOM_RATE_CHANGED, ZoomSignal.ZOOM_UP
            self._raise("YUKI.N > pixbuf signals", yuki_data)
        if event.direction == Gdk.ScrollDirection.DOWN:
            yuki_data = PixbufSignal.ZOOM_RATE_CHANGED, ZoomSignal.ZOOM_DOWN
            self._raise("YUKI.N > pixbuf signals", yuki_data)
    """

    def __init__(self, parent):
        self._parent = parent
        self._reloader = NagatoReloader(self)
        self._painters = AsakuraPainters(self)
        Gtk.DrawingArea.__init__(self)
        # self.set_events(Gdk.EventMask.SCROLL_MASK)
        # self.connect("scroll-event", self._on_scroll)
        self.connect("draw", self._on_draw)
        self._raise("YUKI.N > add to scrolled window", self)
