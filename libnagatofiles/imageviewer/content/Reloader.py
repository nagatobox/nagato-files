
from libnagato4.Object import NagatoObject
from libnagatofiles.imageviewer.signal import Pixbuf as Signal


class NagatoReloader(NagatoObject):

    def receive_transmission(self, user_data):
        yuki_type, yuki_data = user_data
        if yuki_type != Signal.EDITED_PIXBUF_CHANGED:
            return
        self._pixbuf = yuki_data
        yuki_drawing_area = self._enquiry("YUKI.N > drawing area")
        yuki_data = self._pixbuf.get_width(), self._pixbuf.get_height()
        yuki_drawing_area.set_size_request(*yuki_data)
        yuki_drawing_area.queue_draw()

    def __init__(self, parent):
        self._parent = parent
        self._pixbuf = None
        self._raise("YUKI.N > register pixbuf object", self)

    @property
    def pixbuf(self):
        return self._pixbuf
