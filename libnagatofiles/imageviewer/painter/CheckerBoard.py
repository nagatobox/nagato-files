
from libnagato4.Object import NagatoObject
from libnagato4.Ux import Unit

SIZE = Unit(4)


class NagatoCheckerBoard(NagatoObject):

    def _get_color(self, x, y):
        if ((x+y)/SIZE) % 2 == 0:
            return (0, 0, 0, 0.2)
        else:
            return (1, 1, 1, 0.4)

    def _paint_rectangle(self, cairo_context, x, y):
        cairo_context.set_source_rgba(*self._get_color(x, y))
        cairo_context.rectangle(x, y, SIZE, SIZE)
        cairo_context.fill()

    def paint(self, cairo_context):
        yuki_drawing_area = self._enquiry("YUKI.N > drawing area")
        yuki_rectangle, _ = yuki_drawing_area.get_allocated_size()
        for yuki_x in range(0, yuki_rectangle.width, SIZE):
            for yuki_y in range(0, yuki_rectangle.height, SIZE):
                self._paint_rectangle(cairo_context, yuki_x, yuki_y)

    def __init__(self, parent):
        self._parent = parent
