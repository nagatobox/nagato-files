
from gi.repository import Gtk
from libnagato4.Ux import Unit
from libnagato4.widget.HBox import AbstractHBox
from libnagatofiles.imageviewer.bar.Label import NagatoLabel
from libnagatofiles.imageviewer.button.Navigations import AsakuraNavigations
from libnagatofiles.imageviewer.button.Zooms import AsakuraZooms
from libnagatofiles.imageviewer.button.Rotates import AsakuraRotates


class NagatoBar(AbstractHBox):

    def _on_initialize(self):
        self.set_size_request(-1, Unit(5))
        AsakuraNavigations(self)
        NagatoLabel(self)
        AsakuraZooms(self)
        self.add(Gtk.Separator())
        AsakuraRotates(self)
        self._raise("YUKI.N > css", (self, "toolbar"))
        self._raise("YUKI.N > add to box", self)
