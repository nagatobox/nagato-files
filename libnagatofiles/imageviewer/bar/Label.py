
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.imageviewer.signal import Pixbuf as Signal
from libnagatofiles.imageviewer.signal.Receiver import NagatoReceiver

SIGNAL = Signal.SOURCE_PIXBUF_CHANGED
SIZE_TEMPLATE = "width:{},  height:{}"


class NagatoLabel(Gtk.Label, NagatoObject):

    def _yuki_n_signal_received(self, pixbuf):
        yuki_width, yuki_height = pixbuf.get_width(), pixbuf.get_height()
        self._size_text = SIZE_TEMPLATE.format(yuki_width, yuki_height)
        self.set_text(self._size_text)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self)
        self.set_hexpand(True)
        self._size_text = ""
        NagatoReceiver.new_for_signal(self, SIGNAL)
        self._raise("YUKI.N > add to box", self)
