
from libnagato4.Object import NagatoObject
from libnagatofiles.imageviewer.PathsLayer import NagatoPathsLayer


class NagatoImageViewer(NagatoObject):

    @classmethod
    def new_for_path(cls, parent, path):
        yuki_image_viewer = NagatoImageViewer(parent)
        yuki_image_viewer.set_path(path)

    def _inform_page_id(self):
        return "image-viewer"

    def set_path(self, path):
        self._paths.set_path(path)

    def __init__(self, parent):
        self._parent = parent
        self._paths = NagatoPathsLayer(self)
