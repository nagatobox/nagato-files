
from gi.repository import Gio
from gi.repository import GLib


def non_recursive(directory):
    yuki_gio_file = Gio.File.new_for_path(directory)
    for yuki_file_info in yuki_gio_file.enumerate_children("*", 0):
        yuki_content_type = yuki_file_info.get_content_type()
        yuki_names = [directory, yuki_file_info.get_name()]
        yuki_fullpath = GLib.build_filenamev(yuki_names)
        if not yuki_content_type.startswith("image"):
            continue
        yield yuki_fullpath
