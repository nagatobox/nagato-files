
from gi.repository import GLib
from libnagatofiles.imageviewer.paths import DirectoryReader
from libnagatofiles.imageviewer.paths.IndexList import HaruhiIndexList


class HaruhiPaths:

    def move_path(self, shift):
        self._index_list.move_index(shift)
        return self._index_list.get_current()

    def set_initial_path(self, initial_path):
        self._index_list.clear()
        yuki_directory = GLib.path_get_dirname(initial_path)
        for yuki_fullpath in DirectoryReader.non_recursive(yuki_directory):
            self._index_list.append(yuki_fullpath)
        self._index_list.set_index_by_data(initial_path)
        return self._index_list.get_current()

    def __init__(self):
        self._index_list = HaruhiIndexList()
