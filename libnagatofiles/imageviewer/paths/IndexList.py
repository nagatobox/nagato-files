
from gi.repository import GLib


class HaruhiIndexList:

    def get_current(self):
        while self._index >= 0 and len(self._data) > 0:
            yuki_data = self._data[self._index]
            if GLib.file_test(yuki_data, GLib.FileTest.EXISTS):
                return yuki_data
            del self._data[self._index]
            self._index += 0 if len(self._data) > self._index else -1
        return None

    def move_index(self, shift):
        if len(self._data) >= 2:
            self._index = (self._index+shift)%len(self._data)

    def set_index_by_data(self, data):
        self._index = self._data.index(data) if data in self._data else -1

    def clear(self):
        self._index = -1
        self._data.clear()

    def append(self, data):
        self._data.append(data)

    def __init__(self):
        self._index = -1
        self._data = []
