
from libnagato4.Object import NagatoObject
from libnagatofiles.imageviewer.signal import Pixbuf as Signal
from libnagatofiles.imageviewer.ZoomEditor import NagatoZoomEditor
from libnagatofiles.imageviewer.editor.Rotate import NagatoRotate


class NagatoEditors(NagatoObject):

    def edit(self, pixbuf):
        yuki_pixbuf = self._zoom.edit(pixbuf)
        yuki_pixbuf = self._rotate.edit(yuki_pixbuf)
        yuki_data = Signal.EDITED_PIXBUF_CHANGED, yuki_pixbuf
        self._raise("YUKI.N > pixbuf signals", yuki_data)

    def __init__(self, parent):
        self._parent = parent
        self._zoom = NagatoZoomEditor(self)
        self._rotate = NagatoRotate(self)
