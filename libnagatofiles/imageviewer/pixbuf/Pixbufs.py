
from libnagato4.Object import NagatoObject
from libnagatofiles.imageviewer.signal import Pixbuf as Signal
from libnagatofiles.imageviewer.pixbuf.Source import NagatoSource
from libnagatofiles.imageviewer.pixbuf.Editors import NagatoEditors


class NagatoPixbufs(NagatoObject):

    def _edit(self):
        if self._ready:
            self._editors.edit(self._source.pixbuf)

    def _yuki_n_ready(self):
        self._ready = True

    def _yuki_n_request_edit(self):
        self._edit()

    def receive_transmission(self, user_data):
        yuki_type, yuki_data = user_data
        if yuki_type == Signal.SOURCE_PIXBUF_CHANGED:
            self._edit()

    def __init__(self, parent):
        self._parent = parent
        self._ready = False
        self._source = NagatoSource(self)
        self._editors = NagatoEditors(self)
        self._raise("YUKI.N > register pixbuf object", self)
