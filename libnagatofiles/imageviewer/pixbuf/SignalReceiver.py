
from gi.repository import Gio
from gi.repository import GLib
from gi.repository import GdkPixbuf
from libnagato4.Object import NagatoObject
from libnagatofiles.imageviewer.pixbuf.Animation import NagatoAnimation


class NagatoSignalReceiver(NagatoObject):

    def _inform_path(self):
        return self._path

    def receive_transmission(self, path):
        self._path = path
        yuki_gio_file = Gio.File.new_for_path(path)
        yuki_file_info = yuki_gio_file.query_info("*", 0)
        if yuki_file_info.get_content_type() == "image/gif":
            self._animation.start(path)
        else:
            yuki_pixbuf = GdkPixbuf.Pixbuf.new_from_file(path)
            self._raise("YUKI.N > refresh pixbuf", yuki_pixbuf.copy())

    def __init__(self, parent):
        self._parent = parent
        self._path = ""
        self._animation = NagatoAnimation(self)
        self._raise("YUKI.N > register path object", self)
