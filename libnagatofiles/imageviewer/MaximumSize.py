

class HaruhiMaximumSize:

    def _is_different(self, size):
        if self._max.width != size.width or self._max.height != size.height:
            return True
        return False

    def get_zoomed_size(self, pixbuf):
        yuki_width = pixbuf.get_width()
        yuki_height = pixbuf.get_height()
        yuki_h_rate = self._max.width/yuki_width
        yuki_w_rate = self._max.height/yuki_height
        yuki_rate = min(1, min(yuki_h_rate, yuki_w_rate))
        return yuki_width*yuki_rate, yuki_height*yuki_rate

    def try_set_size(self, size):
        if self._max is None or self._is_different(size):
            self._max = size
            return True
        return False

    def __init__(self):
        self._max = None

    @property
    def is_ready(self):
        return self._max is not None
