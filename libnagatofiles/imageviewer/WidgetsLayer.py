
from libnagato4.Object import NagatoObject
from libnagatofiles.imageviewer.PageBox import NagatoPageBox
from libnagatofiles.imageviewer.tab.Tab import NagatoTab


class NagatoWidgetsLayer(NagatoObject):

    def _yuki_n_close_current_tab(self):
        self._raise("YUKI.N > detach tab", self._page_box)
        self._tab.destroy()
        self._page_box.destroy()

    def __init__(self, parent):
        self._parent = parent
        self._page_box = NagatoPageBox(self)
        self._tab = NagatoTab(self)
        yuki_data = self._page_box, self._tab
        self._raise("YUKI.N > insert page at last", yuki_data)
