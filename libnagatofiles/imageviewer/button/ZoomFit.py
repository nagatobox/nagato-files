
from libnagatofiles.filer.bar.button.Button import NagatoButton
from libnagatofiles.imageviewer.signal import Pixbuf as PixbufSignal
from libnagatofiles.imageviewer.signal import Zoom as ZoomSignal


class NagatoZoomFit(NagatoButton):

    NAME = "zoom-fit-best-symbolic"
    TOOLTIP = "Fit"

    def _on_clicked(self, button):
        yuki_data = PixbufSignal.ZOOM_RATE_CHANGED, ZoomSignal.ZOOM_FIT
        self._raise("YUKI.N > pixbuf signals", yuki_data)
