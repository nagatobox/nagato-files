
from libnagatofiles.imageviewer.button.ZoomFit import NagatoZoomFit
from libnagatofiles.imageviewer.button.ZoomIn import NagatoZoomIn
from libnagatofiles.imageviewer.button.ZoomOut import NagatoZoomOut
from libnagatofiles.imageviewer.button.ZoomOriginal import NagatoZoomOriginal


class AsakuraZooms:

    def __init__(self, parent):
        NagatoZoomFit(parent)
        NagatoZoomIn(parent)
        NagatoZoomOut(parent)
        NagatoZoomOriginal(parent)
