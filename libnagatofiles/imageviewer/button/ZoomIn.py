
from libnagatofiles.filer.bar.button.Button import NagatoButton
from libnagatofiles.imageviewer.signal import Pixbuf as PixbufSignal
from libnagatofiles.imageviewer.signal import Zoom as ZoomSignal


class NagatoZoomIn(NagatoButton):

    NAME = "zoom-in-symbolic"
    TOOLTIP = "Zoom In"

    def _on_clicked(self, button):
        yuki_data = PixbufSignal.ZOOM_RATE_CHANGED, ZoomSignal.ZOOM_IN
        self._raise("YUKI.N > pixbuf signals", yuki_data)
