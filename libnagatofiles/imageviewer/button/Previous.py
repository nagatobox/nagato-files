
from libnagatofiles.filer.bar.button.Button import NagatoButton


class NagatoPrevious(NagatoButton):

    NAME = "go-previous-symbolic"
    TOOLTIP = "Previous"

    def _on_clicked(self, button):
        self._raise("YUKI.N > move path", -1)
