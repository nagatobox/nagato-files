
from libnagatofiles.imageviewer.button.RotateLeft import NagatoRotateLeft
from libnagatofiles.imageviewer.button.RotateRight import NagatoRotateRight


class AsakuraRotates:

    def __init__(self, parent):
        NagatoRotateLeft(parent)
        NagatoRotateRight(parent)
