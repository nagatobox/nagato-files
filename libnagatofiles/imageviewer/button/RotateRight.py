
from gi.repository import GdkPixbuf
from libnagatofiles.filer.bar.button.Button import NagatoButton
from libnagatofiles.imageviewer.signal import Pixbuf as Signal

ANGLE = GdkPixbuf.PixbufRotation.CLOCKWISE


class NagatoRotateRight(NagatoButton):

    NAME = "object-rotate-right-symbolic"
    TOOLTIP = "Rotate Right"

    def _on_clicked(self, button):
        yuki_data = Signal.ROTATE_ANGLE_CHANGED, ANGLE
        self._raise("YUKI.N > pixbuf signals", yuki_data)
