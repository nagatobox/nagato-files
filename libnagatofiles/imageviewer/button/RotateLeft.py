
from gi.repository import GdkPixbuf
from libnagatofiles.filer.bar.button.Button import NagatoButton
from libnagatofiles.imageviewer.signal import Pixbuf as Signal

ANGLE = GdkPixbuf.PixbufRotation.COUNTERCLOCKWISE


class NagatoRotateLeft(NagatoButton):

    NAME = "object-rotate-left-symbolic"
    TOOLTIP = "Rotate Left"

    def _on_clicked(self, button):
        yuki_data = Signal.ROTATE_ANGLE_CHANGED, ANGLE
        self._raise("YUKI.N > pixbuf signals", yuki_data)
