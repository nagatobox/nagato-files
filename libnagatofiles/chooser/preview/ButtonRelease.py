
from libnagato4.Object import NagatoObject
from libnagatofiles.chooser.preview import Columns


class NagatoButtonRelease(NagatoObject):

    def _on_clicked(self, tree_path):
        self._raise("YUKI.N > tree path selected", tree_path)

    def _on_button_release(self, widget, event):
        yuki_tree_path = widget.get_path_at_pos(event.x, event.y)
        if event.button in (1, 3):
            self._on_clicked(yuki_tree_path)

    def __init__(self, parent):
        self._parent = parent
        yuki_widget = self._enquiry("YUKI.N > tree like widget")
        yuki_widget.connect("button-release-event", self._on_button_release)
