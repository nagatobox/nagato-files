
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagatothumbnail.video.VideoTrack import HaruhiVideoTrack
from libnagatofiles.chooser.preview.FFMpeg import NagatoFFMpeg
from libnagatofiles.chooser.preview.OutPaths import HaruhiOutPaths


class NagatoLoader(NagatoObject):

    def _timeout(self, out_paths, in_path, size):
        yuki_position, yuki_out = out_paths.pop()
        if yuki_position is None:
            return False
        self._ffmpeg.call_sync(in_path, yuki_out, yuki_position, size)
        return True

    def _initialize_tiles(self):
        yuki_in_path = self._enquiry("YUKI.N > prime path")
        haruhi_video_track = HaruhiVideoTrack(yuki_in_path)
        if not haruhi_video_track.is_valid:
            return
        haruhi_out_paths = HaruhiOutPaths()
        yuki_size = haruhi_video_track.thumbnail_size
        yuki_args = haruhi_out_paths, yuki_in_path, yuki_size
        GLib.timeout_add(100, self._timeout, *yuki_args)

    def __init__(self, parent):
        self._parent = parent
        self._ffmpeg = NagatoFFMpeg(self)
        self._initialize_tiles()
