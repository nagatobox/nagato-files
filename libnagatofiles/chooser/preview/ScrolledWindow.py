
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.chooser.preview.IconView import NagatoIconView


class NagatoScrolledWindow(Gtk.ScrolledWindow, NagatoObject):

    def _yuki_n_add_to_scrolled_window(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ScrolledWindow.__init__(self)
        self.set_size_request(448, 448)
        self._raise("YUKI.N > add to content area", self)
        NagatoIconView(self)
        self._raise("YUKI.N > css", (self, "chooser-iconview"))
