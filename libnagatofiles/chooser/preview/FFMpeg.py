
from gi.repository import Gio
from libnagato4.Object import NagatoObject

FLAG = Gio.SubprocessFlags.NONE


class NagatoFFMpeg(NagatoObject):

    def _on_finished(self, subprocess, task, out_path):
        yuki_success, _, _ = subprocess.communicate_utf8_finish(task)
        if yuki_success:
            self._raise("YUKI.N > thumbnail created", out_path)
        else:
            print("failed")

    def call_sync(self, in_path, out_path, position, size):
        yuki_args = [
            "ffmpegthumbnailer",
            "-i", in_path,
            "-o", out_path,
            "-t", "{}%".format(position/100),
            "-s", str(size)
            ]
        yuki_subprocess = Gio.Subprocess.new(yuki_args, FLAG)
        yuki_data = (None, None, self._on_finished, out_path)
        yuki_subprocess.communicate_utf8_async(*yuki_data)

    def __init__(self, parent):
        self._parent = parent
