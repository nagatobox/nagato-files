
from gi.repository import GdkPixbuf


TYPES = bool, str, GdkPixbuf.Pixbuf, GdkPixbuf.Pixbuf, GdkPixbuf.Pixbuf
SELECTED = 0
NAME = 1
PIXBUF = 2
UNSELECTED_PIXBUF = 3
SELECTED_PIXBUF = 4
