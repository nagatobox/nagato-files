
from gi.repository import Gtk
from libnagato4.Ux import Unit
from libnagato4.widget import Margin
from libnagato4.Object import NagatoObject
from libnagato4.chooser.selectfile.Dialog import NagatoDialog
from libnagatofiles.chooser.mp4tag.image.Initializer import NagatoInitializer
from libnagatofiles.chooser.mp4tag.image.OpenImage import NagatoContext


class NagatoImage(Gtk.Button, NagatoObject):

    def _yuki_n_image_pixbuf(self, pixbuf):
        self._image.set_from_pixbuf(pixbuf)

    def _on_clicked(self, button):
        yuki_context = NagatoContext(self)
        yuki_response = NagatoDialog.run_with_context(self, yuki_context)
        if yuki_response == Gtk.ResponseType.APPLY:
            yuki_data = "coverart-path", yuki_context["selection"][0]
            self._raise("YUKI.N > selection changed", yuki_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self)
        self.props.tooltip_text = "Select Coverart Image"
        self._image = Gtk.Image()
        self._image.set_size_request(Unit(16), Unit(16))
        self.set_image(self._image)
        Margin.set_with_unit(self, 2, 2, 2, 2)
        self.connect("clicked", self._on_clicked)
        NagatoInitializer(self)
        self._raise("YUKI.N > add to box", self)
