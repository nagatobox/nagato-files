
from libnagatofiles.chooser.mp4tag.TagBox import AbstractTagBox


class NagatoTitleBox(AbstractTagBox):

    TITLE = "Title : "
    CSS = "config-box-odd"
    KEY = "name"
