
from gi.repository import GLib
from libnagato4.Object import NagatoObject

VOID_PROPS = {"coverart-path": "", "name": "", "artist": "", "album": ""}


class NagatoContextProps(NagatoObject):

    def set_path(self, path):
        self._raise("YUKI.N > tag", ("source-path", path))
        yuki_basename = GLib.path_get_basename(path)
        yuki_data = "title", "Edit Tag \n{}".format(yuki_basename)
        self._raise("YUKI.N > tag", yuki_data)

    def __init__(self, parent):
        self._parent = parent
        for yuki_key, yuki_value in VOID_PROPS.items():
            self._raise("YUKI.N > tag", (yuki_key, yuki_value))
