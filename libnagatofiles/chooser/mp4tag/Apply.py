
import mutagen
from mutagen.mp4 import MP4Cover
from libnagatofiles.chooser.mp4tag.image import Pixbuf

KEYS = {"\xa9nam": "name", "\xa9ART": "artist", "\xa9alb": "album"}


def _try_set_coverart(mutagen_file, path):
    if path == "":
        return
    yuki_pixbuf = Pixbuf.new_for_size(path, 300)
    yuki_success, yuki_bytes = yuki_pixbuf.save_to_bufferv("png", [], [])
    if yuki_success:
        yuki_cover = MP4Cover(yuki_bytes, MP4Cover.FORMAT_PNG)
        mutagen_file["covr"] = [yuki_cover]

def apply(props):
    yuki_source_path = props["source-path"]
    yuki_file = mutagen.File(yuki_source_path)
    for yuki_key, yuki_value in KEYS.items():
        if props[yuki_value] != "":
            yuki_file[yuki_key] = props[yuki_value]
    _try_set_coverart(yuki_file, props["coverart-path"])
    yuki_file.save()
