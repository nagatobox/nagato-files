
import mutagen
from libnagato4.Object import NagatoObject

KEYS = {"\xa9nam": "name", "\xa9ART": "artist", "\xa9alb": "album"}


class NagatoMetadata(NagatoObject):

    @classmethod
    def new_for_path(cls, parent, path):
        yuki_metadata = cls(parent)
        yuki_metadata.initialize_for_path(path)
        return yuki_metadata

    def select(self, user_data):
        yuki_tag, yuki_data = user_data
        if yuki_tag in KEYS.values() or yuki_tag == "coverart-path":
            self._raise("YUKI.N > tag", user_data)

    def initialize_for_path(self, path):
        yuki_file = mutagen.File(path)
        for yuki_tag in yuki_file:
            if yuki_tag in KEYS:
                yuki_data = KEYS[yuki_tag], yuki_file[yuki_tag][0]
                self._raise("YUKI.N > tag", yuki_data)

    def __init__(self, parent):
        self._parent = parent
