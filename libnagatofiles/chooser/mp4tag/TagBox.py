
from libnagatofiles.chooser.mp4tag.HBox import AbstractHBox
from libnagatofiles.chooser.mp4tag.TagEntry import NagatoTagEntry


class AbstractTagBox(AbstractHBox):

    TITLE = "define title here"
    CSS = "define css here"
    KEY = "define key here"

    def _inform_key(self):
        return self.KEY

    def _on_initialize(self):
        NagatoTagEntry(self)
