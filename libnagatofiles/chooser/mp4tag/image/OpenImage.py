
from gi.repository import Gtk
from libnagato4.file import XdgUserDir
from libnagato4.chooser.context.MimeFilter import HaruhiMimeFilter
from libnagato4.chooser.context.OpenFile import NagatoContext as TFEI

MIME_TYPES = {'png': 'image/png', 'jpeg': 'image/jpeg'}


class NagatoContext(TFEI):

    def _initialize_properties(self):
        self._mime_filter = HaruhiMimeFilter(MIME_TYPES)
        self._props["mime-types"] = MIME_TYPES
        self._props["directory"] = XdgUserDir.PICTURES
