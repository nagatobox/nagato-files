
from gi.repository import GdkPixbuf
from libnagatofiles.chooser.mp4tag.image.Surface import HaruhiSurface


def new_for_size(path, size):
    yuki_pixbuf = GdkPixbuf.Pixbuf.new_from_file(path)
    yuki_width = yuki_pixbuf.get_width()
    yuki_height = yuki_pixbuf.get_height()
    yuki_rate = max(size/yuki_width, size/yuki_height)
    yuki_pixbuf = yuki_pixbuf.scale_simple(
        yuki_width*yuki_rate,
        yuki_height*yuki_rate,
        GdkPixbuf.InterpType.HYPER
        )
    haruhi_surface = HaruhiSurface.new_for_size(size)
    haruhi_surface.paint_pixbuf(yuki_pixbuf)
    return haruhi_surface.get_pixbuf()
