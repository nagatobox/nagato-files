
from gi.repository import Gio
from libnagato4.Object import NagatoObject
from libnagatothumbnail.mp4.Preview import HaruhiPreview


class NagatoInitializer(NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        yuki_preview = HaruhiPreview()
        yuki_path = self._enquiry("YUKI.N > chooser config", "source-path")
        yuki_gio_file = Gio.File.new_for_path(yuki_path)
        yuki_file_info = yuki_gio_file.query_info("*", 0)
        yuki_pixbuf = yuki_preview.load(yuki_path, yuki_file_info)
        self._raise("YUKI.N > image pixbuf", yuki_pixbuf)
