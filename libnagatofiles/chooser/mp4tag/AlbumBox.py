
from libnagatofiles.chooser.mp4tag.TagBox import AbstractTagBox


class NagatoAlbumBox(AbstractTagBox):

    TITLE = "Album : "
    CSS = "config-box-odd"
    KEY = "album"
