
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagatopath import XdgUserDirs


class NagatoUserSpecialDirs(NagatoObject):

    def _append_home_dir(self):
        yuki_data = GLib.get_home_dir(), "user-home-symbolic"
        self._raise("YUKI.N > append path", yuki_data)

    def read(self):
        self._append_home_dir()
        for yuki_index, yuki_icon_name in XdgUserDirs.ICON_NAMES.items():
            yuki_path = GLib.get_user_special_dir(yuki_index)
            yuki_data = yuki_path, yuki_icon_name+"-symbolic"
            self._raise("YUKI.N > append path", yuki_data)

    def __init__(self, parent):
        self._parent = parent
