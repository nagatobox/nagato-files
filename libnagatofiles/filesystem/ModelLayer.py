
from libnagato4.Object import NagatoObject
from libnagatofiles.filesystem.UniqueIdLayer import NagatoUniqueIdLayer
from libnagatofiles.filesystem.ListStore import NagatoListStore


class NagatoModelLayer(NagatoObject):

    def _inform_bookmark_model(self):
        return self._list_store

    def _yuki_n_bookmark_changed(self):
        self._list_store.refresh()

    def __init__(self, parent):
        self._parent = parent
        self._list_store = NagatoListStore(self)
        NagatoUniqueIdLayer(self)
