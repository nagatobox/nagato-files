
from libnagato4.Object import NagatoObject
from libnagatofiles.filesystem.ModelLayer import NagatoModelLayer


class NagatoPortalLayer(NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        NagatoModelLayer(self)
