
from gi.repository import Gio
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagatofiles.trash import Directories


class NagatoMonitor(NagatoObject):

    def _timeout(self):
        yuki_count = 0
        for yuki_file_info in self._gio_file.enumerate_children("*", 0, None):
            yuki_count += 1
        if yuki_count != self._count:
            self._raise("YUKI.N > trash bin count changed", yuki_count)
        self._count = yuki_count
        return GLib.SOURCE_CONTINUE

    def __init__(self, parent):
        self._parent = parent
        self._gio_file = Gio.File.new_for_path(Directories.INFO_DIR)
        self._count = 0
        GLib.timeout_add_seconds(1, self._timeout)
