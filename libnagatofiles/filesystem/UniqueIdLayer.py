
from libnagato4.Object import NagatoObject
from libnagatofiles.ui.Paned import NagatoPaned


class NagatoUniqueIdLayer(NagatoObject):

    # start from 42
    _id = 42

    def _inform_unique_id(self):
        self._id += 1
        return self._id

    def __init__(self, parent):
        self._parent = parent
        NagatoPaned(self)
