
from libnagato4.Object import NagatoObject
from libnagatofiles.bookmark.popover.Bookmark import NagatoBookmark
from libnagatofiles.bookmark.popover.Trash import NagatoTrash


class NagatoRight(NagatoObject):

    def queue(self, path, event):
        if path == "trash":
            self._trash.popup_for_position(event.x, event.y)
        else:
            self._bookmark.popup_for_position(path, event.x, event.y)

    def __init__(self, parent):
        self._parent = parent
        self._bookmark = NagatoBookmark(self)
        self._trash = NagatoTrash(self)
