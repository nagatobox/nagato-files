
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.bookmark.column.Name import NagatoName
from libnagatofiles.bookmark.column.Icon import NagatoIcon
from libnagatofiles.bookmark.click.Click import NagatoClick
from libnagatofiles.bookmark.drag.Event import NagatoEvent


class NagatoTreeView(Gtk.TreeView, NagatoObject):

    def _inform_tree_like_widget(self):
        return self

    def _yuki_n_insert_column(self, column):
        self.insert_column(column, -1)

    def __init__(self, parent):
        self._parent = parent
        yuki_model = self._enquiry("YUKI.N > bookmark model")
        Gtk.TreeView.__init__(self, model=yuki_model)
        self._raise("YUKI.N > css", (self, "bookmark-treeview"))
        self.set_hexpand(True)
        self.set_headers_visible(False)
        self.set_has_tooltip(True)
        self.set_tooltip_column(4)
        self.set_can_focus(False)
        yuki_selection = self.get_selection()
        yuki_selection.mode = Gtk.SelectionMode.NONE
        NagatoIcon(self)
        NagatoName(self)
        NagatoClick(self)
        NagatoEvent(self)
