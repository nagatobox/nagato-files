
import os
from gi.repository import Gio
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagatofiles.util.AddBookmark import NagatoAddBookmark
from libnagatofiles.filer.drag.FileManuever import NagatoFileManeuver


class NagatoSafeQueue(NagatoObject):

    def _get_directory_from_path_index(self, path):
        yuki_model = self._enquiry("YUKI.N > bookmark model")
        yuki_row = yuki_model[path]
        return yuki_row[3]

    def _check_is_executable(self, path=None):
        if path is None:
            return False
        return GLib.file_test(path, GLib.FileTest.IS_EXECUTABLE)

    def _get_target_directory(self):
        yuki_path = self._enquiry("YUKI.N > path at pointer")
        if yuki_path is None:
            return None
        return self._get_directory_from_path_index(yuki_path)

    def _is_movable(self, uris):
        for yuki_uri in uris:
            yuki_file = Gio.File.new_for_uri(yuki_uri)
            yuki_path = yuki_file.get_path()
            if not os.access(yuki_path, os.W_OK):
                return False
        return True

    def _move(self, drag_data, directory):
        self._file_manuever.move_to(directory, drag_data.get_uris())

    def queue(self, drag_data):
        yuki_directory = self._get_target_directory()
        yuki_uris = drag_data.get_uris()
        if yuki_directory is None:
            self._add_bookmark.add_uris(yuki_uris)
        elif self._is_movable(yuki_uris):
            self._move(drag_data, yuki_directory)

    def __init__(self, parent):
        self._parent = parent
        self._add_bookmark = NagatoAddBookmark(self)
        self._file_manuever = NagatoFileManeuver(self)
