
from libnagato4.Object import NagatoObject


class NagatoDragMotion(NagatoObject):

    def _on_drag_motion(self, tree_view, drag_context, x, y, time):
        tree_view.drag_highlight()

    def _on_drag_leave(self, tree_view, context, time):
        self._raise("YUKI.N > unselect all")

    def __init__(self, parent):
        self._parent = parent
        yuki_widget = self._enquiry("YUKI.N > tree like widget")
        yuki_widget.connect("drag-motion", self._on_drag_motion)
        # yuki_widget.connect("drag-leave", self._on_drag_leave)
