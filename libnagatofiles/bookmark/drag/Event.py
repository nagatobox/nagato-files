
from gi.repository import Gdk
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
# from libnagatofiles.filer.drag.SafeQueue import NagatoSafeQueue

ACTION = Gdk.DragAction.MOVE | Gdk.DragAction.COPY
FLAGS = Gtk.DestDefaults.MOTION | Gtk.DestDefaults.DROP


class NagatoEvent(NagatoObject):

    def _get_target_entries(self):
        yuki_id = self._enquiry("YUKI.N > unique id")
        yuki_target_entry = Gtk.TargetEntry.new("text/uri-list", 0, yuki_id)
        return [yuki_target_entry]

    def _on_drag_data_received(self, widget, context, x, y, data, info, time):
        yuki_response = widget.get_path_at_pos(x, y)
        if yuki_response is not None:
            yuki_tree_path, _, _, _ = yuki_response
            yuki_model = widget.get_model()
            print("bookmark", yuki_model[yuki_tree_path][3], data.get_uris())

    def _on_drag_motion(self, tree_view, drag_context, x, y, time):
        yuki_response = tree_view.get_path_at_pos(x, y)
        if yuki_response is not None:
            yuki_tree_path, _, _, _ = yuki_response
            tree_view.set_cursor(yuki_tree_path, None, False)

    def __init__(self, parent):
        self._parent = parent
        yuki_widget = self._enquiry("YUKI.N > tree like widget")
        yuki_widget.drag_dest_set(FLAGS, self._get_target_entries(), ACTION)
        yuki_widget.connect('drag-data-received', self._on_drag_data_received)
        yuki_widget.connect('drag-motion', self._on_drag_motion)
