
from libnagato4.Object import NagatoObject
from libnagatofiles.bookmark.Destination import NagatoDestination
from libnagatofiles.bookmark.DragMotion import NagatoDragMotion


class NagatoDropLayer(NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        NagatoDestination(self)
        NagatoDragMotion(self)
