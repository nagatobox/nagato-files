
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.bookmark.layer.DropLayer import NagatoDropLayer


class NagatoTargetLayer(NagatoObject):

    def _inform_target_entries(self):
        return [self._target_entry]

    def __init__(self, parent):
        self._parent = parent
        yuki_id = self._enquiry("YUKI.N > unique id")
        self._target_entry = Gtk.TargetEntry.new("text/uri-list", 0, yuki_id)
        NagatoDropLayer(self)
