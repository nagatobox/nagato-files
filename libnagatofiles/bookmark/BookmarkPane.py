
from libnagatofiles.bookmark.TreeView import NagatoTreeView
from libnagatofiles.filer.scrolledwindow.ScrolledWindow import (
    NagatoScrolledWindow as TFEI
    )


class NagatoBookmarkPane(TFEI):

    NAME = "bookmark-pane"

    def _on_size_allocate(self, *args):
        self._raise("YUKI.N > size allocate")

    def _on_initialize(self):
        self.add(NagatoTreeView(self))
        self.connect("size-allocate", self._on_size_allocate)
