
from gi.repository import Gdk
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatofiles.bookmark.SafeQueue import NagatoSafeQueue

ACTION = Gdk.DragAction.MOVE | Gdk.DragAction.COPY
FLAGS = Gtk.DestDefaults.MOTION | Gtk.DestDefaults.DROP


class NagatoDestination(NagatoObject):

    def _yuki_n_drop_action_copy(self, user_data):
        self._file_manuever.copy_to(*user_data)

    def _yuki_n_drop_action_move(self, user_data):
        self._file_manuever.move_to(*user_data)

    def _yuki_n_drag_accepted(self, user_data):
        print("accepted", user_data)

    def _on_drag_data_received(self, widget, context, x, y, data, info, time):
        self._queue_sanitizer.queue(data)

    def _initialize_signals(self):
        yuki_widget = self._enquiry("YUKI.N > tree like widget")
        yuki_widget.connect('drag-data-received', self._on_drag_data_received)
        yuki_target_entries = self._enquiry("YUKI.N > target entries")
        yuki_widget.drag_dest_set(FLAGS, yuki_target_entries, ACTION)

    def __init__(self, parent):
        self._parent = parent
        self._initialize_signals()
        self._queue_sanitizer = NagatoSafeQueue(self)
