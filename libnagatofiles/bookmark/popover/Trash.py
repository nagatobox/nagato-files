
from gi.repository import Gdk
from libnagato4.popover.SingleStack import AbstractSingleStack
from libnagatofiles.bookmark.popover.OpenTrash import NagatoOpenTrash


class NagatoTrash(AbstractSingleStack):

    def popup_for_position(self, x, y):
        yuki_rectangle = Gdk.Rectangle()
        yuki_rectangle.x = x
        yuki_rectangle.y = y
        self.set_pointing_to(yuki_rectangle)
        self.popup()
        self.show_all()

    def _add_to_single_stack(self):
        self.set_relative_to(self._enquiry("YUKI.N > tree like widget"))
        NagatoOpenTrash(self)
