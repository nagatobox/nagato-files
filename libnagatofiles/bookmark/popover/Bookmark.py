
from gi.repository import Gdk
from libnagato4.popover.SingleStack import AbstractSingleStack
from libnagatofiles.bookmark.popover.Open import NagatoOpen
from libnagatofiles.bookmark.popover.OpenInNewTab import NagatoOpenInNewTab
from libnagatofiles.bookmark.popover.Remove import NagatoRemove


class NagatoBookmark(AbstractSingleStack):

    def _inform_path(self):
        return self._path

    def popup_for_position(self, path, x, y):
        self._path = path
        yuki_rectangle = Gdk.Rectangle()
        yuki_rectangle.x = x
        yuki_rectangle.y = y
        self.set_pointing_to(yuki_rectangle)
        self.popup()
        self.show_all()

    def _add_to_single_stack(self):
        self._path = None
        self.set_relative_to(self._enquiry("YUKI.N > tree like widget"))
        NagatoOpen(self)
        NagatoOpenInNewTab(self)
        NagatoRemove(self)
