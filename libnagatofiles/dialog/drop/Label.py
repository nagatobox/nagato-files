
from gi.repository import GLib
from libnagato4.file import HomeDirectory
from libnagato4.dialog.Label import AbstractLabel

MARKUP = \
    "<span size='large'><u>WARNING !!</u></span>\n"\
    "\n"\
    "We received following drag data on\n"\
    "{}\n"\
    "What do you want to do ?\n"\
    "\n"


class NagatoLabel(AbstractLabel):

    CSS = "dialog-warning"

    def _set_uris(self, format_):
        for yuki_uri in self._enquiry("YUKI.N > uris"):
            yuki_file_name, _ = GLib.filename_from_uri(yuki_uri)
            yuki_basename = GLib.filename_display_basename(yuki_file_name)
            format_ += "{}\n".format(yuki_basename)
        return format_

    def _get_markup(self):
        yuki_directory = self._enquiry("YUKI.N > directory")
        yuki_format = MARKUP.format(HomeDirectory.shorten(yuki_directory))
        return self._set_uris(yuki_format)
