
from gi.repository import Gio
from libnagato4.file import HomeDirectory
from libnagatofiles.Mikuru import DialogArgs


class HaruhiMessage:

    def _get_list(self, uris):
        yuki_list = []
        for yuki_uri in uris:
            yuki_file = Gio.File.new_for_uri(yuki_uri)
            yuki_list.append(yuki_file.get_basename())
            if len(yuki_list) > 20:
                break
        return yuki_list

    def get_message(self, target_directory, uris):
        yuki_directory = HomeDirectory.shorten(target_directory)
        yuki_list = self._get_list(uris)
        yuki_message = DialogArgs.MESSAGE_DROP+"\n".join(yuki_list)
        yuki_message = yuki_message.format(yuki_directory)
        yuki_more = len(uris)-len(yuki_list)
        if yuki_more:
            yuki_message += "\nand {} more packages".format(yuki_more)
        return yuki_message
