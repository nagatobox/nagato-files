
from gi.repository import Gtk
from gi.repository import GLib
from libnagato4.Object import NagatoObject

MESSAGE = \
    "<span size='large'><u>CONFIRM DELETION</u></span>\n"\
    "\n"\
    "YUKI.N > Delete following file(s) ?\n"\
    "\n"


class NagatoLabel(Gtk.Label, NagatoObject):

    def _get_message(self):
        yuki_message = MESSAGE
        for yuki_path in self._enquiry("YUKI.N > selection"):
            yuki_name = GLib.path_get_basename(yuki_path)
            yuki_message += "{}\n".format(yuki_name)
        return yuki_message

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self)
        self.set_justify(Gtk.Justification.CENTER)
        self.set_line_wrap(True)
        self.set_markup(self._get_message())
        self._raise("YUKI.N > add to scrolled window", self)
