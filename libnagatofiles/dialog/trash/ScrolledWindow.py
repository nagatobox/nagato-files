
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.widget import Margin
from libnagatofiles.dialog.trash.Label import NagatoLabel


class NagatoScrolledWindow(Gtk.ScrolledWindow, NagatoObject):

    def _yuki_n_add_to_scrolled_window(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ScrolledWindow.__init__(self)
        self.set_vexpand(True)
        self.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        Margin.set_with_unit(self, 1, 1, 1, 1)
        NagatoLabel(self)
        self._raise("YUKI.N > add to dialog", self)
