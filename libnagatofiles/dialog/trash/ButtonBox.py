
from libnagato4.dialog.ButtonBox import AbstractButtonBox
from libnagato4.dialog.warning.CancelButton import NagatoCancelButton
from libnagatofiles.dialog.trash.DeleteButton import NagatoDeleteButton


class NagatoButtonBox(AbstractButtonBox):

    def _on_initialize(self):
        NagatoCancelButton(self)
        NagatoDeleteButton(self)
