
from gi.repository import Gio
from gi.repository import GLib


def get_access_new(file_info):
    if not file_info.get_attribute_boolean("access::can-read"):
        return "Unreadable"
    if not file_info.get_attribute_boolean("access::can-write"):
        return "Read Only"
    if not file_info.get_attribute_boolean("access::can-execute"):
        return "Read-Write"
    return "Executable"


def get_age(time):
    yuki_last = GLib.DateTime.new_from_unix_local(time)
    yuki_now = GLib.DateTime.new_now_local()
    yuki_diff = yuki_now.difference(yuki_last)
    if yuki_diff/(24*60*60*1000000) > 1:
        return yuki_last.format("%F")
    else:
        return yuki_last.format("%T")


def get_path_for_sort(fullpath, is_dir):
    yuki_header = "__dir__" if is_dir else "__file__"
    return yuki_header+fullpath.lower()


def get_type(path):
    yuki_gio_file = Gio.File.new_for_path(path)
    yuki_file_info = yuki_gio_file.query_info("standard::content-type", 0)
    return yuki_file_info.get_content_type()


def get_tag(file_info):
    if not file_info.get_attribute_boolean("access::can-read"):
        return "Unreadable"
    if not file_info.get_attribute_boolean("access::can-write"):
        return "Read Only"
    return None
