
from gi.repository import Gio
from gi.repository import GdkPixbuf

FULLPATH = 0
FILE_NAME = 1
IS_DIR = 2              # DEPRECATED. Must be None.
PATH_FOR_SORT = 3
SELECTED = 4

MIME_TYPE = 5           # used by TreeView
READBLE_PERMISSION = 6  # used by TreeView
AGE = 7                 # used by TreeView
AGE_UNIX_TIME = 8       # used by TreeView for sorting.
ICON_IMAGE = 9

TAG = 10                # DEPRECATED. Must be None.
FILE_INFO = 11
TILE_TYPE = 12          # DEPRECATED. Must be None.
UNSELECTED_TILE = 13
SELECTED_TILE = 14

TYPES = (
    str, str, bool, str, bool,
    str, str, str, int, GdkPixbuf.Pixbuf,
    str, Gio.FileInfo, str, GdkPixbuf.Pixbuf, GdkPixbuf.Pixbuf
    )
