
MESSAGE_DELETE = \
    "<span size='large'><u>WARNING !!</u></span>\n"\
    "\n"\
    "Do you really want to delete following files ?\n"\
    "\n"

BUTTONS_DELETE = ["Cancel", "Delete"]

MESSAGE_DROP = \
    "<span size='large'><u>WARNING !!</u></span>\n"\
    "\n"\
    "We received following drag data on\n"\
    "{}\n"\
    "What do you want to do ?\n"\
    "\n"
