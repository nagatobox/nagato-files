
from natsort import humansorted
from gi.repository import Gio
from libnagato4.Object import NagatoObject


class NagatoAppInfoCache(NagatoObject):

    def get_humansorted(self):
        yuki_path = self._enquiry("YUKI.N > prime path")
        yuki_gio_file = Gio.File.new_for_path(yuki_path)
        yuki_file_info = yuki_gio_file.query_info("*", 0)
        yuki_type = yuki_file_info.get_content_type()
        yuki_cache = {}
        for yuki_app_info in Gio.AppInfo.get_all_for_type(yuki_type):
            yuki_cache[yuki_app_info.get_name()] = yuki_app_info
        return humansorted(yuki_cache.items())

    def __init__(self, parent):
        self._parent = parent
