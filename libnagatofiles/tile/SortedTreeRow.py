
from libnagato4.Object import NagatoObject
from libnagatofiles.Mikuru import Columns


class NagatoSortedTreeRow(NagatoObject):

    def _get_filtered(self, filtered):
        yuki_rows = []
        for yuki_row in self._enquiry("YUKI.N > enumerate rows"):
            if yuki_row[Columns.FILE_NAME].startswith(".") == filtered:
                yuki_rows.append(yuki_row)
        return yuki_rows

    def _get_not_show_hidden(self):
        yuki_rows = self._get_filtered(False)
        yuki_rows += self._get_filtered(True)
        return yuki_rows

    def _get_all(self):
        yuki_rows = []
        for yuki_row in self._enquiry("YUKI.N > enumerate rows"):
            yuki_rows.append(yuki_row)
        return yuki_rows

    def get_sorted(self):
        if self._enquiry("YUKI.N > show hidden"):
            return self._get_all()
        else:
            return self._get_not_show_hidden()

    def __init__(self, parent):
        self._parent = parent
