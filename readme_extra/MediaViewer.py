digraph {

    MediaViewer [
        shape=star
        colorscheme=set312
        fillcolor=1
        fontcolor=White
        style=filled
    ]
    
    subgraph cluster_2 {
        PathsLayer [
            shape=doublecircle
            colorscheme=set312
            fillcolor=3
            fontcolor=White
            style=filled
        ]
        PlayerLayer [
            shape=doublecircle
            colorscheme=set312
            fillcolor=4
            fontcolor=White
            style=filled
        ]
        WidgetsLayer [
            shape=doublecircle
            colorscheme=set312
            fillcolor=5
            fontcolor=White
            style=filled
        ]
        label="mainline"
        style=filled
        colorscheme=set312
        fillcolor=2
        color=White
        node [style=filled, fillcolor=White]
        PathsLayer -> PlayerLayer
        PlayerLayer -> WidgetsLayer
     }

    MediaViewer -> PathsLayer

    subgraph cluster_3 {
        label="paths"
        style=filled
        colorscheme=set312
        fillcolor=3
        color=White
        node [style=filled, fillcolor=White]
        Model -> ListStore
        Model -> CurrentIter
    }

    PathsLayer -> Model

    subgraph cluster_4 {
        label="player"
        style=filled
        colorscheme=set312
        fillcolor=4
        color=White
        node [style=filled, fillcolor=White]
        PlayerLayer -> Player
        Player -> "Event(Player)"
        Player -> Receivers
        Receivers -> PathsReceiver
        Receivers -> PlaybackReceiver
        Receivers -> SettingsReceiver
    }

    subgraph cluster_5 {
        "Label(Tab)" [
            shape=rpromoter
            colorscheme=set312
            fillcolor=3
            fontcolor=White
            style=filled
        ]
        label="tab"
        style=filled
        colorscheme=set312
        fillcolor=5
        color=White
        node [style=filled, fillcolor=White]
        Tab -> TabPopover
        Tab -> "Label(Tab)"
    }

    WidgetsLayer -> Tab

    PageBox [
        shape=doublecircle
        colorscheme=set312
        fillcolor=1
        fontcolor=White
        style=filled
    ]

    WidgetsLayer -> PageBox
    PageBox -> DrawingArea
    PageBox -> ActionBar

    ActionBar -> "Play(Button)"
    ActionBar -> "Pause(Button)"
    ActionBar -> "Bar(seek)"
    ActionBar -> "Button(volume)"

}