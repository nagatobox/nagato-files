# PageLayers

### 1: PortalLayer

+ store unique_id for page
+ transmissions for selection/directory/filter changes message

### 2: CurrentDirectoryLayer

+ store current drectory path.

### 3: ModelLayer

+ handles basemodel(child model in Gtk+3 term).
+ append/remove/clear tree_rows at this layer. not FilterLayer

### 4: FilterModelLayer

+ path activation and refilter messages should catch here.

### 5: FilterLayer

+ store keywords and show_hidden
+ filter_func handles here.
+ filter_func message comes from PortalLayer, not sub layers.

### 6: TileLayer

+ Handles TileLoader.

### 7: MonitorLayer

+ Monitor current direcotry at this layer.

### 8: SelectionLayer

+ store selection items
+ monitor selection

### 9: PrimePathLayer

+ store most recent path and tree_row

### 10: SortingLayer

+ handles sort order.

### 11: UtilityLayer (TOO MANY WORKS)

+ handles copy/send/remove files
+ store directory history
+ create directory

### 12: WidgetsLayer

+ Gtk.Box as main container for page.

## Trash Bin Layers (Planning)

### ~~1: PortalLayer~~

+ store unique_id for page
+ transmissions for selection/directory/filter changes message

### ~~2: CurrentDirectoryLayer~~

+ NEEDLESS.

### ~~3: ModelLayer~~

+ handles basemodel(child model in Gtk+3 term).
+ append/remove/clear tree_rows at this layer. not FilterLayer

### ~~4: FilterModelLayer~~

+ path activation and refilter messages should catch here.

### ~~5: FilterLayer~~

+ store keywords and show_hidden
+ filter_func handles here.
+ filter_func message comes from PortalLayer, not sub layers.

### ~~6: TileLayer~~

+ Handles TileLoader.

### ~~7: MonitorLayer~~

+ Monitor current direcotry at this layer.

### 8: SelectionLayer

+ store selection items
+ monitor selection

### 9: PrimePathLayer

+ store most recent path and tree_row

### 10: SortingLayer

+ handles sort order.

### ~~11: UtilityLayer (TOO MANY WORKS)~~

+ NEEDLESS

### ~~12: WidgetsLayer~~

+ Gtk.Box as main container for page.
