digraph {

    Filer [
        shape=star
        colorscheme=set312
        fillcolor=1
        fontcolor=White
        style=filled
    ]

    subgraph cluster_2 {
        DirectoryLayer [
            shape=doublecircle
            colorscheme=set312
            fillcolor=3
            fontcolor=White
            style=filled
        ]
        ModelLayer [
            shape=doublecircle
            colorscheme=set312
            fillcolor=4
            fontcolor=White
            style=filled
        ]
        FilterLayer [
            shape=doublecircle
            colorscheme=set312
            fillcolor=5
            fontcolor=White
            style=filled
        ]
        SelectionLayer [
            shape=doublecircle
            colorscheme=set312
            fillcolor=6
            fontcolor=White
            style=filled
        ]
        label="mainline"
        style=filled
        colorscheme=set312
        fillcolor=2
        color=White
        node [style=filled, fillcolor=White]
        DirectoryLayer -> ModelLayer
        ModelLayer -> FilterLayer
        FilterLayer -> SelectionLayer
        SelectionLayer -> PrimePathLayer
        PrimePathLayer -> WidgetsLayer
    }

    Filer -> DirectoryLayer

    subgraph cluster_4001 {
        label="basemodel"
        style=filled
        colorscheme=set312
        fillcolor=4
        color=White
        node [style=filled, fillcolor=White]
        ModelLayer -> BaseModel
        BaseModel -> DirectoryObserver
    }

    subgraph cluster_4002 {
        label="controllers\n(Model)"
        style=filled
        colorscheme=set312
        fillcolor=4
        color=White
        node [style=filled, fillcolor=White]
        ModelLayer -> "Controllers\n(Model)"
        "Controllers\n(Model)" -> "Sort"
        "Controllers\n(Model)" -> "Loader"
    }

    subgraph cluster_6001 {
        label="controllers\n(selection)"
        style=filled
        colorscheme=set312
        fillcolor=6
        color=White
        node [style=filled, fillcolor=White]
        SelectionLayer -> "Controllers\n(selection)"
        "Controllers\n(selection)" -> Copy
        "Controllers\n(selection)" -> Move
        "Controllers\n(selection)" -> Delete
    }

}