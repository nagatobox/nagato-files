
+ ~~deprecate /tile directory~~
+ integrate media-viewer (for music and videos)
+ integrate image-viewer (for stil and gif)
+ integrate pdf-viewer
+ think about viewers' queue pane
+ show cover-art as thumbnail
+ move all file and path operations to /libnagatopath
+ improve trash-bin selection

## media-viewer

+ deprecate VolumeScale class
+ control from tab popover
+ show media file basename on tab
